###################### divers import
import os
import time
import pandas as pd
from printColors import *

###################### Instance
from instance import *

###################### COR 2024

######### Greedy
from heuristic.greedy import *

######### group based
from MILP.COR_2024.AFJGS_MILP import *

###################### Instance directories

InstSet2 = "Calmels2"

###################### MILP results dictionnary
dictRes = {"instanceSet":[], "instance":[], "model": [], "nbCols":[], "nbRows":[], "model LB":[], "model UB":[], "Gurobi gap":[], "nbNodes":[], "CPU":[]}

###################### Compute models for all instances
for instSet in [InstSet2]:
    dir = os.path.join("instances",instSet)
    for subDir in os.listdir(dir):
        if os.path.isdir(os.path.join(dir, subDir)):
            for file in os.listdir(os.path.join(dir, subDir)):
                instPath = os.path.join(dir, subDir, file)
                
                ######### read instance
                # initialize an instance object
                instance = Instance()
                # read the instance
                instance.read(instPath)
                
                ######### update machine capacities
                instance.changeMachinesCapacities_v02() #new function
                
                ######### print the instance in the terminal
                print(CYAN)
                instance.printInstance()
                print(RESET)
                dictRes["instanceSet"].append(instSet)
                dictRes["instance"].append(file)

                ######### compute Upper Bound
                greedyCPU = time.time()
                instance.CmaxUB, blocks_mj, seq_m = randomGreedy(instance)
                greedyCPU = time.time() - greedyCPU
                
                for modelMILP in [AFJGS_MILP]:
                    start = time.time()
                    milp = modelMILP(instance)
                    milp.warmStart(blocks_mj, seq_m)
                    milp.solve()
                    
                    ######### compute Upper Bound
                    dictRes["model"].append(milp.name)
                    dictRes["model UB"].append(milp.modelUB)
                    dictRes["model LB"].append(milp.modelLB)
                    dictRes["Gurobi gap"].append(milp.modelGap)
                    dictRes["nbCols"].append(milp.nbColumns)
                    dictRes["nbRows"].append(milp.nbRows)
                    dictRes["nbNodes"].append(milp.nbNodes)
                    dictRes["CPU"].append(time.time() - start + greedyCPU)
                 
                    milp.model.dispose()
                    
                    #Save results to file
                    dfres = pd.DataFrame(dictRes)
                    dfres.to_csv(os.path.join("results", milp.name + "_" + instSet + ".csv"))

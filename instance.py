import math
import random
import seaborn as sb
import re

class Instance:
    
    def __init__(self):
        #-------------------------------------
        #           initialisation
        #-------------------------------------
        self.density = 0
        self.mode = "empty"
        self.T = []                         # list of tools (integers)
        self.J = []                         # list of jobs (integers)
        self.M = []                         # list of machines (integers)
        self.T_j = {}                        # {j: [required tools]}
        self.J_t = {}                        # {t: [concerned jobs]}
        self.C_m = {}                        # {m: capacity}
        self.sw_m = {}                       # {m: switch cost}
        self.p_mj = {}                       # {m: {j: processing time}}
        self.weights = {}                   # {m: {t: utility weight}}
        self.B_mj = {}                       # {m: {j: [blocks]}}
        self.S = {}                         # {b: {b': nb switches}}
        self.B_t = {}                     # {t : {"containing": [b], "requiring": [b]}}
        self.nbBlocks = 0                   # number of blocks
        self.CmaxUB = 0                     # minimum Makespan found
        self.CmaxLB = 0                     # minimum Makespan maybe reachable
        self.TSUB = 0                       # minimum number of Tool Switches found
        self.TFTUB = 0                      # minimum Total Flow Time found
        
        self.requirementDensity = 0
        self.clusterDensity = 0
    
    def generates(self, nbT, nbJ, nbM, density, mode = "capacity"):
        #-------------------------------------
        #           initialisation
        #-------------------------------------
        self.density = density
        self.percentageOnes = 0
        self.mode = mode
        self.T = [t for t in range(nbT)]    # list of tools
        self.J = [j for j in range(nbJ)]    # list of jobs
        self.M = [j for j in range(nbM)]    # list of machines
        self.T_j = {}                        # {j: [required tools]}
        self.J_t = {}                        # {t: [concerned jobs]}
        for t in self.T:
            self.J_t[t] = []
        self.C_m = {}                        # {m: capacity}
        self.sw_m = {}                       # {m: switch cost}
        self.p_mj = {}                       # {m: {j: processing time}}
        self.weights = {}                   # {m: {t: utility weight}}
        self.B_mj = {}                       # {m: {j: [blocks]}}
        self.S = {}                         # {b: {b': nb switches}}
        self.nbBlocks = 0                   # number of blocks

        
        #-------------------------------------
        #      generation of C_m, sw_m, p_mj
        #-------------------------------------
        
        #---- density represents the proportion of 1 in T_j matrix
        if mode == "matrix":
            targetNbOnes = math.ceil(len(self.T) * len(self.J) * density)
            targetNbOnes = max(targetNbOnes, max(len(self.T), len(self.J)))
            self.requirementDensity = targetNbOnes / (len(self.T) * len(self.J))
            for j in self.J:
                t = random.choice(self.T)
                self.T_j[j] = [t]
                self.J_t[t].append(j)
                targetNbOnes -= 1
            
            
            for t in self.T:
                if len(self.J_t[t]) < 1:
                    j = random.choice(self.J)
                    self.T_j[j].append(t)
                    self.J_t[t] = [j]
                    targetNbOnes -= 1
            
            while targetNbOnes > 0:
                j = random.choice(self.J)
                t = random.choice(self.T)
                if t not in self.T_j[j]:
                    self.T_j[j].append(t)
                    targetNbOnes -= 1
                    self.J_t[t].append(j)
            
            print("T_j", self.T_j)
            print("J_t", self.J_t)
            
            minT_j = len(self.T)
            maxT_j = 0
            for j in self.J:
                self.T_j[j] = sorted(self.T_j[j])
                maxT_j = max(maxT_j, len(self.T_j[j]) )
                minT_j = min(minT_j, len(self.T_j[j]) )
            

            for m in self.M:
                self.C_m[m] = random.randint(minT_j, int(min(len(self.T), minT_j + maxT_j) ))
                self.sw_m[m] = random.randint(1, 2)
            
            self.C_m[m] = max(self.C_m[m], maxT_j)
        
        #---- density represents the proportion of T_j compared to C_m (Calmels)
        elif mode == "capacity":
            minC_m = len(self.T)
            maxC_m = 0
            for m in self.M:
                self.C_m[m] = random.randint(2, 10)
                minC_m = min(minC_m, self.C_m[m])
                maxC_m = max(maxC_m, self.C_m[m])
                self.sw_m[m] = random.randint(1, 2)

            for j in self.J:
                if density >= 0.5:
                    nb = math.ceil(random.triangular(math.ceil(min(minC_m/4, density * minC_m) ), math.ceil(minC_m/2), density * minC_m) )
                else:
                    nb = math.ceil(random.triangular(math.ceil(maxC_m/2), maxC_m, density * maxC_m) )
                self.T_j[j] = random.sample(self.T,nb)
                self.percentageOnes += len(self.T_j[j])
                for t in self.T_j[j]:
                    self.J_t[t].append(j)
                    
            self.requirementDensity = self.requirementDensity / (len(self.T) * len(self.J))

        for m in self.M:
            self.p_mj[m] = {}
            for j in self.J:
                if len(self.T_j[j]) <= self.C_m[m]:
                    self.p_mj[m][j] = random.randint(1, 5)
        
        print("Pmj")
        print(self.p_mj)
        
        # minimum Makespan found
        self.CmaxUB = sum([min([self.p_mj[m][j] + self.sw_m[m] * len(self.T_j[j]) for m in self.M if j in self.p_mj[m].keys()]) for j in self.J])
        # minimum Makespan maybe reachable
        self.CmaxLB = math.floor(sum([min([self.p_mj[m][j] for m in self.M if j in self.p_mj[m].keys()]) for j in self.J])/len(self.M) )
        # minimum number of Tool Switches found
        self.TSUB = sum([len(self.T_j[j]) for j in self.J]) - sum([self.C_m[m] for m in self.M])
        # minimum Total Flow Time found
        self.TFTUB = self.CmaxUB * len(self.J)
        
    

    def read(self, path):
    
        #-------------------------------------
        #           open file
        #-------------------------------------
        f = open(path, "r")
        lines = f.read().splitlines()
        firstRow = re.split(";",lines[0])
        secondRow = re.split(";",lines[1])
        thirdRow = re.split(";",lines[2])
        
        #-------------------------------------
        #           initialisation
        #-------------------------------------
        self.density = 0
        self.mode = "readed"
        firstRow = re.split(";", lines[0])
        self.T = [t for t in range(int(firstRow[2]) )]    # list of tools
        self.J = [j for j in range(int(firstRow[1]) )]    # list of jobs
        self.M = [m for m in range(int(firstRow[0]) )]    # list of machines
        self.C_m = {}                                      # {m: capacity}
        self.sw_m = {}                                     # {m: switch cost}
        self.p_mj = {}                                     # {m: {j: processing time}}
        self.T_j = {}                                      # {j: [required tools]}
        
        for m in self.M:
            self.C_m[m] = int(secondRow[m])
            self.sw_m[m] = int(thirdRow[m])
            self.p_mj[m] = {}
        
        maxLine = 3 + len(self.M)
        m = 0
        for line in lines[3:maxLine]:
            row = re.split(";", line)
            for j in self.J:
                if row[j] != "NA":
                    self.p_mj[m][j] = int(row[j])
                self.T_j[j] =[]
            m += 1
        
        self.J_t = {}                        # {t: [concerned jobs]}
        
        t = 0
        for line in lines[maxLine:]:
            self.J_t[t] = []
            row = re.split(";", line)
            for j in self.J:
                #print("tool %s"%t, "job %s"%j, int(row[j]))
                if int(row[j]) == 1:
                    self.T_j[j].append(t)
                    self.J_t[t].append(j)
                    self.requirementDensity += 1

            t += 1
        
        self.requirementDensity /= (len(self.T) * len(self.J))
                    
        #print("p_mj before:", self.p_mj)
        for m in self.p_mj.keys():
            toRemove = []
            for j in self.p_mj[m].keys():
                #print(len(self.T_j[j]), self.C_m[m])
                if len(self.T_j[j]) > self.C_m[m]:
                    toRemove.append(j)
            
            for j in toRemove:
                self.p_mj[m].pop(j)
            
        #print("p_mj after:", self.p_mj)
            
        self.weights = {}                   # {m: {t: utility weight}}
        self.B_mj = {}                       # {m: {j: [blocks]}}
        self.S = {}                         # {b: {b': nb switches}}
        self.nbBlocks = 0                   # number of blocks

        # minimum Makespan found
        self.CmaxUB = sum([min([self.p_mj[m][j] + self.sw_m[m]*len(self.T_j[j]) for m in self.M if j in self.p_mj[m].keys()]) for j in self.J])
        # minimum Makespan maybe reachable
        self.CmaxLB = math.floor(sum([min([self.p_mj[m][j] for m in self.M if j in self.p_mj[m].keys()]) for j in self.J])/len(self.M) )
        # minimum number of Tool Switches found
        self.TSUB = sum([len(self.T_j[j]) for j in self.J]) - sum([self.C_m[m] for m in self.M])
        # minimum Total Flow Time found
        self.TFTUB = self.CmaxUB * len(self.J)
    
        f.close()
    
    
    def write(self, path):
    
        #-------------------------------------
        #           open file
        #-------------------------------------
        f = open(path, "w")
        f.write(str(len(self.M)) + ";" + str(len(self.J)) + ";" + str(len(self.T)) + "\n")
        secondLine = ""
        for m in self.M:
            secondLine += str(self.C_m[m]) + ";"
        f.write(secondLine[:-1] + "\n")
        thirdLine = ""
        for m in self.M:
            thirdLine += str(self.sw_m[m]) + ";"
        f.write(thirdLine[:-1] + "\n")
        
        for m in self.M:
            machineLine = ""
            for j in self.J:
                if j in self.p_mj[m].keys():
                    machineLine += str(self.p_mj[m][j]) + ";"
                else:
                    machineLine += "NA;"
            f.write(machineLine[:-1] + "\n")
         
        for t in self.T:
            toolLine = ""
            for j in self.J:
                if j in self.J_t[t]:
                    toolLine += "1;"
                else:
                    toolLine += "0;"
            f.write(toolLine[:-1] + "\n")
        
        f.close()
             
    
    #----------------------- printInstance: shows the instances informations in console
    def printInstance(self):
        print("Nb Tools:",len(self.T))
        print("\nMachines")
        print("m\tC_m\tsw_m")

        for m in self.M:
            print(str(m) + "\t" + str(self.C_m[m]) + "\t" + str(self.sw_m[m]))

        print("\nJobs")

        title = "j" + "\t|"
        for m in self.M:
            title += "\tp"+str(m)+"j"
        title += "\t|\ttools"
        print(title)
        for j in self.J:
            stringJob = str(j) +"\t|"
            for m in self.C_m.keys():
                stringJob += "\t"
                if j in self.p_mj[m].keys():
                     stringJob += str(self.p_mj[m][j])
            stringJob += "\t|"
            for t in self.T_j[j]:
                stringJob += "\t" + str(t)
            print(stringJob)
        
        print("\nLower bound (Cmax):", self.CmaxLB)
        print("\nUpper bound (Cmax):", self.CmaxUB)
 
    #----------------------- change_machines_capacities: compare the real number of required tools and compare with machine capacities. If capacity > required tools, than new capacity equals the number of required tools
    def changeMachinesCapacities(self):
        toolsRequired = [0] * len(self.T)
        nbToolsRequired = 0
        for j in self.J:
            for t in range(0, len(self.T_j[j]) ):
                tool = self.T_j[j][t]
                if toolsRequired[tool] == 0:
                    toolsRequired[tool] = 1
                    nbToolsRequired += 1
                    
        print ("Real nb of required tools: ", nbToolsRequired)
        for m in self.M:
            maxNbTools = 0
            for j in self.p_mj[m]:
                maxNbTools = max(maxNbTools, len(self.T_j[j]))
            if self.C_m[m] > maxNbTools:
                print('Capacity of Machine ', m, ' changed from ',self.C_m[m] , ' to ', maxNbTools )
                self.C_m[m] = maxNbTools
        return nbToolsRequired

    #----------------------- change_machines_capacities: compare the real number of required tools and compare with machine capacities. If capacity > required tools, than new capacity equals the number of required tools
    def changeMachinesCapacities_v02(self):
        toolsRequired = [0] * len(self.T)
        nbToolsRequired = 0
        for m in self.M:
            machine_toolsRequired = [0] * len(self.T)
            machine_nbToolsRequired = 0
            for j in self.p_mj[m]:
                for t in range(0, len(self.T_j[j]) ):
                    tool = self.T_j[j][t]
                    if machine_toolsRequired[tool] == 0:
                        machine_toolsRequired[tool] = 1
                        machine_nbToolsRequired += 1
                        if toolsRequired[tool] == 0:
                            toolsRequired[tool] = 1
                            nbToolsRequired += 1
            if self.C_m[m] > machine_nbToolsRequired:
                print('Capacity of Machine ', m, ' changed from ',self.C_m[m] , ' to ', machine_nbToolsRequired )
                self.C_m[m] = machine_nbToolsRequired
        print ("Real nb of required tools: ", nbToolsRequired)                       

        return nbToolsRequired
           
    #----------------------- compute_weights: computes tools' utility weights based on jobs' reduced cost provided in a dictionnary gainj
    def computeWeights(self, gainj):
        self.weights = {}
        for m in self.M:
            self.weights[m] = {}
            for t in self.T:
                self.weights[m][t] = sum([gainj[j] for j in self.J_t[t] if j in self.p_mj[m].keys()])
        #print("Weights:",self.weights)

    #----------------------- addBlock: add block and update switch matrix S
    def addBlock(self, m, j, block):
        b = self.nbBlocks
        if m not in self.B_mj.keys():
            self.B_mj[m] = {}
        if j in self.p_mj[m].keys():
            if j not in self.B_mj[m].keys():
                self.B_mj[m][j] = {}
                
            for t in self.T_j[j]:
                if t not in block:
                    print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
                    print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\t!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
                    print("t %s not in block"%t,block)
                    print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\t!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
                    print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
            self.B_mj[m][j][b] = block
            if b not in self.S.keys():
                self.S[b] = {}
                self.nbBlocks += 1
            else:
                print("\t /|\\ \n\t/_°_\\")
                print("Error!!!!!!! block %s already exist"%b)
            for j2 in self.B_mj[m].keys():
                if j2 != j:
                    for b2, block2 in self.B_mj[m][j2].items():
                        self.S[b][b2] = len([t for t in block if t not in block2])
                        self.S[b2][b] = len([t for t in block2 if t not in block])
            
            return True
        else:
            print("\t /|\\ \n\t/_°_\\")
            print("Error!!!!!!! job %s is not compatible with machine %s"%(j,m))
        return False
                            
    #----------------------- generate_best_block: return for couple (machine m, job j) the block is the maximum total utility weight which includes T_j
    def generatesBestBlock(self, m, j):
        block = [t for t in self.T_j[j]]                 # filling with T_j
        for k in range(self.C_m[m]-len(self.T_j[j])):     # while not filled
            maxW = 0
            best_t = -1
            for t in self.T:                            # search tool with maximum utility weight
                if (t not in block) and (maxW <= self.weights[m][t]):      # select only tool not already selected
                    maxW = self.weights[m][t]
                    best_t = t
            block.append(best_t)
        return block
  
    #----------------------- generate_all_blocks: generates for couple (machine m, job j) all feasible blocks
    def generatesAllBlocks(self, m, j):
        #print("Nb blocks before:",self.nbBlocks)
        if j in self.p_mj[m].keys():
            partialB = {0: self.T_j[j]}                # add an intial partial block containing only T_j
            while (len(partialB[0]) < self.C_m[m]) and (len(partialB[0]) < len(self.T)):
                newB_mj = {}
                for b, block in partialB.items():       # for all partial block
                    for t in self.T:                    # find a new tool to add
                        if t not in block:
                            if (len(block) == len(self.T_j[j])) or (t > block[-1]):      # add only if greater than any tools in the partial block that is not in T_j
                                newB_mj[len(newB_mj)] = block + [t]

                partialB = newB_mj

            for b, block in partialB.items():
                self.addBlock(m,j,sorted(block))
            #print("machine %s, job %s: %s blocks"%(m,j,len(self.B_mj[m][j])))
            #print("m %s, j %s:"%(m,j),self.B_mj[m][j].values())
    
        return self.nbBlocks

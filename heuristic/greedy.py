
from collections import defaultdict
import numpy as np



from instance import *


################################################################################
#####                           Class Machine()                           #####
################################################################################
# used only for the greedy function
class Machine:

    ##### Constructor
    def __init__(self, Id, sw, C):
        ##### Attributes
        self.Id = Id        # machine index
        self.sw = sw        # machine switch cost
        self.C = C          # machine capacity
        self.T = []         # list of loaded tools
        self.J = []         # sequence of jobs
        self.F = 0          # flow time


    ##### KTNS: TODO
    def KTNS(self, instance, seqJ):
        #---------- initialization
        bestNbswitches = 0
        blocks_j = {}
        T = instance.T #set([t for j in seqJ for t in instance.T_j[j]])
        #----------
        #---------- Step 1: Set 𝐽𝑖=1 for 𝐶 values of 𝑖 having minimal values of 𝐿(𝑖,0). Break the ties arbitrarily. Set 𝐽𝑖=0 for the remaining 𝑀−𝐶 values of 𝑖. Set 𝑛=1.
        # k: position (==instant), t: tool
        matrix_kt = {}
        for k in range(len(seqJ)):
            matrix_kt[k] = {}
            for t in T:
                soonestk = k
                required = False
                while (soonestk < len(seqJ)) and (not required):
                    if(t in instance.T_j[seqJ[soonestk]]):
                        matrix_kt[k][t] = soonestk
                        required = True
                    else:
                        soonestk += 1

                if not required:
                    matrix_kt[k][t] = len(seqJ)

        #print('matrix_kt: ', matrix_kt)
        #----------
        #---------- Step 2 --- Step 5
        for k in range(len(seqJ)):
            #----- if k=0
            if (k==0):
                #----- needed tools for j in position k
                block = [t for t in instance.T_j[seqJ[k]]]
                #----- sort remaining tools (T\block)
                sortedRemainingT = sorted ([t for t in T if t not in block], key = lambda x: matrix_kt[k][x] )
                #print('sortedRemainingT: ', sortedRemainingT)

                #----- fill remainingC elements in block by the smallest tools from sortedRemainingT
                for c in range(self.C -  len(block)):
                    block.append(sortedRemainingT[c])

            #----- if k>0
            #----- compute bestNbswitches
            else:
                #----- block: list of required tools already in the magazine
                block = [t for t in blocks_j[seqJ[k-1]] if t in instance.T_j[seqJ[k]]]
                #----- toolsNotRequired: list of tools not required inside the magazine at position k
                toolsNotRequired = sorted([t for t in blocks_j[seqJ[k-1]] if t not in instance.T_j[seqJ[k]]], key = lambda x: matrix_kt[k][x], reverse=True )
                #----- toolsToAdd: list of missing tools for processing j at position k
                toolsToAdd = [t for t in instance.T_j[seqJ[k]] if t not in block]
                #----- toolsToRemove: list of tools to remove (switch)
                if len(toolsToAdd) > len(toolsNotRequired):
                    print("C_m", self.C)
                    print("block", block)
                    print("T_j", instance.T_j[seqJ[k]])
                    print("toolsNotRequired", toolsNotRequired)
                    print("toolsToAdd", toolsToAdd)
                toolsToRemove = [toolsNotRequired[c] for c in range(len(toolsToAdd))]
                # toolsToRemove = [toolsNotRequired[c] for c in range(len(toolsNotRequired))]
                #----- fill block
                for t in toolsToAdd:
                    block.append(t)

                for t in toolsNotRequired:
                    if t not in toolsToRemove:
                        block.append(t)

                #----- compute bestNbswitches
                bestNbswitches += len(toolsToRemove)
            #----- fill blocks_j[j], where j = seqJ[k]
            blocks_j[seqJ[k]] = block

        return bestNbswitches, blocks_j

    ##### computeBestInsertion: return the best number of switches with its corresponding insertion position
    def computeBestInsertion(self, instance, j):
    
        if len(instance.T_j[j]) > self.C:
            return math.inf , -1
            
        ##### initialize the position at the end
        k = len(self.J)
        # generates the job sequence
        seqJ = self.J + [j]
        # compute KTNS
        bestNbswitches, blocks_j = self.KTNS(instance, seqJ)
        bestPosition = k

        ##### try any other positions
        k -= 1

        while k > 0:
            # generates the job sequence
            seqJ = self.J[:k] + [j] + self.J[k:]
            # compute KTNS
            nbSwitches, blocks_j = self.KTNS(instance, seqJ)
            # keep best position
            if nbSwitches < bestNbswitches:
                bestPosition = k
                bestNbswitches = nbSwitches

            k -= 1

        flowTime = self.sw * bestNbswitches + sum([instance.p_mj[self.Id][j] for j in seqJ])

        return flowTime, bestPosition


    ##### insert:
    def insert(self, instance, j, k):
        if k >= 0 and k < len(self.J):
            # insert the job
            self.J = self.J[:k] + [j] + self.J[k:]
        else:
            # add at the end
            self.J.append(j)

        # compute KTNS
        TS, blocks_j = self.KTNS(instance, self.J)
        self.F = self.sw * TS + sum([instance.p_mj[self.Id][j] for j in self.J])

        return self.F, blocks_j


##### randomGreedy:
def randomGreedy(instance, randRate = 0):
    machines = {}
    for m in instance.M:
        machines[m] = Machine(m, instance.sw_m[m], instance.C_m[m])
    unscheduledJ = [j for j in instance.J]
    Cmax = 0
    while len(unscheduledJ) > 0:
        bestjm = [unscheduledJ[0], 0]
        F, k = machines[0].computeBestInsertion(instance, unscheduledJ[0])
        bestF = max(Cmax, F)
        bestk = -1
        for m in instance.M:
            for j in unscheduledJ:
                if (j in instance.p_mj[m].keys()) and (len(instance.T_j[j]) <= machines[m].C):
                    F, k = machines[m].computeBestInsertion(instance, j)
                    if bestF >= F * (1 - random.uniform(0, randRate) ):
                        bestF = F
                        bestjm = [j, m]
                        bestk = k
        machines[bestjm[1] ].insert(instance, bestjm[0], bestk)
        print(bestjm)
        Cmax = max(Cmax, machines[bestjm[1] ].F)
        unscheduledJ.remove(bestjm[0])

    blocks_mj = {}
    seq_m = {}
    for m, machine in machines.items():
        TS, blocks_j = machine.KTNS(instance, machine.J)
        blocks_mj[m] = blocks_j
        print('machine %s: '%m, 'TS: ', TS)
        seq_m[m] = machine.J

    return Cmax, blocks_mj, seq_m

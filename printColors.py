#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 15 19:33:45 2022

@author: khadijahs
@name: Color.py
@intent: provide a color' configuration for our 'print' task
"""
import os
os.system("")

#----- """ COLOR for PRINT """
BLACK = '\033[30m'
RED = '\033[31m'
GREEN = '\033[32m'
YELLOW = '\033[33m'
BLUE = '\033[34m'
MAGENTA = '\033[35m'
CYAN = '\033[36m'
CRED = '\033[41m'
CGREEN = '\033[42m'
CYELLOW = '\033[43m'
CBLUE = '\033[44m'
CMAGENTA = '\033[45m'
CCYAN = '\033[46m'
LGREY= '\033[37m'  #lightgrey
DGREY = '\033[90m' #darkgrey
LRED = '\033[91m'#lightred
LGREEN = '\033[92m'#lightgreen
LYELLOW = '\033[93m'#lightyellow
LBLUE = '\033[94m'#lightblue
PINK = '\033[95m'
LCYAN = '\033[96m'#lightcyan
UNDERLINE = '\033[4m'
RESET = '\033[0m'




from gurobipy import *
from instance import *
from machineKP import *
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle





################################################################################
#####                           Class AFbaseILP()                           #####
################################################################################
class ArcFlow_CG:
    ##### Constructor
    def __init__(self, inst, blocks_mj, model_name="AF_CG", bounded=True):
        ##### Attributes
        self.bounded = bounded
        self.name = model_name
        self.model = Model(self.name)      # model
        self.obj_value = 0                 # objective value of MILP
        self.blocks_mj = blocks_mj
        
        self.addedColumns = 0
  
        #Initialize auxiliary values
        self.beta = [0] * len(inst.M)
        for m in inst.M:
            self.beta[m] = 99999999
            for j in inst.J:
                if len(inst.T_j[j]) <= inst.C_m[m]:
                    if inst.p_mj[m][j] < self.beta[m]:
                        self.beta[m] = inst.p_mj[m][j]

        #Initialize structure
        self.initialize_af_CG_structure(inst)

        #Fill structure with initial solution information
        self.transform_heur_solution_AF_vairables(inst)
        
        #Create IRMP
        self.create_AF_IRMP_model(inst)

    def create_AF_IRMP_model(self, inst):
        #Create variables
        variables = 0
        constraints = 0

        # ----- C_max variable : For the objective Function
        if self.bounded:
            self.H = inst.FmaxUB
            self.C_max = self.model.addVar(lb=inst.FmaxLB, ub=inst.FmaxUB, obj=1, vtype=GRB.CONTINUOUS, name='C_max')  # makespan
            variables += 1
        else:
            print("AF needs an initial UB to run!!")
            exit(0)
        
        print("AF_CG: creating IRMP...")
        print("Initial UB = ", self.H)

        #----- X variables ---- Arc Variables for Job processing
        self.X_mjpq = {} # existence of the arc(jpqk)
        for m in inst.M:
            for j in inst.J:
                if len(inst.T_j[j]) <= inst.C_m[m]: #if there is enough slots on machine m for job j
                    for p in range(0, self.H - inst.p_mj[m][j] +1):
                        q  = p + inst.p_mj[m][j]
                        if self.sol_X_mjpq[(m, j, p, q)] == True:
                            # print('X[%s][%s][%s][%s]'%(m, j, p, q))
                            self.X_mjpq[(m, j, p, q)] = self.model.addVar(obj=0, lb=0, ub=1, vtype=GRB.CONTINUOUS, name='X[%s][%s][%s][%s]'%(m, j, p, q))
                            variables += 1
               

        #----- Y variables ---- Arc Variables for Tool loading
        self.Y_mtpq = {}  # existence of the arc(tpqk) meaning if tool t is on the machine k at the
        for m in inst.M:
            for t in inst.T:
                for p in range(0, self.H + 1):                                                  
                    for q in range(p + 1, self.H + 1):
                        if self.sol_Y_mtpq[(m, t, p, q)] == True:
                            # print('Y[%s][%s][%s][%s]'%(m, t, p, q))
                            self.Y_mtpq[(m, t, p, q)] = self.model.addVar(obj=0, lb=0, ub=1, vtype=GRB.CONTINUOUS, name='Y[%s][%s][%s][%s]'%(m, t, p, q))
                            variables += 1

                    
        #----- S variables ---- Variables for tool switching operations / Set_UP arcs
        self.S_mpq = {}  # existence of the arc(pqk) meaning if there is a setup/switching from machine k at the
        for m in inst.M:
            for p in range(self.beta[m], self.H+1):
                for l in range(1, inst.C_m[m]+1) : #For each slot / maximum number of switches
                    if p + inst.sw_m[m] * l < self.H+1:
                        if self.sol_S_mpq[(m, p, p + inst.sw_m[m] * l)] == True:
                            # print('S[%s][%s][%s]'%(m, p, p + inst.sw_m[m] * l))
                            self.S_mpq[(m, p, p + inst.sw_m[m] * l)] = self.model.addVar(obj=0, lb=0, ub=1, vtype=GRB.CONTINUOUS, name='S[%s][%s][%s]'%(m, p, p + inst.sw_m[m] * l))
                            variables += 1

                           
        # ----- L variables ---- Dummy Arc Variables : for loss arcs
        self.L_mp = {}  # existence of the arc(pk) meaning if there is a loss on machine k at moment p to finish up the graph till H
        for m in inst.M:
            for p in range(self.beta[m], self.H):
                if (m, p) not in self.L_mp:
                    self.L_mp[(m, p)] = self.model.addVar(obj=0, lb=0, ub=1, vtype=GRB.CONTINUOUS, name='L[%s][%s]'%(m, p))
                    variables += 1
                    
        #Update model
        self.model.update()
        print(variables, "variables created !!")
        
        #Set objective function
        self.model.setObjective( self.C_max, sense=GRB.MINIMIZE )
        
        ############################## Create_constraints ##############################
        #Initially, create structure for dual variables
        self.alpha_j = {}
        self.beta_mq = {}
        self.gamma_mq = {}
        self.tau_mjtq = {}
        self.theta_mq = {}
        self.epsilon_j = {}
        #Create strucutre for reduced costs
        self.rc_X_mjpq = {}
        self.rc_Y_mtpq = {} 
        self.rc_S_mpq = {}
        
       # Constraint_1 - alpha_j
        for j in inst.J: 
            C1 = 0
            for m in inst.M:
                if len(inst.T_j[j]) <= inst.C_m[m]: #if there is enough slots on machine m for job j
                    for p in range(0, self.H - inst.p_mj[m][j] +1 ):
                        if (m, j, p, p + inst.p_mj[m][j]) in self.X_mjpq:
                            C1 += self.X_mjpq[(m, j, p, p + inst.p_mj[m][j])]
            cname = "C1_Job_" + str(j)   
            # self.model.addLConstr(C1 == 1, name=cname)
            self.alpha_j[(j)] = self.model.addLConstr(C1 == 1, name=cname)
            constraints += 1
        #end for j

        # Constraint_2 - beta_mq
        for m in inst.M:
            for q in range(0, self.H+1):
                C2 = 0
                #ARCS LEAVING q
                #Job arcs
                for j in inst.J:
                    for r in range(q+1, self.H+1):
                        if (m, j, q, r) in self.X_mjpq:
                            C2 += self.X_mjpq[(m, j, q, r)]
                            
                #setup arcs
                for r in range(q+1, self.H+1):
                    if (m, q, r) in self.S_mpq:
                        C2 += self.S_mpq[(m, q, r)]
            
                #loss arcs
                if (m, q) in self.L_mp:
                    C2 += self.L_mp[(m, q)]
 
                #ARCS ARRIVING q
                #Job arcs
                for j in inst.J:
                    for p in range(0, q):
                        if (m, j, p, q) in self.X_mjpq:
                            C2 -= self.X_mjpq[(m, j, p, q)]                    
                #setup arcs
                for p in range(0, q):            
                    if (m, p, q) in self.S_mpq:
                        C2 -= self.S_mpq[(m, p, q)]            
            
                #RHS + loss arcs rhs
                if q == 0:
                    C2 -= 1
                else:
                    if q == self.H:
                        C2 += 1
                        for p in range(0, self.H):
                            if (m, p) in self.L_mp:
                                C2 -= self.L_mp[(m, p)]
            
                cname = "C2_machine_time_" + str(m) + "_" + str(q)   
                # self.model.addLConstr(C2 == 0, name=cname)
                self.beta_mq[(m, q)] =self.model.addLConstr(C2 == 0, name=cname)
                constraints += 1
            #end for q
        #end for m
            
        # Constraint_3 - gamma_mq
        for m in inst.M:
            for q in range(0, self.H+1):
                C3 = 0
                #ARCS LEAVING q
                #Tools arcs
                for t in inst.T:
                    for r in range(q+1, self.H+1):
                        if (m, t, q, r) in self.Y_mtpq:
                            C3 += self.Y_mtpq[(m, t, q, r)]
                            
                #setup arcs
                for r in range(q+1, self.H+1):
                    if (m, q, r) in self.S_mpq:
                        setup_coef = (r-q) / inst.sw_m[m]
                        C3 += setup_coef * self.S_mpq[(m, q, r)]
            
                #ARCS ARRIVING q
                #Tools arcs
                for t in inst.T:
                    for p in range(0, q):
                        if (m, t, p, q) in self.Y_mtpq:
                            C3 -= self.Y_mtpq[(m, t, p, q)]                    
                #setup arcs
                for p in range(0, q):            
                    if (m, p, q) in self.S_mpq:
                        setup_coef = (q-p) / inst.sw_m[m]
                        C3 -= setup_coef * self.S_mpq[(m, p, q)]  

                #RHS + loss arcs rhs
                if q == 0:
                    C3 -= inst.C_m[m]
                else:
                    if q == self.H:
                        C3 += inst.C_m[m]
            
                cname = "C3_machine_time_" + str(m) + "_" + str(q)   
                # self.model.addLConstr(C3 == 0, name=cname)
                self.gamma_mq[(m, q)] = self.model.addLConstr(C3 == 0, name=cname)
                constraints += 1
            #end for q
        #end for m
        
        # Constraint_5 - Tool  - tau_mjtq
        for m in inst.M:
            for j in inst.J: 
                for t in range(0, len(inst.T_j[j])):
                    tool = inst.T_j[j][t]
                    if len(inst.T_j[j]) <= inst.C_m[m]: #if there is enough slots on machine m for job j
                        for s in range(0, self.H - inst.p_mj[m][j] + 1):
                            C5 = 0
                            if (m, j, s, s + inst.p_mj[m][j]) in self.X_mjpq:
                                C5 += self.X_mjpq[(m, j, s, s + inst.p_mj[m][j])]
                                
                            for p in range(0, s+1): #including s
                                for q in range(s + inst.p_mj[m][j], self.H + 1):
                                    if (m, tool, p, q) in self.Y_mtpq:
                                        C5 -= self.Y_mtpq[(m, tool, p, q)]
    
                            cname = "C5_machine_job_tool_time_" + str(m) + "_" + str(j) + "_" + str(tool) + "_" + str(s)
                            # self.model.addLConstr(C5 <= 0, name=cname)
                            self.tau_mjtq[(m, j, tool, s)] = self.model.addLConstr(C5 <= 0, name=cname)
                            constraints += 1
                        #end for s
                #end for t
            #end for j
        #end for m

        # Constraint_6 - Alternating paths - theta_mq
        for m in inst.M:
            for q in range(1, self.H):
                C6 = 0
                #ARCS LEAVING q
                #Tools arcs
                for t in inst.T:
                    for r in range(q+1, self.H+1):
                        if (m, t, q, r) in self.Y_mtpq:
                            C6 += self.Y_mtpq[(m, t, q, r)]

                #ARCS ARRIVING q
                #setup arcs
                for p in range(0, q):            
                    if (m, p, q) in self.S_mpq:
                        setup_coef = (q-p) / inst.sw_m[m]
                        C6 -= setup_coef * self.S_mpq[(m, p, q)] 

                cname = "C6_machine_time_" + str(m) + "_" + str(q)
                # self.model.addLConstr(C6 == 0, name=cname)
                self.theta_mq[(m, q)] = self.model.addLConstr(C6 == 0, name=cname)
                constraints += 1
            #end for q
        #end for m

        # Constraint_8 - Makespan constraint - epsilon_j
        for j in inst.J: 
            C8 = self.C_max
            for m in inst.M:
                for p in range(0, self.H + 1):
                    for q in range(0, self.H + 1):
                        if (m, j, p, q) in self.X_mjpq:
                            C8 -= q*self.X_mjpq[(m, j, p, q)]
            cname = "C8_job_" + str(j)
            # self.model.addLConstr(C8 >= 0, name=cname)
            self.epsilon_j[(j)] = self.model.addLConstr(C8 >= 0, name=cname)
            constraints += 1

        # # Valid inequalities
        # # VI 1 - no two consecutive setups arcs on same machine
        # for m in inst.M:
        #     for q in range(1, self.H):
        #         VI1 = 0
        #         #ARCS LEAVING q
        #         #Setups arcs
        #         for t in range(1, inst.C_m[m]+1):
        #             aux_end_time = q + t*inst.sw_m[m]
        #             if (m, q, aux_end_time) in self.S_mpq:
        #                 VI1 += self.S_mpq[(m, q, aux_end_time)]
 
        #         #ARCS ARRIVING q
        #         #setup arcs
        #         for t in range(1, inst.C_m[m]+1):
        #             aux_start_time = q - t*inst.sw_m[m]
        #             if (m, aux_start_time, q) in self.S_mpq:
        #                 VI1 += self.S_mpq[(m, aux_start_time, q)]

        #         cname = "VI1_machine_time_" + str(m) + "_" + str(q)
        #         self.model.addLConstr(VI1 <= 1, name=cname)
        #         constraints += 1

        # # VI 3 - nb of setups on machine m is smaller than nb of jobs on machine m
        # for m in inst.M:
        #     VI3 = 0
        #     #sum all jobs arcs + all loss arcs
        #     for j in inst.J:
        #         for p in range(0, self.H + 1):
        #             for q in range(0, self.H + 1):
        #                 if (m, j, p, q) in self.X_mjpq:
        #                     VI3 += self.X_mjpq[(m, j, p, q)]    
            
        #     #sum all setup arcs
        #     for p in range(1, self.H):
        #         for l in range(1, inst.C_m[m]+1):
        #             aux_end_time = p + l * inst.sw_m[m]
        #             if (m, p, aux_end_time) in self.S_mpq:
        #                 VI3 -= self.S_mpq[(m, p, aux_end_time)]

        #     cname = "VI3_machine_" + str(m)
        #     self.model.addLConstr(VI3 >= 1, name=cname)
        #     constraints += 1

        # # VI 4 - makespan based on machines and loss arcs
        # for m in inst.M:
        #     VI4 = self.C_max
        #     VI5 = 0
        #     for p in range(0, self.H + 1):
        #         if (m, p) in self.L_mp:
        #             VI4 -= p * self.L_mp[(m, p)]
        #             VI5 += self.L_mp[(m, p)]

        #     cname = "VI4_machine_" + str(m)
        #     self.model.addLConstr(VI4 >= 0, name=cname)
        #     cname = "VI5_machine_" + str(m)
        #     self.model.addLConstr(VI5 <= 1, name=cname)
        #     constraints += 2

        print(constraints, "constraints created !!")

        #Update model
        self.model.update()
        
        self.relaxedModel = self.model.relax()
        self.model.write("AF_CG_IRMP.lp")

    def initialize_af_CG_structure(self, inst):
        self.nb_all_variables = 0
        self.H = inst.FmaxUB
        #----- X variables ---- Arc Variables for Job processing
        self.sol_X_mjpq = {} # existence of the arc(jpqk) 
        # for m in range(0,inst.M):    #for each machine
        for m in inst.M:    #for each machine
            self.sol_X_mjpq[m] = {}
            for j in inst.J:     #for each job
                self.sol_X_mjpq[m][j] = {}
                if len(inst.T_j[j]) <= inst.C_m[m]:
                    for p in range(0, self.H - inst.p_mj[m][j] +1 ):   #for each time instant
                        self.sol_X_mjpq[m][j][p] = {}
                        if (m, j, p, p + inst.p_mj[m][j]) not in self.sol_X_mjpq:   # the q here id equal H-proc_time[k-1][j-1] it's imposed
                            self.sol_X_mjpq[(m, j, p, p + inst.p_mj[m][j])] = False
                            self.nb_all_variables += 1
        
        #----- Y variables ---- Arc Variables for Tool loading
        self.sol_Y_mtpq = {}  # existence of the arc(tpqk) meaning if tool t is on the machine k at the
        for m in inst.M:
            for t in inst.T:
                minProc_mt = 1        
                for p in range(0, self.H + 1):                                                  
                    for q in range(p + minProc_mt, self.H + 1):
                        if (m, t, p, q) not in self.sol_Y_mtpq:
                            self.sol_Y_mtpq[(m, t, p, q)] = False
                            self.nb_all_variables += 1
                    
        #----- S variables ---- Variables for tool switching operations / Set_UP arcs
        self.sol_S_mpq = {}  # existence of the arc(pqk) meaning if there is a setup/switching from machine k at the
        for m in inst.M:
            for p in range(self.beta[m], self.H+1):
                for l in range(1, inst.C_m[m]+1) : #For each slot / maximum number of switches
                    if p + inst.sw_m[m] * l < self.H+1:
                        if (m, p, p + inst.sw_m[m] * l) not in self.sol_S_mpq:
                            self.sol_S_mpq[(m, p, p + inst.sw_m[m] * l)] = False
                            self.nb_all_variables += 1
                            
        # ----- L variables ---- Dummy Arc Variables : for loss arcs
        self.sol_L_mp = {}  # existence of the arc(pk) meaning if there is a loss on machine k at moment p to finish up the graph till H
        for m in inst.M:
            for p in range(self.beta[m], self.H):
                if (m, p) not in self.sol_L_mp:
                    self.sol_L_mp[(m, p)] = False
                    self.nb_all_variables += 1        
        
    def transform_heur_solution_AF_vairables(self, inst):
        #Job sequence
        self.sol_heur_c_max = inst.FmaxUB
        self.sol_ct_machines = [0] * len(inst.M)
        self.sol_seq = [[] for l in inst.M]
        self.sol_st = [[] for i in inst.M]
        self.sol_ct = [[] for k in inst.M]
        self.sol_nb_changes = [0] * len(inst.M)
        # self.sol_tools_planning = []
        self.sol_tools_planning = [[[ False for t in inst.T ] for j in range(len(self.blocks_mj[m].items()))] for m in inst.M]
    
                    
        # The best job and machine where chosen
        for m, blocks_j in self.blocks_mj.items():
            print("Machine %s:"%m)
            ct = 0
            st = 0
            nb_iter = 0
            for j, block in blocks_j.items():
                #add job in the sequence
                self.sol_seq[m].append(j)
                #add tools in the sequence
                for t in block:
                    self.sol_tools_planning[m][nb_iter][t] = True
                
                #Var x
                if(nb_iter > 0): #else is the first job
                    #compare tools with previous tools
                    aux_nb_changes = inst.C_m[m]
                    for t in block:
                        if (self.sol_tools_planning[m][nb_iter-1][t]):
                            aux_nb_changes -= 1
                    #update st and nb of changes
                    st = ct + (aux_nb_changes*inst.sw_m[m])
                    self.sol_nb_changes[m] += aux_nb_changes
                
                #VAR S
                if(st > ct):
                    self.sol_S_mpq[(m, ct, st)] = True
                    print("s[", m, ", ", ct, ", ", st,"] = ", self.sol_S_mpq[(m, ct, st)])
                #update ct
                ct = st + inst.p_mj[m][j]
                self.sol_st[m].append(st)
                self.sol_ct[m].append(ct)
                self.sol_ct_machines[m] = ct
                   
                #VAR X
                self.sol_X_mjpq[(m, j, st, ct)] = True
                print("x[", m, ", ", j, " ,", st, ", ", ct,"] = ", self.sol_X_mjpq[(m, j, st, ct)])
                nb_iter += 1
            
            #end machine                           
            #Tool arcs
            for t in inst.T: #for each tool
                tool_start_time = -1
                tool_end_time = -1
                tool_charged = False
                for i in range(len(self.sol_seq[m])):
                    if self.sol_tools_planning[m][i][t] == 1:
                        if tool_charged:
                            tool_end_time = self.sol_ct[m][i]
                        else:
                            tool_start_time = self.sol_st[m][i]
                            tool_end_time = self.sol_ct[m][i]
                            tool_charged = True
                    else:
                        if tool_charged:
                            self.sol_Y_mtpq[(m, t, tool_start_time, tool_end_time)] = True
                            print("y[", m, ", ", t, ", ", tool_start_time, " ,", tool_end_time, "] = ", self.sol_Y_mtpq[(m, t, tool_start_time, tool_end_time)])
                            tool_charged = False
                if tool_charged:
                    self.sol_Y_mtpq[(m, t, tool_start_time, self.sol_heur_c_max)] = True
                    print("y[", m, ", ", t, ", ", tool_start_time, " ,", self.sol_heur_c_max, "] = ", self.sol_Y_mtpq[(m, t, tool_start_time, self.sol_heur_c_max)])


        # # Print sequence
        # print("===============================")
        # print("Heuristic solution: Cmax =", self.sol_heur_c_max)
        # for k in inst.M:
        #     print("Machine", k, "C[", k, "] =", self.sol_ct_machines[k], "\tNb of changes =", self.sol_nb_changes[k])
        #     print("Seq:\t", end="")
        #     for i in range(len(self.sol_seq[k])):
        #         print(self.sol_seq[k][i], "\t", end="")
        #     print("\nSt :\t", end="")
        #     for i in range(len(self.sol_st[k])):
        #         print(self.sol_st[k][i], "\t", end="")
        #     print("\nCt :\t", end="")
        #     for i in range(len(self.sol_ct[k])):
        #         print(self.sol_ct[k][i], "\t", end="")
        #     print()
            
        # print("===============================")
        # print("Tools!")
        # for k in range(len(self.sol_tools_planning)):
        #     print("Machine", k, "\tNb of changes =", self.sol_nb_changes[k])
        #     for n in range(len(self.sol_tools_planning[k])):
        #         for t in range(len(self.sol_tools_planning[k][n])):
        #             print(int(self.sol_tools_planning[k][n][t]), " ", end="")
        #         print()
        #     print()
        # print("===============================")
        
        print("TRANSFORMATION FINISHED")

    def solve_CG(self, timeLimit=60, OutputFlag=False):
           
        self.model.Params.OutputFlag = OutputFlag
        self.model.params.timeLimit = timeLimit
        self.model.optimize()
        print("Status CG: ", self.model.status)
        if self.model.status in [2,9]:
            self.model.write("AF_CG_IRMP_solution.sol")
            return self.model.ObjVal
        else:
            if self.model.status == 3:
                print("Model is infeasible!!!")
                self.model.computeIIS()
                self.model.write("AF_CG_IRMP.ilp")
            return -1
        
    def solve(self, timeLimit=60, OutputFlag=True, relax=False):
        if relax:
            self.relaxedModel = self.model.relax()
            self.relaxedModel.Params.OutputFlag = OutputFlag
            self.relaxedModel.params.timeLimit = timeLimit
            self.relaxedModel.optimize()
            return self.relaxedModel.ObjVal
        else:
            self.model.Params.OutputFlag = OutputFlag
            self.model.params.timeLimit = timeLimit
            self.model.relax()
            self.model.optimize()
            print("Status: ", self.model.status)
            if self.model.status in [2,9]:
                self.model.write("AF_CG_IRMP_solution.sol")
                return self.model.ObjVal
            else:
                if self.model.status == 3:
                    print("Model is infeasible!!!")
                    self.model.computeIIS()
                    self.model.write("AF_CG_IRMP.ilp")
                return -1

    def pricing_calculate_reduced_costs(self, inst):
        #----- X variables ---- Arc Variables for Job processing
        self.neg_rc_X_mjpq = []
        
        for m in inst.M:
            for j in inst.J:
                if len(inst.T_j[j]) <= inst.C_m[m]: #if there is enough slots on machine m for job j
                    for p in range(0, self.H - inst.p_mj[m][j] +1):
                        q  = p + inst.p_mj[m][j]
                        if self.sol_X_mjpq[(m, j, p, q)] == False:
                            #calculate RC
                            rc = -self.alpha_j[(j)].pi -self.beta_mq[(m,p)].pi + self.beta_mq[(m,q)].pi - q*self.epsilon_j[(j)].pi
                            for t in inst.T_j[j]:
                                rc -= self.tau_mjtq[(m,j,t,p)].pi
                            #Check if RC is negatif                                
                            if rc < 0:
                                aux_rc = [m, j, p, q, rc]
                                self.neg_rc_X_mjpq.append(aux_rc)
                                # print('rc_X[%s][%s][%s][%s] = %s'%(m, j, p, q, rc))

        #----- Y variables ---- Arc Variables for Tool loading
        self.neg_rc_Y_mtpq = []
        
        for m in inst.M:
            for t in inst.T:
                for p in range(0, self.H + 1):                                                  
                    for q in range(p + 1, self.H + 1):
                        if self.sol_Y_mtpq[(m, t, p, q)] == False:
                            #calculate RC
                            rc = -self.gamma_mq[(m,p)].pi + self.gamma_mq[(m,q)].pi
                            if p > 0 and p < self.H:
                                rc -= self.theta_mq[(m,p)].pi
                            for j in inst.J_t[t]:
                                for a in range(p, q - inst.p_mj[m][j]+1):
                                    rc -= self.tau_mjtq[(m, j, t, a)].pi
                            #Check if RC is negatif
                            if rc < 0:
                                aux_rc = [m, t, p, q, rc]
                                self.neg_rc_Y_mtpq.append(aux_rc)
                                # print('rc_Y[%s][%s][%s][%s] = %s'%(m, t, p, q, rc))
                    
        #----- S variables ---- Variables for tool switching operations / Set_UP arcs
        self.neg_rc_S_mpq = []
        
        for m in inst.M:
            for p in range(self.beta[m], self.H+1):
                for l in range(1, inst.C_m[m]+1) : #For each slot / maximum number of switches
                    q = p + inst.sw_m[m] * l    
                    if q < self.H+1:
                        if self.sol_S_mpq[(m, p, q)] == False:
                            #calculate RC
                            rc = -self.beta_mq[(m,p)].pi + self.beta_mq[(m,q)].pi - l*self.gamma_mq[(m,p)].pi + l*self.gamma_mq[(m,q)].pi
                            if q > 0 and q < self.H:
                                rc += l*self.theta_mq[(m,q)].pi
                            #Check if RC is negatif
                            if rc < 0:
                                aux_rc = [m, p, q, rc]
                                self.neg_rc_S_mpq.append(aux_rc)
                                # print('rc_S[%s][%s][%s] = %s'%(m, p, q, rc))

    
    def addColumns(self, inst):
        new_added_cols = 0
        #Columns X
        # for i in range(0,min(1,len(self.neg_rc_X_mjpq))):
        for i in range(0,len(self.neg_rc_X_mjpq)):
            mach = self.neg_rc_X_mjpq[i][0]
            job = self.neg_rc_X_mjpq[i][1]
            st = self.neg_rc_X_mjpq[i][2]
            ct = self.neg_rc_X_mjpq[i][3]
            rc = self.neg_rc_X_mjpq[i][4]
            #This variable appears in these constraints:
            col = Column()
            col.addTerms(1.0, self.alpha_j[(job)])
            col.addTerms(1.0, self.beta_mq[(mach,st)])
            col.addTerms(-1.0, self.beta_mq[(mach,ct)])
            col.addTerms(-ct, self.epsilon_j[(job)])
            for tool in inst.T_j[job]:
                col.addTerms(-1.0, self.tau_mjtq[(mach, job, tool, st)])
            #Add new column to the model
            self.model.addVar(vtype=GRB.CONTINUOUS, obj=0.0, lb=0.0, ub=1.0, column=col, name='_X[%s][%s][%s][%s]'%(mach, job, st, ct) )
            self.sol_X_mjpq[(mach, job, st, ct)] = True
            new_added_cols += 1

        #Columns Y
        # for i in range(0,min(1,len(self.neg_rc_Y_mtpq))):
        for i in range(0,len(self.neg_rc_Y_mtpq)):
            mach = self.neg_rc_Y_mtpq[i][0]
            tool = self.neg_rc_Y_mtpq[i][1]
            st = self.neg_rc_Y_mtpq[i][2]
            ct = self.neg_rc_Y_mtpq[i][3]
            rc = self.neg_rc_Y_mtpq[i][4]
            #This variable appears in these constraints:
            col = Column()
            col.addTerms(1.0, self.gamma_mq[(mach,st)])
            col.addTerms(-1.0, self.gamma_mq[(mach,ct)])
            if st > 0 and st < self.H:
                col.addTerms(-1.0, self.theta_mq[(mach,st)])
            for job in inst.J_t[tool]:
                for a in range(st, ct - inst.p_mj[mach][job]+1):
                    col.addTerms(-1.0, self.tau_mjtq[(mach, job, tool, a)])
            #Add new column to the model
            self.model.addVar(vtype=GRB.CONTINUOUS, obj=0.0, lb=0.0, ub=1.0, column=col, name='_Y[%s][%s][%s][%s]'%(mach, tool, st, ct) )
            self.sol_Y_mtpq[(mach, tool, st, ct)] = True
            new_added_cols += 1
    
         #Columns S
        # for i in range(0,min(1,len(self.neg_rc_S_mpq))):
        for i in range(0,len(self.neg_rc_S_mpq)):
            mach = self.neg_rc_S_mpq[i][0]
            st = self.neg_rc_S_mpq[i][1]
            ct = self.neg_rc_S_mpq[i][2]
            rc = self.neg_rc_S_mpq[i][3]
            l = (ct-st)/inst.sw_m[mach]
            #This variable appears in these constraints:
            col = Column()
            col.addTerms(1.0, self.beta_mq[(mach,st)])
            col.addTerms(-1.0, self.beta_mq[(mach,ct)])
            col.addTerms(l, self.gamma_mq[(mach,st)])
            col.addTerms(-l, self.gamma_mq[(mach,ct)])
            if ct > 0 and ct < self.H:
                col.addTerms(-l, self.theta_mq[(mach,ct)])
            #Add new column to the model
            self.model.addVar(vtype=GRB.CONTINUOUS, obj=0.0, lb=0.0, ub=1.0, column=col, name='_S[%s][%s][%s]'%(mach, st, ct) )
            self.sol_S_mpq[(mach, st, ct)] = True
            new_added_cols += 1
       
        
        return new_added_cols






                 
    def getDuals(self, inst):
        dual_mkt = {}
        for m in inst.M:
            dual_mkt[m] = {}
            for k in range(1,self.Km[m]):
                dual_mkt[m][k] = {}
                for t in self.W[m][k].keys():
                    dual_mkt[m][k][t] = self.relaxedModel.getAttr("Pi", [self.relaxedModel.getConstrByName('loading_on_machine_m%s_position_k%s_tool_t%s'%(m,k,t))])[0]
            
        dual_j = {}
        for j in inst.J:
            dual_j[j] = self.relaxedModel.getAttr("Pi", [self.relaxedModel.getConstrByName('exactly_one_block_j%s'%j)])[0]
        return dual_j, dual_mkt
    
    
    def KTNS(self, inst):
        for m in inst.M:
            magazine = {}       # sequence of magazine state
            T = {}              # tools' required positions
            for k in self.X[m].keys():
                for j in inst.p_mj[m].keys():
                    for b, block in inst.Bmj[m][j].items():
                        if self.X[m][k][b].x > 0:
                            magazine[k] = []
                            for t in inst.T_j[j]:
                                magazine[k].append(t)
                                if t not in T:
                                    T[t] = [k]
                                else:
                                    T[t].append(k)
            NbSwitches = 0
            for k in magazine.keys():
                while (len(magazine[k]) < inst.C_m[m]) and (len(T.keys()) > 0):
                    soonest_k = len(magazine.keys())
                    best_t = -1
                    for t in T.keys():
                        if (t not in magazine[k]) and (soonest_k < T[t][0]):
                            soonest_k = T[t][0]
                            best_t = t
                    if (soonest_k < len(magazine.keys())) and (best_t >= 0):
                        magazine[k].append(best_t)
                        
                if k > 0:
                    NbSwitches += len([t for t in magazine[k] if t not in magazine[t-1]])
    
        return True
    
    
    def plotSolution(self,inst, name=""):
        if self.model.status in [2,9]:
            f, ax = plt.subplots(figsize=(10,10))
            ax.set_xlim([0,self.Fmax.x])
            ax.set_ylim([0,sum([c+2 for c in inst.C_m.values()])])

            y = 0
            for m in inst.M:
                x = 0
                lastb = 0
                for k in self.X[m].keys():
                    for t in self.W[m][k].keys():
                        if self.W[m][k][t].x > 0:
                            #print("W[m%s][k%s][t%s] = %s"%(m,k,t,self.W[m][k][t].x))
                            length = inst.sw_m[m]
                            ax.add_patch(Rectangle((x,y), length, 1, fc="red", ec="black", alpha=0.3))
                            plt.text(x+length/3,y + 0.33,"t"+str(t))
                            x += length
                    for j in inst.p_mj[m].keys():
                        for b, block in inst.Bmj[m][j].items():
                            if self.X[m][k][b].x > 0:
                                #print("X[m%s][k%s][b%s]"%(m,k,b), block)
                                lastb = b
                                length = inst.p_mj[m][j]
                                ax.add_patch(Rectangle((x,y), length, 1, fc="green", ec="black", alpha=0.3))
                                plt.text(x+length/3,y + 0.33,"j"+str(j))
                                
                                yt = y+1
                                for t in block:
                                    ax.add_patch(Rectangle((x,yt), length, 1, fc="yellow", ec="black", alpha=0.3))
                                    plt.text(x+length/3,yt + 0.33,"t"+str(t))
                                    yt += 1
                                x += length
                y += inst.C_m[m] + 2
                                
            f.savefig(name + "sol.png")
                    

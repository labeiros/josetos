from gurobipy import *
from instance import *
from machineKP_MILP import *



################################################################################
#####                           Class BlockILP()                           #####
################################################################################
class Block_MILP(MILP):
    ##### Constructor
    def __init__(self, model_name, inst, bounded=True, improved=True):
        ##### Attributes
        self.name = model_name
        self.model = Model(self.name)      # model
        
        self.obj_value = 0              # objective value of MILP
  
        #----- dict of nb. positions k per m
        self.K_m = {m:len(inst.J) for m in inst.M}
        if improved:
            for m in inst.M:
                KP = MachineKP_MILP(inst, m)
                self.K_m[m] = int(KP.solve(outputFlag = False) )
                print("KP: |K_%s| = %s"%(m,self.K_m[m]))


        self.X = {}                     # X_{m,k}^b = 1 if block b is used for kth job computed on machine m
        self.W = {}                     # W_{m,k,t} = 1 if tool t is loaded at position k on machine m
        if bounded:
            self.Fmax = self.model.addVar(lb=inst.FmaxLB, ub=inst.FmaxUB, obj=1, vtype=GRB.INTEGER, name='Fmax')        # makespan
        else:
            self.Fmax = self.model.addVar(obj=1, vtype=GRB.INTEGER, name='Fmax')        # makespan
        
        for m in inst.M:
            self.X[m] = {}
            self.W[m] = {}
            for k in range(self.K_m[m]):   # k <= number of jobs computables on machine m
                self.X[m][k] = {}
                self.W[m][k] = {}
                for j in inst.B_mj[m].keys():
                    #self.X[m][k][j] = {}
                    for b in inst.B_mj[m][j].keys():
                        self.X[m][k][b] = self.model.addVar(obj=0, vtype=GRB.BINARY, name='X[%s][%s][%s]'%(m,k,b))
                        for t in inst.B_mj[m][j][b]:
                            if t not in self.W[m][k].keys():
                                self.W[m][k][t] = self.model.addVar(obj=0, vtype=GRB.BINARY, name='W[%s][%s][%s]'%(m,k,t))
        
        #print("gamma:",self.gamma)
        
        #print("----------------")
        
        #print("X:",self.X)
        
        for m in inst.M:
            self.model.addLConstr( quicksum(self.X[m][0][b] for b in self.X[m][0].keys()) <= 1,'at_most_one_block_m%s_k0'%m )
            for k in range(1,self.K_m[m]):
                for t in self.W[m][k].keys():
                    self.model.addLConstr( self.W[m][k][t] >= quicksum(self.X[m][k][b] - self.X[m][k-1][b] for j in inst.B_mj[m].keys() for b, block in inst.B_mj[m][j].items() if t in block),'loading_on_machine_m%s_position_k%s_tool_t%s'%(m,k,t) )
                #for b in self.X[m][k].keys():
                    #self.model.addLConstr( self.gamma[m][k] >= quicksum(inst.S[b2][b] * (self.X[m][k][b] + self.X[m][k-1][b2] -1) for b2 in self.X[m][k-1].keys() if b in inst.S[b2].keys()),'nb_Switches_m%s_k%s_b%s'%(m,k,b) )
                
                self.model.addLConstr( quicksum(self.X[m][k][b] for b in self.X[m][k].keys()) <= 1,'at_most_one_block_m%s_k%s'%(m,k) )
                self.model.addLConstr( quicksum(self.X[m][k][b] - self.X[m][k-1][b] for b in self.X[m][k].keys()) <= 0,'precedence_m%s_k%s'%(m,k) )
            #if len(inst.p_mj[m]) > 0:
                #self.model.addConstr( quicksum(self.X[m][1][b] for b in self.X[m][1].keys()) >= 1,'at_least_one_block_m%s'%m )

            self.model.addLConstr( self.Fmax >=  quicksum( inst.sw_m[m]*self.W[m][k][t] for k in self.W[m].keys() for t in self.W[m][k].keys()) + quicksum(inst.p_mj[m][j]*self.X[m][k][b] for j in inst.B_mj[m].keys() for k in self.X[m].keys() for b in self.X[m][k].keys() if b in inst.B_mj[m][j].keys()))
            
        for j in inst.J:
            self.model.addLConstr( quicksum(self.X[m][k][b]  for m in self.X.keys() if j in inst.B_mj[m].keys() for k in self.X[m].keys() for b in inst.B_mj[m][j].keys()) == 1,'exactly_one_block_j%s'%j )
            
        self.model.setObjective( self.Fmax, sense=GRB.MINIMIZE )
        
        self.relaxedModel = self.model.relax()
        self.model.write("blockModel.lp")

    
    def addColumn(self, newBlock):
        
        return True

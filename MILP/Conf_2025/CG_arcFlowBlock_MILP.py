from gurobipy import *
from instance import *
from MILP import *
from machineKP_MILP import *
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle




################################################################################
#####                         Class ArcFlowBlock_MILP()                         #####
################################################################################
class ArcFlowBlock_MILP_CG(MILP):

    ##### Constructor
    def __init__(self, inst, blocks_mj, model_name = "AF_Block_Position_MILP_CG", bounded = True):

        # super().__init__(modelName)       
        ##### Attributes
        self.bounded = bounded
        self.name = model_name
        self.model = Model(self.name)      # model
        self.obj_value = 0                 # objective value of MILP 

        self.blocks_mj = blocks_mj
        self.addedColumns = 0
        
        self.Km = {}
        for m in inst.M:
            KP = MachineKP_MILP(inst, m)
            self.Km[m] = int(KP.solve())       # maximum number of job without exceeding makespan upper bound
        for m in inst.M:
            print("Km[%s]:"%m, self.Km[m])
            
        #store columns: initial and new ones
        self.initi_columns_m  = {}
        self.initi_columns_j  = {}
        self.initi_columns_p  = {}
        self.initi_columns_ts = {}
        self.new_columns_m  = {}
        self.new_columns_j  = {}
        self.new_columns_p  = {}
        self.new_columns_ts = {}

        #Initialize structure
        self.initialize_AFBlock_CG_structure(inst)

        #Fill structure with initial solution information
        self.transform_heur_solution_AF_Block_vairables(inst)
        
        #Create IRMP
        self.create_AFBlock_IRMP_model(inst)
        
        # #Solve column generation
        # lb, nb_iter, nb_cols = self.solve_CG_block(inst)

    def storeColumn(self, inst, m, j, p, block, id_block, col_type):
        if(col_type == 0): #initial
            self.initi_columns_m[id_block]  = m
            self.initi_columns_j[id_block]  = j
            self.initi_columns_p[id_block]  = p
            self.initi_columns_ts[id_block] = block
        else: #new added
            self.new_columns_m[id_block] = m
            self.new_columns_j[id_block] = j
            self.new_columns_p[id_block] = p
            self.new_columns_ts[id_block] = block

    def printColumn(self, inst, m, j, p, block, id_block):
        print("id:\t", id_block, "\tm\t", m, "\tj\t",j,"\tp\t", p,"\tblock\t", block,"\n")

    def initialize_AFBlock_CG_structure(self, inst):
        self.nb_all_variables = 0
        self.H = inst.FmaxUB
        
        #Structure to count the number of blocks per machine, job and position
        self.nb_blocks_mpj = {}
        for m in inst.M:    #for each machine
            self.nb_blocks_mpj[m] = {}
            for p in range(self.Km[m]):   # k <= max number of jobs on machine m
                self.nb_blocks_mpj[m][p] = {}
                for j in inst.J:     #for each job
                    self.nb_blocks_mpj[m][p][j] = 0
         
        #Structure to define the blocks per machine, job and position 
        self.id_mpj_b = {}
        self.sol_val_mpj_b = {}
        self.list_of_tools_mpjs_b = {}
        for m in inst.M:    #for each machine
            self.id_mpj_b[m] = {}
            self.sol_val_mpj_b[m] = {}
            self.list_of_tools_mpjs_b[m] = {}
            for p in range(self.Km[m]):   # k <= max number of jobs on machine m
                self.id_mpj_b[m][p] = {}
                self.sol_val_mpj_b[m][p] = {}
                self.list_of_tools_mpjs_b[m][p] = {}
                for j in inst.J:     #for each job
                    self.id_mpj_b[m][p][j] = {}
                    self.sol_val_mpj_b[m][p][j] = {}
                    self.list_of_tools_mpjs_b[m][p][j] = {}
                    # for s in range(inst.C_m[m]):   # s number of slots per machine
                    for s in inst.T:
                        self.list_of_tools_mpjs_b[m][p][j][s] = {}

    def transform_heur_solution_AF_Block_vairables(self, inst): #get blocks
        nb_blocks = 0
        for m, blocks_j in self.blocks_mj.items():
            pos = 0        
            for j, block in blocks_j.items():
                #consider a new block of job j in position pos of machine m
                newB = self.nb_blocks_mpj[m][pos][j] #index of new block               
                self.id_mpj_b[m][pos][j][newB] = nb_blocks
                self.sol_val_mpj_b[m][pos][j][newB] = 1.0

                for s in inst.T: #initially, the tools are not charged
                    self.list_of_tools_mpjs_b[m][pos][j][s][newB] = False
                #add tools in the sequence
                for t in block:
                    self.list_of_tools_mpjs_b[m][pos][j][t][newB] = True

                 #updating number of blocks created and position
                self.nb_blocks_mpj[m][pos][j] += 1
                pos +=1
                nb_blocks += 1
    

        #Structure to define the blocks per machine, job and position 
        print("=========== SOL ===============")
        # self.list_of_tools_mpjs_b[0][1][3][3][0] = False
        # self.list_of_tools_mpjs_b[0][1][3][0][0] = True
        print("Fmax = %s"%inst.FmaxUB)
        for m in inst.M:    #for each machine
            print("Machine %s:"%m)
            for p in range(self.Km[m]):   # k <= max number of jobs on machine m
                # print("\tPosition %s:"%p)
                for j in inst.J:     #for each job
                    # print("nb blocks = ", len(self.sol_val_mpj_b[m][p][j]))
                    nbB = len(self.sol_val_mpj_b[m][p][j])
                    for i in range(nbB):
                        if(self.sol_val_mpj_b[m][p][j][i] > 0.5): #if block is used
                            # if(j == 0):                                          #REMOVE REMOVE REMOVE REMOVE
                            #     self.list_of_tools_mpjs_b[m][p][j][0][i] = False #REMOVE REMOVE REMOVE REMOVE
                            #     self.list_of_tools_mpjs_b[m][p][j][9][i] = True  #REMOVE REMOVE REMOVE REMOVE
                            print("\tJob", j, ": {", end="")
                            for s in inst.T:
                                if ( self.list_of_tools_mpjs_b[m][p][j][s][i] == True ):
                                    print(" ", s, end="")
                            print("}")
        print("===============================")
        print("TRANSFORMATION FINISHED")

    def create_AFBlock_IRMP_model(self, inst):

        #Initialize auxiliary values
        beta = [0] * len(inst.M)
        for m in inst.M:
            beta[m] = 99999999
            for j in inst.J:
                if len(inst.T_j[j]) <= inst.C_m[m]:
                    if inst.p_mj[m][j] < beta[m]:
                        beta[m] = inst.p_mj[m][j]

        #Create variables
        variables = 0
        constraints = 0
        
        # ----- C_max variable : For the objective Function
        if self.bounded:
            self.H = inst.FmaxUB
            self.C_max = self.model.addVar(lb=inst.FmaxLB, ub=inst.FmaxUB, obj=1, vtype=GRB.CONTINUOUS, name='C_max')  # makespan
            variables += 1
        else:
            print("AF_Block_Position_CG needs an initial UB to run!!")
            exit(0)
        
        print("AF_Block_PositionCG: creating IRMP...")
        print("Initial UB = ", self.H)
    
    
        #----- X variables ---- Variables for Job processing and tools loading
        nb_init_cols_x = 0
        self.X = {}   # X_{m,k}^b = 1 if block b is used for kth position on machine m
        for m in inst.M:
            self.X[m] = {}
            for k in range(self.Km[m]):   # k <= max number of jobs on machine m
                self.X[m][k] = {}
                for j in inst.J:     #for each job
                    self.X[m][k][j] = {}
                    nbB = len(self.sol_val_mpj_b[m][k][j])
                    for b in range(nbB):
                        self.X[m][k][j][b] = self.model.addVar(obj=0, lb=0, ub=1, vtype=GRB.CONTINUOUS, name='X[%s][%s][%s][%s]'%(m,k,j,b))
                        
                        # #Create block to be stored
                        # block = {}
                        # storeColumn(inst, m, j, k, block, nb_init_cols_x, 0)
                        # self.list_of_tools_mpjs_b[m][p][j][t][b] == True
                        # nb_init_cols_x += 1
                        
                        variables += 1
        #----- Y variables ---- Variables for tools switching
        self.Y = {}   # Y_{m,k,t} = 1 if tool t is loaded at position k and not at position k+1 on machine m
        for m in inst.M:
            self.Y[m] = {}
            for k in range(self.Km[m]):   # k <= max number of jobs on machine m
                self.Y[m][k] = {}
                for t in inst.T:    # for each tool
                    if (m, k, t) not in self.Y:
                    # if t not in self.Y[m][k].keys():
                        self.Y[m][k][t] = self.model.addVar(obj=0, lb=0, ub=1, vtype=GRB.CONTINUOUS, name='Y[%s][%s][%s]'%(m,k,t))
                        variables += 1
        
        #----- L variables ---- Loss arc variables
        self.L = {}   # L_{m,k} = 1 if loss arc used at position k on machine m
        for m in inst.M:
            self.L[m] = {}
            for k in range(self.Km[m]):   # k <= max number of jobs on machine m
                print("m: ", m , "\tk: ", k)
                if (m, k) not in self.L:
                    self.L[m][k] = self.model.addVar(obj=0, lb=0, ub=1, vtype=GRB.CONTINUOUS, name='L[%s][%s]'%(m,k))
                    variables += 1        
        
        #Update model
        self.model.update()
        print(variables, " variables created !!")
        
        ############################## Create_constraints ##############################
        #Initially, create structure for dual variables
        self.alpha_j = {}
        self.beta_mp = {}
        self.gamma_mpt = {}
        self.lambda_m = {}
        #Create strucutre for reduced costs
        self.rc_X_mpjb = {}
        
        # Constraint_1 - each job should be scheduled
        for j in inst.J:
            self.alpha_j[(j)] = self.model.addLConstr( quicksum(self.X[m][p][j][b] for m in inst.M for p in range(self.Km[m]) for b in range(len(self.X[m][p][j])) ) >= 1, 'C1_%s'%j )
            constraints += 1
            
        # Constraint_2 - flow conservation
        for m in inst.M:
            for p in range(self.Km[m]+1):
                rhs = 0
                if(p == 0):
                    rhs = 1
                    self.beta_mp[(m,p)] = self.model.addLConstr( self.L[m][p] + quicksum(self.X[m][p][j][b] for j in inst.J for b in  range(len(self.X[m][p][j])) ) == rhs,'C2_%s_%s'%(m,p) )
                elif(p == self.Km[m]):    # if last position
                    rhs = -1 + quicksum(self.L[m][k] for k in range(self.Km[m]) )
                    self.beta_mp[(m,p)] = self.model.addLConstr( -quicksum(self.X[m][p-1][j][b] for j in inst.J for b in range(len(self.X[m][p-1][j])) ) == rhs,'C2_%s_%s'%(m,p) )
                else:
                    self.beta_mp[(m,p)] = self.model.addLConstr( self.L[m][p] + quicksum(self.X[m][p][j][b] for j in inst.J for b in range(len(self.X[m][p][j]))) - quicksum(self.X[m][p-1][j][b] for j in inst.J for b in range(len(self.X[m][p-1][j])) ) == rhs,'C2_%s_%s'%(m,p) )
                    
                constraints += 1
       
        # Constraint_3 - number of switches constraints
        for m in inst.M:
            for p in range(0,self.Km[m]-1):
                # for t in self.Y[m][k].keys():
                for t in inst.T:
                    # self.model.addLConstr( self.Y[m][p][t] + self.L[m][p+1] + quicksum(-self.X[m][p][j][b] + self.X[m][p+1][j][b] for j in inst.J for b in len(self.X[m][p][j]) self.list_of_tools_mpjs_b[m][p][j][s][i] == True           if t in block) >= 0,'loading_on_machine_m%s_position_k%s_tool_t%s'%(m,k,t) )
                    self.gamma_mpt[(m,p,t)] = self.model.addLConstr( self.Y[m][p][t] + self.L[m][p+1] - quicksum(self.X[m][p][j][b] for j in inst.J for b in range(len(self.X[m][p][j])) if self.list_of_tools_mpjs_b[m][p][j][t][b] == True) + quicksum(self.X[m][p+1][j][b] for j in inst.J for b in range(len(self.X[m][p+1][j])) if self.list_of_tools_mpjs_b[m][p+1][j][t][b] == True) >= 0,'C3_m%s_%s_%s'%(m,p,t) )                        
                    constraints += 1                    
        
        # Constraint_4 - makespan constraint
        for m in inst.M:
            self.lambda_m[(m)] = self.model.addLConstr( self.C_max -  quicksum( inst.sw_m[m]*self.Y[m][p][t] for p in range(self.Km[m]) for t in inst.T) - quicksum(inst.p_mj[m][j]*self.X[m][p][j][b] for p in range(self.Km[m]) for j in inst.J for b in  range(len(self.X[m][p][j])) ) >= 0,'C4_m%s'%(m))
            constraints += 1
        
        print(constraints, " constraints created !!")
        
        # Objective function  
        self.model.setObjective( self.C_max, sense=GRB.MINIMIZE )
        
        #Update model
        self.model.update()
        
        self.relaxedModel = self.model.relax()
        #self.model.write("CG_LP/IRMP_AF_Block_Position.lp")

    def solve_CG_RMP(self, timeLimit=60, OutputFlag=False):
        self.model.Params.OutputFlag = OutputFlag
        self.model.params.timeLimit = timeLimit
        self.model.params.threads = 1
        self.model.optimize()
        print("Status CG: ", self.model.status)
        if self.model.status in [2,9]:
            self.model.write("CG_LP/AFBlock_CG_IRMP_solution.sol")
            return self.model.ObjVal
        else:
            if self.model.status == 3:
                print("Model is infeasible!!!")
                self.model.computeIIS()
                self.model.write("AFBlock_CG_IRMP.ilp")
            return -1

    def pricing_AFBlock(self, inst):
        best_rc = 0
        best_m = -1
        best_j = -1
        best_p = -1
        best_tools = {}
        
        for m in inst.M:
            #print("Machine ", m)
            #print("\tlambda = ", self.lambda_m[m].pi)
            rc_jp = {}
            x_jpt = {}
            for j in inst.J:
                for p in range(self.Km[m]):
                    for t in inst.T:
                        x_jpt[(j,p,t)] = False
            
            delta_gamma_t = []
            for p in range(self.Km[m]): #from 0 to Km[m]-1
                # print("\tPosition ", p)
                delta_beta = -self.beta_mp[(m,p)].pi + self.beta_mp[(m,p+1)].pi
                # print("\t\tBeta: m = ", m ," p = ", p, ": ", self.beta_mp[(m,p)].pi)
                # print("\t\tDelta beta: m = ", m ," p = ", p, ": ", delta_beta)
                for t in inst.T: #calculate aux_delta for each tool
                    aux_delta = 0
                    if p < self.Km[m]-1: #from 0 to Km[m] -2
                        aux_delta += self.gamma_mpt[(m,p,t)].pi
                    if p > 0: #from 1 to Km[m]-1
                        aux_delta -= self.gamma_mpt[(m,p-1,t)].pi
                    delta_gamma_t.append( (t, aux_delta) )

                #Sort delta gamma values
                #print("\t\tDelta gamma: m = ", m ," p = ", p, ": ", delta_gamma_t)
                delta_gamma_t = sorted(delta_gamma_t, key = lambda x: x[1])
                #print("\t\tDelta gamma sorted: m = ", m ," p = ", p, ": ", delta_gamma_t)

                for j in inst.J:
                    if len(inst.T_j[j]) <= inst.C_m[m]: #if there is enough space for scheduling job j on machine m
                        rc_jp[(j,p)] = -self.alpha_j[j].pi + inst.p_mj[m][j]*self.lambda_m[m].pi + delta_beta 
                        for t in inst.T_j[j]: #tools needed by this job j
                            x_jpt[(j,p,t)] = True #this tool should be taken anyway
                            if p < self.Km[m]-1: #from 0 to Km[m] -2
                                rc_jp[(j,p)] += self.gamma_mpt[(m,p,t)].pi
                            if p > 0: #from 1 to Km[m]-1
                                rc_jp[(j,p)] -= self.gamma_mpt[(m,p-1,t)].pi

                        #Selecting best tools for job j position p
                        if inst.C_m[m] - len(inst.T_j[j]) > 0:
                            counter = 0
                            for a, b in delta_gamma_t:
                                if(x_jpt[(j,p,a)] == False):
                                    x_jpt[(j,p,a)] = True
                                    rc_jp[(j,p)] += b
                                    counter += 1
                                    if counter == inst.C_m[m] - len(inst.T_j[j]):
                                        break
                        
                        #print("\t\tRC[",m,"][",p,"][",j,"] = ", rc_jp[(j,p)], "  (alpha = ", self.alpha_j[j].pi, ")")
                        #store if best so far
                        if rc_jp[(j,p)] < best_rc:
                            best_rc = rc_jp[(j,p)]
                            best_m = m
                            best_j = j
                            best_p = p
                            for t in inst.T:
                                best_tools[(t)] = x_jpt[(j,p,t)]
                
                #when finish a machine
                delta_gamma_t.clear()
                
        return best_rc, best_m, best_j, best_p, best_tools
        # return rc_jp, x_jpt

    def addColumns_block(self, inst, m, j, p, block, id_block):
        new_added_cols = 0
        
        #This variable appears in these constraints:
        col = Column()
        #Alpha
        col.addTerms(1.0, self.alpha_j[(j)])
        #Beta
        #if p >=0 self.Km[m] -1:
        col.addTerms(1.0, self.beta_mp[(m,p)])
        # if p < self.Km[m] -1:
        if p+1 <= self.Km[m]:
            col.addTerms(-1.0, self.beta_mp[(m,p+1)])
        #Gamma   
        for t in block:
            if block[t] == True:
                if p < self.Km[m]-1:
                    col.addTerms(-1.0, self.gamma_mpt[(m,p,t)])
                if p > 0:
                    col.addTerms(1.0, self.gamma_mpt[(m,p-1,t)])
        #Lambda
        col.addTerms(-inst.p_mj[m][j], self.lambda_m[m])
        
        #Add new column to the model
        self.model.addVar(vtype=GRB.CONTINUOUS, obj=0.0, lb=0.0, ub=1.0, column=col, name='_X[%s][%s][%s][%s]'%(m, p, j, id_block) )
        # self.sol_X_mjpq[(mach, job, st, ct)] = True
        new_added_cols += 1
        self.model.update();
       
        return new_added_cols

    def printSol_RC(self):
        for variable in self.model.getVars():
            print(variable.getAttr(GRB.Attr.VarName), " Val = ", variable.getAttr(GRB.Attr.X), " RC = ", variable.getAttr(GRB.Attr.RC) )
        print()
        
    def printDuals(self):
        for constraint in self.model.getConstrs():
            print(constraint.getAttr(GRB.Attr.ConstrName), " Sense = ", constraint.getAttr(GRB.Attr.Sense), " RHS = ", constraint.getAttr(GRB.Attr.RHS), " Dual = ", constraint.getAttr(GRB.Attr.Pi) )
        print()
 
    def solve_CG_block(self, inst):
        lb = -1
        new_cols = 1
        iteration = 0
        tot_added_cols = 0
        while new_cols > 0: #tot_cols > 0:
        # while iteration < 1000:
            print("=================================\titeration ", iteration, "\t=================================")
            lb = self.solve_CG_RMP()
            # print("=================================\tMaster problem solved!\t=================================")
            # self.printDuals()
            # print("=================================\tDuals printed!\t=================================")
            # self.printSol_RC()
            # print("=================================\tReduced costs printed!\t=================================")
            print ("\tlb: ", lb)
            print("=================================\tSolving pricing problem...\t=================================")
            best_rc, best_m, best_j, best_p, best_block = self.pricing_AFBlock(inst)
            print("Best col: Rc = ", best_rc, " mach: ", best_m, " j: ", best_j, " pos: ", best_p, " Block: ", best_block)
            #Print columns in file, if needed for debug
            # with open('added_columns.txt', 'a') as f:
            #     f.write(str(best_rc))
            #     f.write("\t")
            #     f.write(str(best_m))
            #     f.write("\t")
            #     f.write(str(best_j))
            #     f.write("\t")
            #     f.write(str(best_p))
            #     f.write("\t")
            #     f.write(str(best_block))
            #     f.write("\n")
            
            print("=================================\tPricing problem solved!\t=================================")
            new_cols = 0
            if best_rc < -0.0000001:
                self.storeColumn(inst, best_m, best_j, best_p, best_block, iteration, 1)
                print("=================================\tAdding new columns...\t=================================")
                new_cols = self.addColumns_block(inst, best_m, best_j, best_p, best_block, iteration)
            #     new_cols = AF.addColumns(inst)
                print("\tNb new columns added: nb = ", new_cols)
                tot_added_cols += new_cols
                #self.model.write("CG_LP/_AF_CG_IRMP_%s.lp"%iteration)
                print("=================================\titeration ", iteration, " done!! ===========================")
                iteration += 1
            else:
                print("\tNo more negative reduced cost column exists!!")
                            
        print("\n=================================")
        print("Done in: ", iteration, " iterations")
        print(tot_added_cols, " columns added")
        
        # #Print all generated columns
        # for i in range(len(self.new_columns_m)):
        #     self.printColumn(inst, self.new_columns_m[i], self.new_columns_j[i], self.new_columns_p[i], self.new_columns_ts[i], i)
            
        print("=================================")
        
        return lb, iteration, tot_added_cols
 
    def getDuals(self, inst):
        
        dual_j = {}
        # TODO
        return dual_j
    
    # def plotSolution(self, inst, name=""):
        if self.model.status in [2,9]:
            f, ax = plt.subplots(figsize=(10,10) )
            ax.set_xlim([0,self.Fmax.x])
            ax.set_ylim([0,sum([c+2 for c in inst.Cm.values()])])
            
            # TODO
            
            f.savefig(name + "sol.png")
                    

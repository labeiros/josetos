from gurobipy import *
from instance import *
from MILP.MILP import *

from machineKP_MILP import *
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle




################################################################################
#####                         Class ArcFlowBlock_MILP()                         #####
################################################################################
class ArcFlowBlock_MILP(MILP):

    ##### Constructor
    def __init__(self, inst, model_name = "AF_Block_Position_MILP", bounded = True):
        # super().__init__(modelName)
        ##### Attributes
        self.name = model_name
        self.model = Model(self.name)      # model
        self.obj_value = 0                 # objective value of MILP 
        
        self.Km = {}
        for m in inst.M:
            KP = MachineKP_MILP(inst, m)
            self.Km[m] = int(KP.solve() )       # maximum number of job without exceeding makepsan upper bound
            print("Km[%s]:"%m, self.Km[m])

        #Create variables
        variables = 1
        constraints = 0
             
        #Initialize auxiliary values
        beta = [0] * len(inst.M)
        for m in inst.M:
            beta[m] = 99999999
            for j in inst.J:
                if len(inst.T_j[j]) <= inst.C_m[m]:
                    if inst.p_mj[m][j] < beta[m]:
                        beta[m] = inst.p_mj[m][j]

        
        # ----- C_max variable : For the objective Function
        if bounded:
            self.H = inst.FmaxUB
            self.C_max = self.model.addVar(lb=inst.FmaxLB, ub=inst.FmaxUB, obj=1, vtype=GRB.CONTINUOUS, name='C_max')  # makespan
        else:
            print("AF_Block_Position needs an initial UB to run!!")
            exit(0)
        
        print("AF_Block_Position: creating model...")
        print("Initial UB = ", self.H)
 
        #----- X variables ---- Variables for Job processing and tools loading
        self.X = {}   # X_{m,k}^b = 1 if block b is used for kth position on machine m
        for m in inst.M:
            self.X[m] = {}
            for k in range(self.Km[m]):   # k <= max number of jobs on machine m
                self.X[m][k] = {}
                for j in inst.B_mj[m].keys(): # for all blocks
                    for b in inst.B_mj[m][j].keys():
                        self.X[m][k][b] = self.model.addVar(obj=0, vtype=GRB.BINARY, name='X[%s][%s][%s]'%(m,k,b))
                        variables += 1

        #----- Y variables ---- Variables for tools switching
        self.Y = {}   # Y_{m,k,t} = 1 if tool t is loaded at position k and not at position k+1 on machine m
        for m in inst.M:
            self.Y[m] = {}
            for k in range(self.Km[m]):   # k <= max number of jobs on machine m
                self.Y[m][k] = {}
                for t in inst.T:    # for each tool
                    if (m, k, t) not in self.Y:
                    # if t not in self.Y[m][k].keys():
                        self.Y[m][k][t] = self.model.addVar(obj=0, vtype=GRB.BINARY, name='Y[%s][%s][%s]'%(m,k,t))
                        variables += 1
        
        #----- L variables ---- Loss arc variables
        self.L = {}   # L_{m,k} = 1 if loss arc used at position k on machine m
        for m in inst.M:
            self.L[m] = {}
            for k in range(self.Km[m]):   # k <= max number of jobs on machine m
                print("m: ", m , "\tk: ", k)
                if (m, k) not in self.L:
                    self.L[m][k] = self.model.addVar(obj=0, vtype=GRB.BINARY, name='L[%s][%s]'%(m,k))
                    variables += 1        
        
        #Update model
        self.model.update()
        print(variables, " variables created !!")
        
        ############################## Create_constraints ##############################
        # Constraint_1 - each job should be scheduled
        for j in inst.J:
            self.model.addLConstr( quicksum(self.X[m][k][b] for m in inst.M if j in inst.B_mj[m].keys() for k in self.X[m].keys() for b in inst.B_mj[m][j].keys()) == 1,'exactly_one_block_j%s'%j )
            constraints += 1
            
        # Constraint_2 - flow conservation
        for m in inst.M:
            for k in range(self.Km[m]+1):
                rhs = 0
                if(k == 0):
                    rhs = 1
                    self.model.addLConstr( self.L[m][k] + quicksum(self.X[m][k][b] for j in inst.B_mj[m].keys() for b in inst.B_mj[m][j].keys()) == rhs,'flow_on_machine_m%s_position_k%s'%(m,k) )
                elif(k == self.Km[m]):    # if last position
                    rhs = -1 + quicksum(self.L[m][p] for p in range(self.Km[m]) )
                    self.model.addLConstr( -quicksum(self.X[m][k-1][b] for j in inst.B_mj[m].keys() for b in inst.B_mj[m][j].keys()) == rhs,'flow_on_machine_m%s_position_k%s'%(m,k) )
                else:
                    self.model.addLConstr( self.L[m][k] + quicksum(self.X[m][k][b] - self.X[m][k-1][b] for j in inst.B_mj[m].keys() for b in inst.B_mj[m][j].keys()) == rhs,'flow_on_machine_m%s_position_k%s'%(m,k) )
                    
                constraints += 1
       
        # Constraint_3 - number of switches constraints
        for m in inst.M:
            for k in range(0,self.Km[m]-1):
                for t in self.Y[m][k].keys():
                    self.model.addLConstr( self.Y[m][k][t] + self.L[m][k+1] + quicksum(-self.X[m][k][b] + self.X[m][k+1][b] for j in inst.B_mj[m].keys() for b, block in inst.B_mj[m][j].items() if t in block) >= 0,'loading_on_machine_m%s_position_k%s_tool_t%s'%(m,k,t) )
                        
                    constraints += 1                    
        
        # Constraint_4 - makespan constraint
        for m in inst.M:
            self.model.addLConstr( self.C_max >=  quicksum( inst.sw_m[m]*self.Y[m][k][t] for k in range(self.Km[m]) for t in inst.T) + quicksum(inst.p_mj[m][j]*self.X[m][k][b] for j in inst.B_mj[m].keys() for k in self.X[m].keys() for b in self.X[m][k].keys() if b in inst.B_mj[m][j].keys()),'makespan_on_machine_m%s'%(m))
            constraints += 1
        
        print(constraints, " constraints created !!")
        
        # Valid inequalities
  
        self.model.setObjective( self.C_max, sense=GRB.MINIMIZE )
        
        #Update model
        self.model.update()
        
        self.relaxedModel = self.model.relax()
        self.model.write("AF_Block_Position.lp")
        
        


    
    def getDuals(self, inst):
        dual_j = {}
        # TODO
        return dual_j
    
    
    def plotSolution(self, inst, name=""):
        if self.model.status in [2,9]:
            f, ax = plt.subplots(figsize=(10,10) )
            ax.set_xlim([0,self.Fmax.x])
            ax.set_ylim([0,sum([c+2 for c in inst.Cm.values()])])
            
            # TODO
            
            f.savefig(name + "sol.png")
                    

from ..general.instance import *
from MILP import *
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle


################################################################################
#####                          Class Precedence_MILP()                     #####
################################################################################
class Precedence_MILP(MILP):
    #-----------------------
    #----------------------- Constructor
    def __init__(self, inst, modelName = "precedenceMILP", improved=False):
        super().__init__(modelName)
        ##### Attributes
        #-----
        self.bigG = inst.FmaxUB
        
        ##### Symmetry
        if improved:
            self.I = {} # I_{m}: the nb. of times smaller index precedes higher index (i before j & i<j)
            self.D = {} # D_{m}: the nb. of times higher index precedes smaller index (i before j & i>j)
            for m in inst.M:
                self.I[m] = self.model.addVar(obj = 0, vtype = GRB.INTEGER, name='I[%s]'%(m) )
                self.D[m] = self.model.addVar(obj = 0, vtype = GRB.INTEGER, name='D[%s]'%(m) )
        
        ##### Vbles
        #----- Binary
        self.X = {}     # X_{m,i,j}
        self.Alpha = {} # Alpha_{m,j}
        self.Omega = {} # Omega_{m,j}
        self.U = {}     # U_{m,j}
        self.V = {}     # V_{j,t}
        self.W = {}     # W_{j,t}
        #----- Continuous
        self.F = {}     # F_{j} --- completion time of job j
        self.Fmax = self.model.addVar(lb=inst.FmaxLB, ub=inst.FmaxUB, obj = 1, vtype = GRB.INTEGER, name='Fmax')
        #-----
        #----- generate_variables for model
        #--- X
        for m in inst.M:
            self.X[m] = {}
            for i in inst.p_mj[m].keys():
                self.X[m][i] = {}
                for j in inst.p_mj[m].keys():
                    if i != j:
                        self.X[m][i][j] = self.model.addVar(obj = 0, vtype = GRB.BINARY, name='X[%s][%s][%s]'%(m, i, j) )
        
        #--- Alpha, Omega, U
        for m in inst.M:
            self.Alpha[m] = {}
            self.Omega[m] = {}
            self.U[m] = {}
            for j in inst.p_mj[m].keys():
                self.Alpha[m][j] = self.model.addVar(obj = 0, vtype = GRB.BINARY, name='Alpha[%s][%s]'%(m, j) )
                self.Omega[m][j] = self.model.addVar(obj = 0, vtype = GRB.BINARY, name='Omega[%s][%s]'%(m, j) )
                self.U[m][j] = self.model.addVar(obj = 0, vtype = GRB.BINARY, name='U[%s][%s]'%(m, j) )
        
        #--- V, W
        for j in inst.J:
            self.V[j] = {}
            self.W[j] = {}
            self.F[j] = self.model.addVar(lb=min(inst.p_mj[m][j] for m in inst.p_mj.keys() if j in inst.p_mj[m].keys()), obj = 0, vtype = GRB.INTEGER, name='F[%s]'%(j) )
            for t in inst.T:
                if t in inst.T_j[j]:
                    self.V[j][t] = self.model.addVar(lb = 1, obj = 0, vtype = GRB.BINARY, name='V[%s][%s]'%(j, t) )
                    self.W[j][t] = self.model.addVar(obj = 0, vtype = GRB.BINARY, name='W[%s][%s]'%(j, t) )
                else:
                    self.V[j][t] = self.model.addVar(obj = 0, vtype = GRB.BINARY, name='V[%s][%s]'%(j, t) )
                    self.W[j][t] = self.model.addVar(ub = 0, obj = 0, vtype = GRB.BINARY, name='W[%s][%s]'%(j, t) )

        #----- generate_constraints for model - Calmels_2022 (Chap 3 - page 47-48-49)
        #----- constraint (1)
        for i in inst.J:
            self.model.addLConstr( quicksum(self.X[m][i][j] for m in inst.M if i in self.X[m].keys() for j in self.X[m][i].keys() ) <= 1,'C1_i%s'%i)
        #----- constraint (2)
        for j in inst.J:
            self.model.addLConstr( quicksum(self.X[m][i][j] for m in inst.M for i in self.X[m].keys() if j in self.X[m][i].keys() )  <= 1,'C2_j%s'%j)

        #----- constraint (3) - (4)
        for m in inst.M:
            for j in inst.p_mj[m].keys():
                #----- constraint (3)
                self.model.addLConstr( quicksum(self.X[m][j][k] for k in self.X[m][j].keys()) - quicksum(self.X[m][i][j] for i in self.X[m].keys() if j in self.X[m][i].keys()) <= self.Alpha[m][j],'C3_m%s_j%s'%(m, j) )
                #----- constraint (4)
                self.model.addLConstr( quicksum(self.X[m][i][j] for i in self.X[m].keys() if j in self.X[m][i].keys()) - quicksum(self.X[m][j][k] for k in self.X[m][j].keys()) <= self.Omega[m][j],'C4_m%s_j%s'%(m, j) )

        #----- constraint (5) - (9) - (10) - (11)
        for j in inst.J:
            #----- constraint (5)
            self.model.addLConstr( quicksum(self.V[j][t] for t in self.V[j].keys()) <= quicksum(inst.C_m[m] * self.U[m][j] for m in self.U.keys() if j in self.U[m].keys()),'C5_j%s'%(j) )
            #----- constraint (9)
            self.model.addLConstr( quicksum(self.Alpha[m][j] for m in self.Alpha.keys() if j in self.Alpha[m].keys()) <= 1,'C9_j%s'%(j) )
            #----- constraint (10)
            self.model.addLConstr( quicksum(self.Omega[m][j] for m in self.Omega.keys() if j in self.Omega[m].keys()) <= 1,'C10_j%s'%(j) )
            #----- constraint (11)
            self.model.addLConstr( quicksum(self.U[m][j] for m in self.U.keys() if j in self.U[m].keys()) == 1,'C9_j%s'%(j) )
                
        #----- constraint (6)
        for m in inst.M:
            for i in inst.p_mj[m].keys():
                for j in inst.p_mj[m].keys():
                    if i != j:
                        for t in self.V[j].keys():
                            self.model.addLConstr( self.X[m][i][j] + self.V[j][t] - self.V[i][t] <= self.W[j][t] + 1,'C6_m%s_i%s_j%s_t%s'%(m,i,j,t) )
        
        #----- constraint (7) - (8)
        for m in inst.M:
            #----- constraint (7)
            self.model.addLConstr( quicksum(self.Alpha[m][j] for j in self.Alpha[m].keys()) <= 1,'C7_m%s'%(m) )
            #----- constraint (8)
            self.model.addLConstr( quicksum(self.Omega[m][j] for j in self.Omega[m].keys()) <= 1,'C8_m%s'%(m) )
            
        #----- constraint (12) - (13) - (15) - (16) - (18)
        for m in inst.M:
            for j in inst.p_mj[m].keys():
                if j in self.Alpha[m].keys() and j in self.U[m].keys():
                    #----- constraint (12)
                    self.model.addLConstr( self.Alpha[m][j] <= self.U[m][j],'C12_m%s_j%s'%(m,j) )
                if j in self.Omega[m].keys() and j in self.U[m].keys():
                    #----- constraint (13)
                    self.model.addLConstr( self.Omega[m][j] <= self.U[m][j],'C13_m%s_j%s'%(m,j) )
                if j in self.Alpha[m].keys() and j in self.U[m].keys():
                    #----- constraint (15)
                    self.model.addLConstr( quicksum(self.X[m][i][j] for i in self.X[m].keys() if j in self.X[m][i].keys()) + self.Alpha[m][j] == self.U[m][j],'C15_m%s_j%s'%(m,j) )
                if j in self.Omega[m].keys() and j in self.U[m].keys():
                    #----- constraint (16)
                    self.model.addLConstr( quicksum(self.X[m][j][k] for k in self.X[m][j].keys()) + self.Omega[m][j] == self.U[m][j],'C16_m%s_j%s'%(m,j) )
                #----- constraint (18)
                self.model.addLConstr( self.F[j] >= inst.p_mj[m][j]*self.U[m][j] + quicksum(inst.sw_m[m] * self.W[j][t] for t in inst.T_j[j]) ,'C18_m%s_j%s'%(m,j) )
            
        #----- constraint (14) - (17)
        for m in inst.M:
            for i in inst.p_mj[m].keys():
                for j in inst.p_mj[m].keys():
                    if i != j:
                        #----- constraint (14)
                        self.model.addLConstr( 2 * self.X[m][i][j] <= self.U[m][i] + self.U[m][j],'C14_m%s_i%s_j%s'%(m,i,j) )
                        #----- constraint (17)
                        self.model.addLConstr( self.F[j] >= self.F[i] + inst.p_mj[m][j]*self.U[m][j] + quicksum(inst.sw_m[m] * self.W[j][t] for t in inst.T_j[j]) - self.bigG*(1 - self.X[m][i][j]) ,'C17_m%s_i%s_j%s'%(m,i,j) )
                 
        #----- constraint (19)
        for j in inst.J:
            #----- constraint (26)
            self.model.addLConstr( self.Fmax >= self.F[j] ,'C26_j%s'%j )
            '''
            for t in inst.T_j[j]:
                #----- constraint (19)
                self.model.addLConstr(self.V[j][t] == 1,'C19_j%s_t%s'%(j,t))
            for t in inst.T:
                if t not in inst.T_j[j]:
                    #----- constraint (20)
                    self.model.addLConstr(self.W[j][t] == 0,'C20_j%s_t%s'%(j,t))
            '''
        #-----
        if improved:
            #----- breaking symetry constraint () -
            for m in inst.M:
                self.model.addLConstr( self.I[m]  == quicksum(self.X[m][i][j] for i in self.X[m].keys() for j in self.X[m][i].keys() if j > i),'def_I_m%s'%m )
                self.model.addLConstr( self.D[m]  == quicksum(self.X[m][i][j] for i in self.X[m].keys() for j in self.X[m][i].keys() if j < i),'def_D_m%s'%m )
                self.model.addLConstr( self.I[m]  >= self.D[m],'symetry_m%s'%m )
        
        #-----
        #----- objective_function
        self.model.setObjective( self.Fmax, sense = GRB.MINIMIZE)

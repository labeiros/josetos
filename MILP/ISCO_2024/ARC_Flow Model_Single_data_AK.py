# -*- coding: utf-8 -*-
"""
Created on Thu May 25 13:20:19 2023

@author: Ghazouani Imen 
"""
################################################################################
#####            Importing libraries & Gurobi functions and classes        #####

from prettytable import PrettyTable
from gurobipy import *
import pandas as pd
import numpy as np
import traceback
import copy
import csv
import os

################################################################################
#####                              PARAMETERS                              #####

#----- Gurobi Paramaters 
gurobi_params = {'TimeLimit':1800, 'MIPGap':0.0001, 'NodefileStart':0.5, 'Presolve':1, 'Symmetry':1, 'Heuristics':0.05, 'Cuts': 1, 'IntegralityFocus':1, 'IntFeasTol':1e-9}#
#----- 

################################################################################
#==== === ===   1st step: Implementing automatically the INPUT DATA ===== ====== ====== 

#----------- Import parameters from excel sheets
file_path = 'C:/Users/arthur.kramer/Documents/Travail/Research/TSSP/3-SSP-NPM/Codes/SSP-NPM/x64/Release/Instances/02-05-10/ins261_m=2_j=5_t=10_dens=s_var=1.csv'
data_instance = pd.read_csv(file_path, sep=';', header=None)

# Extract the filename from the file path
filename_with_extension = os.path.basename(file_path)

# Split the filename into name and extension
filename_parts = os.path.splitext(filename_with_extension)

# Extract the desired part from the filename
Instance_Label = filename_parts[0]
print('\n')
print('The file currently being worked on is:', Instance_Label)
print('\n')

#------- Set the instance chracteristics of the model: First line in the CSV 
First_row= [int(x) for x in list(data_instance.iloc[0].dropna())] # first is m; second is j; third in the list is T
M = First_row[0] # number of the machines
J = First_row[1] # number of jobs
T = First_row[2] # number of Tools
print('************** Starting with the instance: ************* ')
print("This problem considers: {} jobs, {} tools and {} machines.".format(J,T,M))
print('\n')

#----------- Get the capacities of the available machines 
C = [int(x) for x in list(data_instance.iloc[1].dropna())]                          
print("Machine tools capacities:", C)

#----------- Get the switching time data on each machine     
sw = [int(x) for x in list(data_instance.iloc[2].dropna())]    
print("Machine switching times:", sw)

#----------- List of the Jobs to be processed
Jobs = []
for j in range(J):
    Jobs.append(j+1)

#----------- .List of all tools to be used 
Tools = []
for t in range(T):
    Tools.append(t+1)
        
     
#----------- Get the procedure time data of each job on the available machines
proc_time=[]
for machines in range(M):
    proc_time.append([int(x) for x in list(data_instance.iloc[ machines + 3].dropna())])
print("The list of processing times is as follows: [machines[jobs]]:", proc_time)
print('\n')

#----------- Get the data of the tools required for each job
                       #Line: Jobs || Column: Tools
#Get rid of the used rows and keep only the dataframe of the tool requierement
for i in range(M+3):
    data_instance= data_instance.drop(i)
# Transpose the dataframe the columns to rows and the rows to columns in a way to have the jobs on rows and tools on columns
data_instance=data_instance.T.astype(int)
#Rename the header of the Dataframe
data_instance.columns = Tools
print('Matrix of Tools requirement:')
print(data_instance)

#get the tool requierement
tools_requirement=data_instance.iloc[:, :].values.tolist() 
print('\n')
print("The list of tool requirements is as follows:[jobs[tools]]:", tools_requirement)
print('\n')

#New added code
#Tools required by each job
tr_j = [[] for j in range(J)]
for j in range(J):
    for t in range(T):
        if(tools_requirement[j][t]):
            tr_j[j].append(t)
#Pre-processing on the machines capacities
nb_tools_used = 0
for t in range(T):
    for j in range(J):
        if(tools_requirement[j][t]):
            nb_tools_used += 1
            break
for k in range(M):
    if (C[k] > nb_tools_used):
        C[k] = nb_tools_used
        print("New C[",k,"] = ", C[k])


#----------- Declaring the H: Horizon constant
H= 30

##### This is a sample example of how the data of global variables are constructed ########################################
# The fixed parameters of the model
# J = 6 # number of jobs
# T = 9 # number of Tools
# M = 2 # number of the machines
# C = [4, 3]  # list of the machines' capacities
# sw = [1, 2] # list of the switching time on each machine
# proc_time = [
#              [1,3,4,5,6,1],         
#              [99,4,99,99,3,1]
#             ]
# tools_requirement = [[1,0,0,1,0,0,0,1,1],
#       [1,0,1,0,1,0,0,0,0],
#       [0,1,0,0,0,1,1,1,0],
#       [1,0,0,0,1,0,1,0,1],
#       [0,0,1,0,1,0,0,1,0],
#       [1,1,0,1,0,0,0,0,0]
#       ]
###############################################################################

#====  === ===      CREATING HEURISTIC    ====== ======== ========== ======= ======= 
class Solution:
    def __init__(self):
        self.hasSolution = False
        self.initialize()
        self.sol_big_cmax = J * M * T * 1000
           
    def initialize(self):
        self.sol_heur_c_max = 0
        self.sol_ct_machines = [0] * M
        self.sol_seq = [[] for l in range(M)]
        self.sol_st = [[] for i in range(M)]
        self.sol_ct = [[] for k in range(M)]
        self.sol_nb_changes = [0] * M
        self.sol_tools_planning = []
        self.sol_jobs_unscheduled = [True] * J
        self.sol_tools = [[] for k in range(M)]
        for k in range(M):
            self.sol_tools[k] = [[] for o in range(C[k])]

    def heuristic(self):
        self.initialize()
        
        job_machine = [0] * J
        for i in range(J):
            for k in range(M):
                if len(tr_j[i]) <= C[k]:
                    job_machine[i] += 1
        
        counter = 0
        while counter < J:
            restr_jobs = False
            best_machine = -1
            best_job = -1
            best_cmax = self.sol_big_cmax
            for i in range(J):
                if self.sol_jobs_unscheduled[i]:
                    if job_machine[i] < M:
                        restr_jobs = True
                    for k in range(M):
                        if C[k] >= len(tr_j[i]):
                            if not restr_jobs:
                                if self.sol_ct_machines[k] + proc_time[k][i] < best_cmax:
                                    best_cmax = self.sol_ct_machines[k] + proc_time[k][i]
                                    best_job = i
                                    best_machine = k
                            else:
                                best_cmax = self.sol_ct_machines[k] + proc_time[k][i]
                                best_job = i
                                best_machine = k
            # The best job and machine where chosen
            self.sol_seq[best_machine].append(best_job)
            self.sol_st[best_machine].append(self.sol_ct_machines[best_machine])
            self.sol_ct[best_machine].append(best_cmax)
            self.sol_ct_machines[best_machine] = best_cmax
            
            self.sol_jobs_unscheduled[best_job] = False
    
            if best_cmax > self.sol_heur_c_max:
                self.sol_heur_c_max = best_cmax
    
            counter += 1

        # Print sequence
        print("===============================")
        print("Heuristic solution: Cmax =", self.sol_heur_c_max)
        for k in range(len(self.sol_seq)):
            print("Machine", k, "C[", k, "] =", self.sol_ct_machines[k])
            print("Seq:\t", end="")
            for i in range(len(self.sol_seq[k])):
                print(self.sol_seq[k][i], "\t", end="")
            print()
            print("St :\t", end="")
            for i in range(len(self.sol_st[k])):
                print(self.sol_st[k][i], "\t", end="")
            print()
            print("Ct :\t", end="")
            for i in range(len(self.sol_ct[k])):
                print(self.sol_ct[k][i], "\t", end="")
            print()
    
        print("===============================")
            
    def KTNS(self):
        # Initialize data structure    
        L_kin = [[[ len(self.sol_seq[k]) for s in range(len(self.sol_seq[k])) ] for t in range(T)] for k in range(M)]
        
        # Fill L_kin
        for k in range(M):
            nb_positions = len(self.sol_seq[k])
            for t in range(T):
                for n in range(nb_positions):
                    for m in range(n, nb_positions):
                        job_index = self.sol_seq[k][m]
                        if tools_requirement[job_index][t] and m < L_kin[k][t][n]:
                            L_kin[k][t][n] = m
        print(L_kin)
        # KTNS
        self.sol_tools_planning = [[[ False for t in range(T) ] for j in range(len(self.sol_seq[k]))] for k in range(M)]
        # Wn = [[[ False for t in range(T) ] for j in range(len(self.sol_seq[k]))] for k in range(M)]
        for k in range(M):
            Ji = [False] * T
            nb_positions = len(self.sol_seq[k])
            counter = 0
            min_pos = J + 2
            tool_selected = -1 
            while counter < C[k]:  # we need to fill magazine capacity
                for t in range(T):  # for each tool
                    if not Ji[t] and L_kin[k][t][0] < min_pos:
                        tool_selected = t
                        min_pos = L_kin[k][t][0]
                Ji[tool_selected] = True
                min_pos = J + 2
                counter += 1
                       
            check = True
            i_check = -1
            n = 0
            while n < nb_positions:
                check = False
                while not check:
                    check = True
                    i_check = -1
                    for i in range(T):  # for each tool
                        if L_kin[k][i][n] == n and Ji[i] == False:
                            i_check = i
                            check = False
                    if not check:
                        Ji[i_check] = True
                        max_check = -1
                        best_i = -1
                        for i in range(T):
                            if Ji[i] == True and L_kin[k][i][n] > max_check:
                                max_check = L_kin[k][i][n]
                                best_i = i
                        Ji[best_i] = False
                    else:
                        for t in range(T):
                            self.sol_tools_planning[k][n][t] = Ji[t]    
                        n += 1
            # Count number of changes and update completion times
            for i in range(nb_positions - 1):
                for t in range(T):
                    if self.sol_tools_planning[k][i][t] and not self.sol_tools_planning[k][i + 1][t]:
                        self.sol_nb_changes[k] += 1
                        
                self.sol_st[k][i + 1] += self.sol_nb_changes[k] * sw[k]
                self.sol_ct[k][i + 1] += self.sol_nb_changes[k] * sw[k]
                
            self.sol_ct_machines[k] = self.sol_ct[k][-1]
            
            if self.sol_ct_machines[k] > self.sol_heur_c_max:
                self.sol_heur_c_max = self.sol_ct_machines[k]
                
        print("KTNS done!")
        
        # Print sequence
        print("===============================")
        print("Heuristic solution: Cmax =", self.sol_heur_c_max)
        for k in range(M):
            print("Machine", k, "C[", k, "] =", self.sol_ct_machines[k], "\tNb of changes =", self.sol_nb_changes[k])
            print("Seq:\t", end="")
            for i in range(len(self.sol_seq[k])):
                print(self.sol_seq[k][i], "\t", end="")
            print("\nSt :\t", end="")
            for i in range(len(self.sol_st[k])):
                print(self.sol_st[k][i], "\t", end="")
            print("\nCt :\t", end="")
            for i in range(len(self.sol_ct[k])):
                print(self.sol_ct[k][i], "\t", end="")
            print()
            
        print("===============================")
        print("Tools!")
        for k in range(len(self.sol_tools_planning)):
            print("Machine", k, "\tNb of changes =", self.sol_nb_changes[k])
            for n in range(len(self.sol_tools_planning[k])):
                for t in range(len(self.sol_tools_planning[k][n])):
                    print(int(self.sol_tools_planning[k][n][t]), " ", end="")
                print()
            print()
        print("===============================")
       
           
    def initialize_af_solution_structure(self):
        H = self.sol_heur_c_max
        #----- X variables ---- Whitch are Arc Variables for Job processing
        self.sol_Xjpq_k = {} # existence of the arc(jpqk) 
        for j in range(1,J+1):     #for each job
            for k in range(1,M+1):    #for each machine
                for p in range(0,self.sol_heur_c_max-proc_time[k-1][j-1] +1 ):   #for each Time Horizon on the machine  (we go to p=H-proc_time[k-1][j-1]  to impose that no job have a q that exceeds the Horizon imposed so we take into account at least
                    if (j, p, p + proc_time[k-1][j-1], k) not in self.sol_Xjpq_k:   # the q here id equal H-proc_time[k-1][j-1] it's imposed
                        self.sol_Xjpq_k[(j, p, p + proc_time[k-1][j-1], k)] = False
                 
        #----- Y variables ---- Whitch are Arc Variables for Tool loading
        self.sol_Ytpq_k = {}  # existence of the arc(tpqk) meaning if tool t is on the machine k at the
        Jt = np.array(tools_requirement)
        for t in range(1,T+1):
            # extrait les indices des lignes où la colonne du Tool t a une valeur de 1
            indices_Jobs = np.where(Jt[:, t-1] == 1)[0]
            if len(indices_Jobs)!=0:   # Handle the case where no job requieres the t tool  : So we won't create an arc for that case
                for k in range(1,M+1):
                    proc_times_indices_jobs=[]
                    for proc in indices_Jobs:
                        proc_times_indices_jobs.append(proc_time[k-1][proc])
                    alpha= min( proc_times_indices_jobs)
                    for  p in range(0, self.sol_heur_c_max-alpha+1):                                                  
                       for q in range(p+alpha, self.sol_heur_c_max+1):
                           if (t, p, q, k) not in self.sol_Ytpq_k:
                               self.sol_Ytpq_k[(t, p, q, k)] = False
               
        #----- S variables ---- Whitch are Variables for tool switching operations / Set_UP arcs
        self.sol_Spq_k = {}  # existence of the arc(pqk) meaning if there is a setup/switching from machine k at the
        for k in range(1,M+1):
            Beta= min(proc_time[k-1])
            for p in range(Beta,self.sol_heur_c_max):
                #for p in range(0,H): # fo better version p should start from Beta knowing that we do not count a  set-up for the first load
                for l in range(1, C[k-1]+1) :                 # The beta will be the minimum proc time of all possible jobs tio be processed on machine k
                    if p+ sw[k-1]*l <= self.sol_heur_c_max :
                        if (p, p+ sw[k-1]*l, k) not in self.sol_Spq_k:
                            self.sol_Spq_k[(p, p+ sw[k-1]*l, k)] = False
                
        # ----- L variables ---- Which are dummy Arc Variables : for loss arcs
        self.sol_lp_k = {}  # existence of the arc(pk) meaning if there is a loss on machine k at moment p to finish up the graph till H
        for k in range(1,M+1):
            Beta= min(proc_time[k-1])
            for p in range(Beta,self.sol_heur_c_max):
                if (p, k) not in self.sol_lp_k:
                    self.sol_lp_k[(p, k)] = False

    def transform_heur_solution_AF_vairables(self):
        #Job sequence
        print("H: ", self.sol_heur_c_max)
        for k in range(M):
            #Job and setup arcs
            st = 0
            ct = 0
            for i in range(len(self.sol_seq[k])):
                job = self.sol_seq[k][i]
                st = self.sol_st[k][i]
                if i > 0 and st - ct > 0: #then there is an setup in between
                    self.sol_Spq_k[(ct, st, k+1)] = True
                    print("s[", ct, ", ", st, " ,", k+1,"] = ", self.sol_Spq_k[(ct, st, k+1)])
                ct = self.sol_ct[k][i]
                self.sol_Xjpq_k[(job+1, st, ct, k+1)] = True
                print("x[", job+1, ", ", st, " ,", ct, ", ", k+1,"] = ", self.sol_Xjpq_k[(job+1, st, ct, k+1)])
                #for each tool
            #Loss arcs
            if(self.sol_ct_machines[k] < self.sol_heur_c_max):
                self.sol_lp_k[(self.sol_ct_machines[k], k+1)] = True
                print("l[", self.sol_ct_machines[k], ", ", k+1,"] = ", self.sol_lp_k[(self.sol_ct_machines[k], k+1)])
            #Tool arcs
            for t in range(T): #for each tool
                tool_start_time = -1
                tool_end_time = -1
                tool_charged = False
                for i in range(len(self.sol_seq[k])):
                    if self.sol_tools_planning[k][i][t] == 1:
                        if tool_charged:
                            tool_end_time = self.sol_ct[k][i]
                        else:
                            tool_start_time = self.sol_st[k][i]
                            tool_end_time = self.sol_ct[k][i]
                            tool_charged = True
                    else:
                        if tool_charged:
                            self.sol_Ytpq_k[(t+1, tool_start_time, tool_end_time, k+1)] = True
                            print("y[", t+1, ", ", tool_start_time, " ,", tool_end_time, ", ", k+1,"] = ", self.sol_Ytpq_k[(t+1, tool_start_time, tool_end_time, k+1)])
                            tool_charged = False
                if tool_charged:
                    self.sol_Ytpq_k[(t+1, tool_start_time, self.sol_heur_c_max, k+1)] = True
                    print("y[", t+1, ", ", tool_start_time, " ,", self.sol_heur_c_max, ", ", k+1,"] = ", self.sol_Ytpq_k[(t+1, tool_start_time, self.sol_heur_c_max, k+1)])


        print("TRANSFORMATION FINISHED")
           
#====  === ===      CREATING The MODEL    ====== ======== ========== ======= ======= 

model_name = "ARC_Flow_SSP_NPM_Cmax" # Define the name of the model
model = Model(model_name)   # Declaring the model

def create_model(model, solution): #Variables + constraints

    global variables
    global constraints
    #----- Putting to 0 the model's number of variables and constraints
    variables = 0
    constraints = 0
    started = 0
   ############################## Create_decision_variables ##############################
    try:
        H = solution.sol_heur_c_max
        print("Solution sol cmax = ",  solution.sol_heur_c_max, "\tH: ", H)
   #----- X variables ---- Whitch are Arc Variables for job processing          
        Xjpq_k = {} # existence of the arc(jpqk) meaning if job j is on the machine k at the                 #The horizon goes from 0 to H in the timeline axis
        for j in range(1,J+1):     #for each job
         for k in range(1,M+1):    #for each machine
          for p in range(0,H-proc_time[k-1][j-1] +1 ):   #for each Time Horizon on the machine  (we go to p=H-proc_time[k-1][j-1]  to impose that no job have a q that exceeds the Horizon imposed so we take into account at least
                                                            # that for each job he can have an arc existing going from p= H-proc_time[k-1][j-1] to H  and then will decide to take it or not according the optimization
            if (j, p, p + proc_time[k-1][j-1], k) not in Xjpq_k:   # the q here id equal H-proc_time[k-1][j-1] it's imposed
               Xjpq_k[(j, p, p + proc_time[k-1][j-1], k)] = model.addVar(0, 1, 0, vtype=GRB.BINARY,name=f"X[{j},{p},{p + proc_time[k-1][j-1]},{k}]") # (lb, ub, obj, type, name) set var initial value to 0 if there is an initial UB
               variables += 1
               if(solution.sol_Xjpq_k[(j, p, p + proc_time[k-1][j-1], k)]):
                   Xjpq_k[(j, p, p + proc_time[k-1][j-1], k)].start = 1
                   started += 1
               else:
                   Xjpq_k[(j, p, p + proc_time[k-1][j-1], k)].start = 0
                    
   #----- Y variables ---- Whitch are Arc Variables for Tool loading
        Ytpq_k = {}  # existence of the arc(tpqk) meaning if tool t is on the machine k at the
        Jt = np.array(tools_requirement)
        for t in range(1,T+1):
            # extrait les indices des lignes où la colonne du Tool t a une valeur de 1
            indices_Jobs = np.where(Jt[:, t-1] == 1)[0]
            if len(indices_Jobs)!=0:   # Handle the case where no job requieres the t tool  : So we won't create an arc for that case
                for k in range(1,M+1):
                    proc_times_indices_jobs=[]
                    for proc in indices_Jobs:
                        proc_times_indices_jobs.append(proc_time[k-1][proc])
                    alpha= min( proc_times_indices_jobs)
                    for  p in range(0, H-alpha+1):                                                  
                        for q in range(p+alpha, H+1):
                            if (t, p, q, k) not in Ytpq_k:
                                Ytpq_k[(t, p, q, k)] = model.addVar(0, 1, 0, GRB.BINARY, f"Y({t}_{p}_{q}_{k})")
                                variables += 1
                                if solution.sol_Ytpq_k[(t, p, q, k)]:
                                    Ytpq_k[(t, p, q, k)].start = 1
                                    started += 1
                                # else:
                                #     Ytpq_k[(t, p, q, k)].start = 0
                 
   #----- S variables ---- Whitch are Variables for tool switching operations / Set_UP arcs
        Spq_k = {}  # existence of the arc(pqk) meaning if there is a setup/switching from machine k at the
        for k in range(1,M+1):
            Beta= min(proc_time[k-1])
            for p in range(Beta,H):       
                for l in range(1, C[k-1]+1) :                 # The beta will be the minimum proc time of all possible jobs tio be processed on machine k
                    if    p+ sw[k-1]*l <= H :
                        if (p, p+ sw[k-1]*l, k) not in Spq_k:
                            Spq_k[(p, p+ sw[k-1]*l, k)] = model.addVar(0, 1, 0, GRB.BINARY, f"S({p}_{p+ sw[k-1]*l}_{k})")
                            variables += 1
                            if(solution.sol_Spq_k[(p, p+ sw[k-1]*l, k)]):
                                Spq_k[(p, p+ sw[k-1]*l, k)].start = 1
                                started += 1
                            else:
                                Spq_k[(p, p+ sw[k-1]*l, k)].start = 0
                
   # ----- L variables ---- Which are dummy Arc Variables : for loss arcs
        lp_k = {}  # existence of the arc(pk) meaning if there is a loss on machine k at moment p to finish up the graph till H
        for k in range(1,M+1):
            Beta= min(proc_time[k-1])
            for p in range(Beta,H):
                if (p, k) not in lp_k:
                    lp_k[(p, k)] = model.addVar(0, 1, 0, GRB.BINARY, f"L({p}_{k})")
                    variables += 1
                    if(solution.sol_lp_k[(p, k)]):
                        lp_k[(p, k)].start = 1
                        started += 1
                    else:
                        lp_k[(p, k)].start = 0    
        
        print("started = ", started)
   # ----- CMAX variable : For the objective Function
        CMAX = model.addVar(0, H, 1, GRB.CONTINUOUS, 'CMAX')
            
    #Update model
        model.update()
        print(variables, "variables created !!")
    
    #***********  SAVING THE VARIABLES IN A .TXT FILE
    
    #Open an txt file for writing
        filename = "variables_Arc_Flow_Cmax.Txt"
        with open(filename, "w") as file:

    # Iterate over the model's variables and write their declarations to the file
         for var in model.getVars():
          file.write(var.getAttr("VarName") + "\n")
         
    except Exception as e:
      print("Error creating variables of AF model:", str(e))
        
       # print( Xjpq_k[(1,0,8,1)].getAttr("VarName")) This should give the var  X[(1,0,8,1)] since its true the arc exists, this possibility of (1,0,8,1) exists
        #print(Xjpq_k[(1,0,1,1)].getAttr("VarName")) This sould give an error sonce this arc do not exist ( we have for job 1 on machine on that starts at 0 should finish at 8 since its proc time is 8 therefore no arc (1,0,1,1) should be created)
          
        
        
     ############################## Create_constraints ##############################
    try:  

       # Constraint_1
        for j in range(1,J+1): 
            C1 = 0
            for k in range(1, M+1):
                for p in range(0,H-proc_time[k-1][j-1] +1):
                    if (j , p, p + proc_time[k-1][j-1], k) in Xjpq_k:
                            C1 += Xjpq_k[(j, p, p + proc_time[k-1][j-1], k)]
            cname = "C1_Job_" + str(j)   
            model.addConstr(C1 == 1, name=cname)
            constraints += 1
            
       # Constraint_2
        for k in range(1,M+1):
         Beta= min(proc_time[k-1])
         for q in range(0,H+1):
               
            C2_X_1 = 0
            C2_S_1 = 0
            C2_X_2 = 0
            C2_S_2 = 0   
            C2_L_2 = 0
            C2_L = 0

            if (q , k) in lp_k:
               C2_L += lp_k[(q , k)]
               
            for j in range(1,J+1):
             if (j , q , q + proc_time[k-1][j-1], k) in Xjpq_k:
                C2_X_1 += Xjpq_k[(j, q , q + proc_time[k-1][j-1], k)]
                
             if (j , q - proc_time[k-1][j-1], q , k) in Xjpq_k:
                C2_X_2 += Xjpq_k[(j, q - proc_time[k-1][j-1] , q , k)]
                
            for l in range(1, C[k-1]+1) :
             if  ( q , q + sw[k-1]*l   , k) in  Spq_k :
                C2_S_1 +=  Spq_k[(q, q + sw[k-1]*l , k)]
                
             if q -sw[k-1]*l  >= Beta and  ( q -sw[k-1]*l , q  , k) in  Spq_k : 
                  C2_S_2 +=   Spq_k[(q - sw[k-1]*l, q , k)]                 
                         
            if q==0:
                cname = "C2_Machine_" + str(k)+ "_Horizon_" + str(q)
                model.addConstr(C2_X_1 == 1, name=cname)
                constraints += 1
                
            elif q==H:
                for p in range(Beta , H):
                 if (p, k)  in lp_k:
                    C2_L_2 += lp_k[(p, k)]   
                cname = "C2_Machine_" + str(k)+ "_Horizon_" + str(q)
                model.addConstr(- C2_X_2 - C2_S_2 == -1 + C2_L_2 , name=cname)
                constraints += 1
            
            else:
                cname = "C2_Machine_" + str(k)+ "_Horizon_" + str(q)
                model.addConstr(C2_X_1 + C2_S_1 + C2_L - C2_X_2 - C2_S_2 == 0 , name=cname)
                constraints += 1                
            
            
       # Constraint_3
        for k in range(1,M+1):
         Beta= min(proc_time[k-1])
         for q in range(0,H+1):
               
            C3_Y_1 = 0
            C3_S_1 = 0
            C3_Y_2 = 0
            C3_S_2 = 0    
            
            for t in range(1,T+1):
             for r in range(q+Beta , H+1 ):
              if (t, q, r, k)  in Ytpq_k:
                C3_Y_1 += Ytpq_k[(t, q , r , k)]
                
             for r in range (0 , q):
              if (t, r, q, k) in Ytpq_k:
                C3_Y_2 += Ytpq_k[(t,  r , q , k)]
                
            for l in range(1, C[k-1]+1) :
             if  ( q , q + sw[k-1]*l   , k) in  Spq_k :
                C3_S_1 += l * Spq_k[(q, q + sw[k-1]*l , k)]
                
            for l in range(1, C[k-1]+1) : 
             if q -sw[k-1]*l  >= Beta and  ( q -sw[k-1]*l , q  , k) in  Spq_k : 
                 C3_S_2 +=    l * Spq_k[(q - sw[k-1]*l , q , k)]

                         
            if q==0:
                cname = "C3_Machine_" + str(k)+ "_Horizon_" + str(q)
                model.addConstr(C3_Y_1 + C3_S_1 - C3_Y_2 - C3_S_2 == C[k-1] , name=cname)
                constraints += 1
                
            elif q==H:
                cname = "C3_Machine_" + str(k)+ "_Horizon_" + str(q)
                model.addConstr(C3_Y_1 + C3_S_1 - C3_Y_2 - C3_S_2 == - C[k-1] , name=cname)
                constraints += 1
            
            else:
                cname = "C3_Machine_" + str(k)+ "_Horizon_" + str(q)
                model.addConstr(C3_Y_1 + C3_S_1 - C3_Y_2 - C3_S_2 == 0 , name=cname)
                constraints += 1               
                
                
       # Constraint_4
        # creation of the list Tj of tools  
        Tj = np.array(tools_requirement)  
        for k in range(1,M+1):
           for j in range(1,J+1):
               for q in range(0, H - proc_time[k-1][j-1] + 1 ):
                 if (j , q , q + proc_time[k-1][j-1], k) in Xjpq_k:
                   C4_X = Xjpq_k[(j, q, q + proc_time[k-1][j-1], k)]
                 
                 #List of (indexis-1) of tools that are requiered by job j
                 indices_Tools = np.nonzero(Tj[j-1])[0]
                 for t in indices_Tools:  
                  C4_Y = 0
                  for p in range(0,q +1):
                   for r in range(q + proc_time[k-1][j-1], H+1):  # cela c'est l'inferieure ou egale dans le somme (la formulation)
                    if (t+1 , p , r , k ) in  Ytpq_k :
                      C4_Y+= Ytpq_k[(t+1, p, r, k)]
                     
                  cname = "C4_Machine_" + str(k) + "_Job_" + str(j) + "_start-Horizon_" + str(q) + "_Tool_"+ str(t+1)
                  model.addConstr(C4_X - C4_Y <= 0, name=cname)
                  constraints += 1                                                                                 
       # Constraint_5 
        for k in range(1,M+1):
         Beta= min(proc_time[k-1])
         for q in range(1,H):
          C5_Y=0
          C5_S=0
          for t in range(1,T+1):
            for r in range(q+Beta , H+1 ):
              if (t , q , r , k ) in  Ytpq_k:
                C5_Y+= Ytpq_k[(t, q , r , k)]
          
          for l in range(1, C[k-1]+1) : 
              if q -sw[k-1]*l  >= Beta and  ( q -sw[k-1]*l , q  , k) in  Spq_k : 
                C5_S+=   l * Spq_k[(q - sw[k-1]*l , q , k)]
           
          cname = "C5_Machine_" + str(k)+ "_Entree_arc_sortie-Horizon_" + str(q)
          model.addConstr(C5_Y - C5_S == 0, name=cname)
          constraints += 1
                 
                 
       # Constraint_6
        for j in range(1,J+1): 
            C6 = 0
            for k in range(1, M+1):
                for p in range(0,H-proc_time[k-1][j-1] +1):
                    if (j, p, p + proc_time[k-1][j-1], k) in Xjpq_k:
                            C6 +=      ( p + proc_time[k-1][j-1] ) *  Xjpq_k[(j, p, p + proc_time[k-1][j-1], k)]
            cname = "C6_Job_" + str(j)
            model.addConstr(C6 <= CMAX, name=cname)
            constraints += 1    
            
        #Update model
        model.update()
        print(constraints, "Constraints created !!")
            
    except Exception as e:
       print("Error creating constraints of AF model:", str(e))
       traceback.print_exc()
       
  ############################## Create_objective_function ##############################
  # =============================================================================
  #----- Obj.Fct : min CMAX
    model.setObjective( CMAX , sense=GRB.MINIMIZE )
  # =============================================================================
    
  #Write model to .lp file
    model.write( model_name + '.lp')       
    

def solve_model(model, gurobi_params): # The Model parameters + optimization part
    try:
        H = solution.sol_heur_c_max
        print("Solution sol cmax = ",  solution.sol_heur_c_max, "\tH: ", H)
        #LOAD GUROBI PARAMETERS
        model.params.timeLimit = gurobi_params['TimeLimit']                # set time limit
        model.params.MIPGap = gurobi_params['MIPGap']                      # set gap limit
        model.Params.Presolve = gurobi_params['Presolve']                  # set Gurobi presolve parameter
        model.Params.NodefileStart = gurobi_params['NodefileStart']        # set Gurobi NodefileStart parameter --- for Out-of-Memory
        model.Params.Symmetry = gurobi_params['Symmetry']                  # set Gurobi symmetry parameter
        model.Params.Heuristics = gurobi_params['Heuristics']              # set Gurobi heuristics parameter
        model.Params.Cuts = gurobi_params['Cuts']                          # set Gurobi cuts parameter
        #model.Params.MIPFocus = gurobi_params['MIPFocus']                  # set the focus of the MIP solver
        model.Params.Cutoff = H                  # set the UB
        #model.Params.NoRelHeurTime = gurobi_params['NoRelHeurTime']        # Limits the amount of time (in seconds) spent in the NoRel heuristic
        model.Params.Threads = 1                                           # set Gurobi threads parameter                                              # set Gurobi threads parameter
        model.Params.IntegralityFocus = gurobi_params['IntegralityFocus']
        model.Params.IntFeasTol = gurobi_params['IntFeasTol']
        #model.Params.OutputFlag = 1                                       # Controls Gurobi output: enables or disables solver output                           
    
    
    # =============================================================================
    #SOLVE MODEL
        print('+++++ +++++ Solving model +++++ +++++')
        model.optimize()
    # =============================================================================
    
    
#==== === === === === === ===  Output Data  ===== ====== ====== ===== ====== ====== 
        
    ###  Try_2: Printing the optimization results in a table form
        Results_table = PrettyTable()
        Results_table.field_names = ["Desicion_Variable", "Optimal_Value"]
        for v in model.getVars():
            if v.X != 0 :
                  Results_table.add_row([v.VarName, int(v.X)])
        print(Results_table)
        
        
        # =============================================================================
        # Print The result of the objective function
        print('** Obj: The minimum MAKESPAN found is  %g **' % model.ObjVal)
        # =============================================================================
        
        
    except GurobiError as e:
         print('Error code ' + str(e.errno) + ': ' + str(e))

    except AttributeError:
         print('Encountered an attribute error')   
         
    # Check if the model is infeasible
    if model.Status == GRB.INFEASIBLE:
     # Compute the Irreducible Inconsistent Subsystem (IIS)
     model.computeIIS()
     # Write the IIS to a file
     model.write("infeasible_constraints.ilp")     
         
#==== === === === === === ===  Main  ===== ====== ====== ===== ====== ====== 
# heuristic()


solution = Solution()
solution.heuristic()
solution.KTNS()
solution. initialize_af_solution_structure()
solution.transform_heur_solution_AF_vairables()

create_model(model,solution)
solve_model(model, gurobi_params)









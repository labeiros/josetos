from gurobipy import *
from instance import *
#from machineKP import *
from machineKP_MILP import *
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle




################################################################################
#####                           Class AF_without_setup_variables()                           #####
################################################################################
class ArcFlowWOSetup_MILP(MILP):
    ##### Constructor
    def __init__(self, inst, modelName="AFwoSetup", bounded=True):
        super().__init__(modelName)
        ##### Attributes
        self.name = modelName
        self.model = Model(self.name)      # model
        self.obj_value = 0                 # objective value of MILP
  
        #Create variables
        variables = 0
        constraints = 0
             
        #Initialize auxiliary values
        beta = [0] * len(inst.M)
        for m in inst.M:
            beta[m] = 99999999
            for j in inst.J:
                if len(inst.T_j[j]) <= inst.C_m[m]:
                    if inst.p_mj[m][j] < beta[m]:
                        beta[m] = inst.p_mj[m][j]

        
        # ----- C_max variable : For the objective Function
        if bounded:
            self.H = inst.FmaxUB
            self.C_max = self.model.addVar(lb=inst.FmaxLB, ub=inst.FmaxUB, obj=1, vtype=GRB.CONTINUOUS, name='C_max')  # makespan
        else:
            print("AFwoSetup needs an initial UB to run!!")
            exit(0)
        
        print("AFwoSetup: creating model...")
        print("Initial UB = ", self.H)
        #----- X variables ---- Arc Variables for Job processing
        self.X_mjpq = {} # existence of the arc(jpqk) 
        # for m in range(0,inst.M):    #for each machine
        for m in inst.M:    #for each machine
            self.X_mjpq[m] = {}
            for j in inst.J:     #for each job
                self.X_mjpq[m][j] = {}
                if len(inst.T_j[j]) <= inst.C_m[m]:
                    for p in range(0, self.H - inst.p_mj[m][j] +1 ):   #for each time instant
                        self.X_mjpq[m][j][p] = {}
                        if len(inst.T_j[j]) <= inst.C_m[m]: #if there is enough slots on machine m for job j
                            if (m, j, p, p + inst.p_mj[m][j]) not in self.X_mjpq:   # the q here id equal H-proc_time[k-1][j-1] it's imposed
                                self.X_mjpq[(m, j, p, p + inst.p_mj[m][j])] = self.model.addVar(obj=0, vtype=GRB.BINARY, name='X[%s][%s][%s][%s]'%(m, j, p, p + inst.p_mj[m][j]))
                                variables + 1
        #----- Y variables ---- Arc Variables for Tool loading
        self.Y_mtpq = {}  # existence of the arc(tpqk) meaning if tool t is on the machine k at the
        for m in inst.M:
            for t in inst.T:
                if len(inst.J_t[t]) > 0:
                    minProc_mt = self.H #minimum processing time on machine m among jobs requiring tool t
                    for j in inst.J_t[t]:
                        if len(inst.T_j[j]) <= inst.C_m[m]: #if there is enough slots on machine m for job j
                            if (inst.p_mj[m][j] < minProc_mt): # if job j requires tool t and proc time is smaller
                                minProc_mt = inst.p_mj[m][j]
                            
                    for p in range(0, self.H + 1):                                                  
                        for q in range(p + minProc_mt, self.H + 1):
                            if (m, t, p, q) not in self.Y_mtpq:
                                self.Y_mtpq[(m, t, p, q)] = self.model.addVar(obj=0, vtype=GRB.BINARY, name='Y[%s][%s][%s][%s]'%(m, t, p, q))
                                variables + 1

        # ----- L variables ---- Dummy Arc Variables : for loss arcs
        self.L_mp = {}  # existence of the arc(pk) meaning if there is a loss on machine k at moment p to finish up the graph till H
        for m in inst.M:
            for p in range(beta[m], self.H):
                if (m, p) not in self.L_mp:
                    self.L_mp[(m, p)] = self.model.addVar(obj=0, vtype=GRB.CONTINUOUS, name='L[%s][%s]'%(m, p))
                    variables + 1
                    
        #Update model
        self.model.update()
        #print(variables, "variables created !!")
        
        ############################## Create_constraints ##############################
       # Constraint_1
        for j in inst.J: 
            C1 = 0
            for m in inst.M:
                if len(inst.T_j[j]) <= inst.C_m[m]: #if there is enough slots on machine m for job j
                    for p in range(0, self.H - inst.p_mj[m][j] +1 ):
                        if (m, j, p, p + inst.p_mj[m][j]) in self.X_mjpq:
                            C1 += self.X_mjpq[(m, j, p, p + inst.p_mj[m][j])]
            cname = "C1_Job_" + str(j)   
            self.model.addLConstr(C1 == 1, name=cname)
            constraints += 1
        #end for j

        # Constraint_2
        for m in inst.M:
            for q in range(0, self.H+1):
                C2 = 0
                #ARCS LEAVING q
                #Job arcs
                for j in inst.J:
                    for r in range(q+1, self.H+1):
                        if (m, j, q, r) in self.X_mjpq:
                            C2 += self.X_mjpq[(m, j, q, r)]
                                        
                #loss arcs
                if (m, q) in self.L_mp:
                    C2 += self.L_mp[(m, q)]
 
                #ARCS ARRIVING q
                #Job arcs
                for j in inst.J:
                    for p in range(0, q):
                        if (m, j, p, q) in self.X_mjpq:
                            C2 -= self.X_mjpq[(m, j, p, q)]                    
            
                #RHS + loss arcs rhs
                if q == 0:
                    C2 -= 1
                else:
                    if q == self.H:
                        C2 += 1
                        for p in range(0, self.H):
                            if (m, p) in self.L_mp:
                                C2 -= self.L_mp[(m, p)]
            
                cname = "C2_machine_time_" + str(m) + "_" + str(q)   
                self.model.addLConstr(C2 == 0, name=cname)
                constraints += 1
            #end for q
        #end for m
            
        # Constraint_3
        for m in inst.M:
            for q in range(0, self.H+1):
                C3 = 0
                #ARCS LEAVING q
                #Tools arcs
                for t in inst.T:
                    for r in range(q+1, self.H+1):
                        if (m, t, q, r) in self.Y_mtpq:
                            C3 += self.Y_mtpq[(m, t, q, r)]
            
                #ARCS ARRIVING q
                #Tools arcs
                for t in inst.T:
                    for p in range(0, q):
                        if (m, t, p, q) in self.Y_mtpq:
                            C3 -= self.Y_mtpq[(m, t, p, q)]                    

                #RHS + loss arcs rhs
                if q == 0:
                    C3 -= inst.C_m[m]
                else:
                    if q == self.H:
                        C3 += inst.C_m[m]
            
                cname = "C3_machine_time_" + str(m) + "_" + str(q)   
                self.model.addLConstr(C3 == 0, name=cname)
                constraints += 1
            #end for q
        #end for m
        
        # Constraint_5 - Tool requirements
        for m in inst.M:
            for j in inst.J: 
                for t in range(0, len(inst.T_j[j])):
                    tool = inst.T_j[j][t]
                    if len(inst.T_j[j]) <= inst.C_m[m]: #if there is enough slots on machine m for job j
                        for s in range(0, self.H - inst.p_mj[m][j] + 1):
                            C5 = 0
                            if (m, j, s, s + inst.p_mj[m][j]) in self.X_mjpq:
                                C5 += self.X_mjpq[(m, j, s, s + inst.p_mj[m][j])]
                                
                            for p in range(0, s+1): #including s
                                for q in range(s + inst.p_mj[m][j], self.H + 1):
                                    if (m, tool, p, q) in self.Y_mtpq:
                                        C5 -= self.Y_mtpq[(m, tool, p, q)]
    
                            cname = "C5_machine_job_tool_time_" + str(m) + "_" + str(j) + "_" + str(tool) + "_" + str(s)
                            self.model.addLConstr(C5 <= 0, name=cname)
                            constraints += 1
                        #end for s
                #end for t
            #end for j
        #end for m


        # Constraint_8 - Makespan constraint
        for m in inst.M:
            C8 = self.C_max
            for p in range(0, self.H + 1):
                for q in range(0, self.H + 1):
                    for j in inst.J: 
                        if (m, j, p, q) in self.X_mjpq:
                            C8 -= inst.p_mj[m][j] *self.X_mjpq[(m, j, p, q)]
                            
                    for t in inst.T:
                        if (m, t, p, q) in self.Y_mtpq:
                            C8 -= inst.sw_m[m] *self.Y_mtpq[(m, t, p, q)]                        
                            
            cname = "C8_machine_" + str(m)
            self.model.addLConstr(C8 >= -(inst.C_m[m] * inst.sw_m[m]), name=cname)
            constraints += 1

        # Valid inequalities
  
        self.model.setObjective( self.C_max, sense=GRB.MINIMIZE )
        
        #Update model
        self.model.update()
        
        self.relaxedModel = self.model.relax()
        #self.model.write("AFwoSetup.lp")

    '''
    def solve(self, timeLimit=60, OutputFlag=True, relax=False):
        if relax:
            self.relaxedModel = self.model.relax()
            self.relaxedModel.Params.OutputFlag = OutputFlag
            self.relaxedModel.params.timeLimit = timeLimit
            self.relaxedModel.optimize()
            return self.relaxedModel.ObjVal
        else:
            self.model.Params.OutputFlag = OutputFlag
            self.model.params.timeLimit = timeLimit
            self.model.relax()
            self.model.optimize()
            print("Status: ", self.model.status)
            if self.model.status in [2,9]:
                self.model.write("AFwoSetup_solution.sol")
                return self.model.ObjVal
            else:
                if self.model.status == 3:
                    print("Model is infeasible!!!")
                    # self.model.computeIIS()
                    # self.model.write("AFwoSetup.ilp")
                return -1
    '''
    
    def getDuals(self, inst):
        dual_mkt = {}
        for m in inst.M:
            dual_mkt[m] = {}
            for k in range(1,self.Km[m]):
                dual_mkt[m][k] = {}
                for t in self.W[m][k].keys():
                    dual_mkt[m][k][t] = self.relaxedModel.getAttr("Pi", [self.relaxedModel.getConstrByName('loading_on_machine_m%s_position_k%s_tool_t%s'%(m,k,t))])[0]
            
        dual_j = {}
        for j in inst.J:
            dual_j[j] = self.relaxedModel.getAttr("Pi", [self.relaxedModel.getConstrByName('exactly_one_block_j%s'%j)])[0]
        return dual_j, dual_mkt
    
    def addColumn(self, newBlock):
        
        return True
    
    
    def KTNS(self, inst):
        for m in inst.M:
            magazine = {}       # sequence of magazine state
            T = {}              # tools' required positions
            for k in self.X[m].keys():
                for j in inst.p_mj[m].keys():
                    for b, block in inst.Bmj[m][j].items():
                        if self.X[m][k][b].x > 0:
                            magazine[k] = []
                            for t in inst.T_j[j]:
                                magazine[k].append(t)
                                if t not in T:
                                    T[t] = [k]
                                else:
                                    T[t].append(k)
            NbSwitches = 0
            for k in magazine.keys():
                while (len(magazine[k]) < inst.C_m[m]) and (len(T.keys()) > 0):
                    soonest_k = len(magazine.keys())
                    best_t = -1
                    for t in T.keys():
                        if (t not in magazine[k]) and (soonest_k < T[t][0]):
                            soonest_k = T[t][0]
                            best_t = t
                    if (soonest_k < len(magazine.keys())) and (best_t >= 0):
                        magazine[k].append(best_t)
                        
                if k > 0:
                    NbSwitches += len([t for t in magazine[k] if t not in magazine[t-1]])
    
        return True
    
    
    def plotSolution(self,inst, name=""):
        if self.model.status in [2,9]:
            f, ax = plt.subplots(figsize=(10,10))
            ax.set_xlim([0,self.Fmax.x])
            ax.set_ylim([0,sum([c+2 for c in inst.C_m.values()])])

            y = 0
            for m in inst.M:
                x = 0
                lastb = 0
                for k in self.X[m].keys():
                    for t in self.W[m][k].keys():
                        if self.W[m][k][t].x > 0:
                            #print("W[m%s][k%s][t%s] = %s"%(m,k,t,self.W[m][k][t].x))
                            length = inst.sw_m[m]
                            ax.add_patch(Rectangle((x,y), length, 1, fc="red", ec="black", alpha=0.3))
                            plt.text(x+length/3,y + 0.33,"t"+str(t))
                            x += length
                    for j in inst.p_mj[m].keys():
                        for b, block in inst.Bmj[m][j].items():
                            if self.X[m][k][b].x > 0:
                                #print("X[m%s][k%s][b%s]"%(m,k,b), block)
                                lastb = b
                                length = inst.p_mj[m][j]
                                ax.add_patch(Rectangle((x,y), length, 1, fc="green", ec="black", alpha=0.3))
                                plt.text(x+length/3,y + 0.33,"j"+str(j))
                                
                                yt = y+1
                                for t in block:
                                    ax.add_patch(Rectangle((x,yt), length, 1, fc="yellow", ec="black", alpha=0.3))
                                    plt.text(x+length/3,yt + 0.33,"t"+str(t))
                                    yt += 1
                                x += length
                y += inst.C_m[m] + 2
                                
            f.savefig(name + "sol.png")
                    

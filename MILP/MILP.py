from gurobipy import *

################################################################################
#####                              Class MILP()                            #####
################################################################################
class MILP:
    ##### MILP(name)
    def __init__(self, modelName = "MILP", callBack=False):
        ##### Attributes
        self.name = modelName             # name of the model (used to write lp and sol files)
        self.model = Model(self.name)      # Gurobi model
        self.objValue = -1              # objective value of MILP
        self.nbColumns = -1             # number of variables
        self.nbRows = -1                # number of canstraints
        self.nbNodes = -1               # number of nodes after resolution
        self.modelUB = -1               # upper bound
        self.modelLB = -1               # final lower bound
        self.modelGap = -1              # gurobi optimization gap
        self.modelExec_time = -1        # gurobi execution time
        self.callBack = callBack
        self.cbData = -1


    ##### solve(time limit, output information, relaxation
    def solve(self, timeLimit = 3600, outputFlag = True, relax = False, iUB = -1):
        #Define callback first
        if relax:
            self.relaxedModel = self.model.relax()
            self.relaxedModel.Params.OutputFlag = outputFlag
            self.relaxedModel.params.timeLimit = timeLimit
            self.relaxedModel.optimize()
            return self.relaxedModel.ObjVal
        else:
            self.model.Params.OutputFlag = outputFlag
            self.model.params.timeLimit = timeLimit

            self.model.Params.MIPGap = 0.0
            self.model.Params.MIPGapAbs = 0.99999
            self.model.Params.Threads = 1
            if(iUB != -1): self.model.Params.Cutoff = iUB
            self.model.Params.MIPFocus = 1 #1 = feasible solutions
            #self.model.Params.Cuts = 0
            #self.model.Params.Presolve = 0
            #self.model.Params.Aggregate = 0
            #self.model.Params.Heuristics = 0.95    #default = 0.05
            #self.model.Params.DisplayInterval = 10
            #self.model.Params.CutPasses = 0
            self.model.Params.LazyConstraints = 1            
            #self.model.relax()

            self.model.optimize()
            
            #print("Status: ", self.model.status)
            self.nbColumns = self.model.NumVars
            self.nbRows = self.model.NumConstrs
            
            if self.model.status in [2,9]: #optimal or feasible solution found
                self.nbNodes = self.model.NodeCount
                self.modelUB = self.model.ObjVal
                self.modelLB = self.model.ObjBound
                self.modelGap = self.model.MIPGap
                self.modelExec_time = self.model.Runtime
                self.objValue = self.model.ObjVal
                #self.model.write("Solution.sol")
            else:
                if self.model.status == 3:
                    print("Model is infeasible!!!")
                    self.model.computeIIS()
                    self.model.write("Infeasible_model.ilp")
                self.modelUB = -1

            return self.modelUB
    
    ##### getDuals
    def getDuals(self):
        duals = {}
        for c in self.relaxedModel.getConstrs():
            duals[c.VarName] = c.Pi
            
        return duals
    
    
    ##### writeModel(directory path)
    def writeModel(self, path = os.path.join("output","lp") ):
        self.model.write(os.path.join(path, self.name + '.lp') )

    ##### writeSolution(directory path)
    def writeSolution(self, path = os.path.join("output","sol") ):
        self.model.write(os.path.join(path, self.name + '.sol') )

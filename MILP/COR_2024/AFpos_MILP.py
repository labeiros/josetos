from .Improved_MILP import *
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle

################################################################################
#####                               Class AFpos_MILP()                       #####
################################################################################
class AFpos_MILP(Improved_MILP):
    #-----------------------
    #----------------------- Constructor
    def __init__(self, inst, modelName = "AFpos_MILP", cb = False):
        super().__init__(inst, modelName)
        self.callBack=cb
        ##### Attributes
    
        #-----
        self.x = {}     # X_{m,k,j} j in process at position k of machine m
        self.y = {}     # Y_{m,k,k',t} if t loaded from k to k' of machine m
        self.l = {}     # l_{m,k}  if k last used position of machine m
        self.beta = {}   # beta_{m,t} if t loaded at least once on machine m
        #-----
        
        for m in inst.M:
            self.x[m] = {}
            self.y[m] = {}
            self.l[m] = {}
            self.beta[m] = {}
            for k in range(self.K_m[m]):
                self.x[m][k] = {}
                self.y[m][k] = {}
                self.l[m][k] = self.model.addVar(vtype = GRB.BINARY, name='l[%s][%s]'%(m, k) )
                for j in inst.p_mj[m].keys():
                    self.x[m][k][j] = self.model.addVar(vtype = GRB.BINARY, name='x[%s][%s][%s]'%(m, k, j) )
                    self.var_mk[m][k].append(self.x[m][k][j])
                    for t in inst.T_j[j]:
                        if t not in self.beta[m].keys():
                            self.beta[m][t] = self.model.addVar(vtype = GRB.BINARY, name = 'beta[%s][%s]'%(m, t) )
                self.l[m][self.K_m[m]] = self.model.addVar(vtype = GRB.BINARY, name='l[%s][%s]'%(m, self.K_m[m]) )

            for k in range(self.K_m[m]):
                self.var_mk[m][k].append(self.l[m][k+1])
                if k < self.K_m[m]:
                    for k2 in range(k+1,self.K_m[m]+1):
                        self.y[m][k][k2] = {}
                        for j in inst.p_mj[m].keys():
                            for t in inst.T_j[j]:
                                if t not in self.y[m][k][k2].keys():
                                    self.y[m][k][k2][t] = self.model.addVar(vtype = GRB.BINARY, name = 'y[%s][%s][%s][%s]'%(m, k, k2, t) )
                                    if k > 0:
                                        self.var_mk[m][k].append(self.y[m][k][k2][t])
                                    self.var_mk[m][k2-1].append(self.y[m][k][k2][t])
                                    
        self.Cmax = self.model.addVar(lb=inst.CmaxLB, ub=inst.CmaxUB, vtype = GRB.INTEGER, name = 'Cmax')         # makespan

        #-----
        #----- generate_constraints for model - adapted Akhundov_2024 (page 8-10)
        #----- constraint (1)
        for j in inst.J:
            self.model.addLConstr( quicksum(self.x[m][k][j] for m in inst.M if j in inst.p_mj[m].keys() for k in range(self.K_m[m]) ) == 1,'C1_j%s'%j)

        #----- constraint
        for m in inst.M:
            self.model.addLConstr( self.l[m][0] + quicksum(self.x[m][0][j] for j in self.x[m][0].keys()) == 1 ,'C2_m%s'%(m) )
        
            self.model.addLConstr(1 - quicksum(self.x[m][self.K_m[m]-1][j] for j in self.x[m][0].keys()) == quicksum(self.l[m][k] for k in range(self.K_m[m])) ,'C3_m%s'%(m) )
            
            for k in range(1,self.K_m[m]):
                self.model.addLConstr(self.l[m][k] + quicksum(self.x[m][k][j] for j in self.x[m][0].keys()) - quicksum(self.x[m][k - 1][j] for j in self.x[m][0].keys()) == 0 ,'C4_m%s_k%s'%(m, k) )
            
            #print(self.y[m])
            self.model.addLConstr(quicksum(self.y[m][0][k][t] for k in range(1,self.K_m[m]+1) for t in self.beta[m].keys()) == inst.C_m[m] ,'C5_m%s'%(m) )
                
            self.model.addLConstr(quicksum(self.y[m][k][self.K_m[m]][t] for k in range(self.K_m[m]) for t in self.beta[m].keys()) == inst.C_m[m] ,'C6_m%s'%(m) )
            
            for k in self.y[m].keys():
                if k > 0 and k < self.K_m[m]:
                    self.model.addLConstr(quicksum(self.y[m][k2][k][t] for k2 in range(k) for t in self.beta[m].keys()) == quicksum(self.y[m][k][k2][t] for k2 in range(k+1,self.K_m[m]+1) for t in self.beta[m].keys()) ,'C7_m%s_k%s'%(m, k) )
              
            
            for k in range(self.K_m[m]):
                for j in self.x[m][k].keys():
                    for t in inst.T_j[j]:
                        self.model.addLConstr( self.x[m][k][j] <= quicksum(self.y[m][k2][k3][t] for k2 in range(k+1) for k3 in range(k+1, self.K_m[m]+1))  ,'C8_m%s_k%s_j%s_t%s'%(m, k, j, t) )
            
            #----- define Cmax
            self.model.addLConstr(self.Cmax >= quicksum(inst.sw_m[m] * self.y[m][k][k2][t] for k in range(1,self.K_m[m]) for k2 in range(k+1,self.K_m[m]+1) for t in self.y[m][k][k2].keys()) + quicksum( inst.p_mj[m][j]*self.x[m][k][j] for k in self.x[m].keys() for j in self.x[m][k].keys()) ,'C9_m%s'%m )
            
            
            self.model.addLConstr(quicksum( j*self.x[m][k][j] for k in range(math.ceil(self.K_m[m]/2)) for j in self.x[m][k].keys()) >= quicksum( j*self.x[m][k][j] for k in range(math.ceil(self.K_m[m]/2), self.K_m[m]) for j in self.x[m][k].keys()) ,'breaking_symetry_m%s'%m )
            
            
            #---- valid inequalities
            for t in self.beta[m].keys():
                for j in inst.J_t[t]:
                    if j in inst.p_mj[m].keys():
                        self.model.addLConstr( quicksum(self.x[m][k][j] for k in range(self.K_m[m]) ) <= self.beta[m][t],'tool_presence_m%s_t%s_j%s'%(m,t,j))
                
                #self.model.addLConstr( quicksum(self.y[m][k][k2][t] for k in range(self.K_m[m]) for k2 in range(k+1,self.K_m[m])) >= self.beta[m][t] - inst.C_m[m],'tool_loading_m%s_t%s'%(m,t))
                
                # for k in range(1,self.K_m[m]-1):
                for k in range(1,self.K_m[m]):
                    self.model.addLConstr( quicksum(self.y[m][k2][k][t] for k2 in range(k)) + quicksum(self.y[m][k][k2][t] for k2 in range(k+1,self.K_m[m]+1)) <= self.beta[m][t],'interval_continuity_m%s_t%s_k%s'%(m,t,k))
            
            self.model.addLConstr( quicksum(self.y[m][k][k2][t] for t in self.beta[m].keys() for k in range(self.K_m[m]) for k2 in range(k+1,self.K_m[m]+1)) >= quicksum(self.beta[m][t] for t in self.beta[m].keys()) - inst.C_m[m],'tool_loading_m%s'%m)
            
            #----- constraint LB on the number of position NOT VALID!!!
            #self.model.addLConstr( quicksum(self.x[m][k][j] for j in inst.J if j in inst.p_mj[m].keys() for k in range(self.K_m[m])) >= self.min_K_m[m] ,'min_pos_m%s'%(m) )
            
        #-----
        #----- objective_function
        self.model.setObjective( self.Cmax, sense = GRB.MINIMIZE)
        #self.model.write(modelName +'.lp' )
        self.model.update()
        
        if self.callBack == True:
            #DEFINE CALLBACK OBJECT
            self.cbData = CallbackData(inst, self.K_m, self.treshold_UB, self.model.getVars())
    
    #-----------------------
    #-----------------------
    def warmStart(self, blocks_mj, seq_m):
        for m in self.var_mk.keys():
            firstHalf = [seq_m[m][k] for k in range(math.floor(min(len(seq_m[m]),self.K_m[m]/2)))]
            if sum(firstHalf) < sum([j for j in seq_m[m] if j not in firstHalf]):
                seq_m[m].reverse()
            for k in self.var_mk[m].keys():
                for var in self.var_mk[m][k]:
                    var.start = False
                    
        for m, seq in seq_m.items():
            for k in range(len(seq)):
                j = seq[k]
                self.x[m][k][j].start = 1.0
                
            self.l[m][len(seq)].start = 1.0
            
            magazine = set()
            for k in range(len(seq)):
                for t in blocks_mj[m][seq[k]]:
                    if len(magazine) < self.inst.C_m[m]:
                        start = 0
                    else:
                        start = k
                    end = self.K_m[m]
                    self.beta[m][t].start = 1.0
                    for k2 in range(k+1, len(seq)):
                        if t not in blocks_mj[m][seq[k2]] and len(blocks_mj[m][seq[k2]]) == self.inst.C_m[m]:
                            end = k2
                            k = k2+1
                            break
                    self.y[m][start][end][t].start = 1.0
                    #print("machine %s, tool %s: %s --> %s"%(m, t, start, end))
                    magazine.add(t)
                    #print(magazine)
        return True
  
    def printSolution(self, inst):
        print("Cmax: %s"%self.Cmax.x)
        for m in self.x.keys():
            Fm = 0
            for k in self.x[m].keys():
                for k2 in self.y[m][k].keys():
                    for t in self.y[m][k][k2].keys():
                        if self.y[m][k][k2][t].x > 0.5:
                            if k > 0:
                                print("Y[%s][%s][%s][%s]"%(m,k,k2,t), "Fm: %s + %s"%(Fm, inst.sw_m[m]))
                                Fm += inst.sw_m[m]
                            else:
                                print("Y[%s][%s][%s][%s]"%(m,k,k2,t))

                for j in self.x[m][k].keys():
                    if self.x[m][k][j].x > 0.5:
                        print("X[%s][%s][%s]"%(m,k,j), "Fm: %s + %s"%(Fm, inst.p_mj[m][j]))
                        Fm += inst.p_mj[m][j]
    
    
    #-----------------------
    #-----------------------
    def plotSolution(self, inst, name = ""):
        if self.model.status in [2,9]:
            f, ax = plt.subplots(figsize = (10, 10) )
            ax.set_xlim([0,self.Cmax.x])
            ax.set_ylim([0,sum([c+2 for c in inst.C_m.values()])])

            y = 0
            for m in inst.M:
                x = 0
                lastb = 0
                for k in self.x[m].keys():
                    for t in self.beta[m][k].keys():
                        if self.beta[m][k][t].x > 0:
                            #print("W[m%s][k%s][t%s] = %s"%(m,k,t,self.beta[m][k][t].x))
                            length = inst.sw_m[m]
                            ax.add_patch(Rectangle((x,y), length, 1, fc="red", ec="black", alpha=0.3))
                            plt.text(x+length/3,y + 0.33,"t"+str(t))
                            x += length
                    for j in self.x[m][k].keys():
                        if self.x[m][k][j].x > 0:
                            #print("X[m%s][k%s][b%s]"%(m,k,j))
                            length = inst.p_mj[m][j]
                            ax.add_patch(Rectangle((x,y), length, 1, fc="green", ec="black", alpha=0.3))
                            plt.text(x+length/3,y + 0.33,"j"+str(j))
                            
                            yt = y+1
                            for t in self.beta[m][k].keys():
                                if self.beta[m][k][t].x > 0:
                                    ax.add_patch(Rectangle((x,yt), length, 1, fc="yellow", ec="black", alpha=0.3))
                                    plt.text(x+length/3,yt + 0.33,"t"+str(t))
                                    yt += 1
                            x += length
                y += inst.C_m[m] + 2
                                
            f.savefig(name + "sol.png")

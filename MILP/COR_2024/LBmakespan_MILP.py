from gurobipy import *
from instance import *
from MILP.MILP import *

################################################################################
#####                            Class LBmakespan_MILP()                       #####
################################################################################
class LBmakespan_MILP(MILP):
    #-----------------------
    #----------------------- Constructor
    def __init__(self, inst, modelName = "LowerBoundCmaxMILP"):
        super().__init__(modelName)
        self.alpha = {}                 # alpha_{mj} = 1 if job j is selected
        self.beta = {}                  # beta_{mt} = 1 if tool t is loaded on machine m
        self.Cmax = self.model.addVar(obj = 0, vtype = GRB.INTEGER, name = 'Cmax')
        
        for m in inst.M:
            self.alpha[m] = {}
            self.beta[m] = {}
            for j in inst.p_mj[m].keys():
                self.alpha[m][j] = self.model.addVar(obj = 0, vtype=GRB.BINARY, name = 'alpha[%s]'%j)
            for t in inst.T:
                self.beta[m][t] = self.model.addVar(obj=0, vtype=GRB.BINARY, name = 'beta[%s]'%t)
        
        for m in inst.M:
            for j in inst.p_mj[m].keys():
                for t in inst.T_j[j]:
                    print(self.beta[m][t])
                    print(self.alpha[m][j])
                    self.model.addConstr(self.beta[m][t] >= self.alpha[m][j],'j%s_tool_t%s_requirement_on_machine_m%s'%(j,t,m) )
                
            self.model.addConstr( (quicksum(self.beta[m][t] for t in self.beta[m].keys() ) - inst.C_m[m]) * inst.sw_m[m] + quicksum(inst.p_mj[m][j] * self.alpha[m][j] for j in self.alpha[m].keys() ) <= self.Cmax ,'makespan')
        
        for j in inst.J:
            self.model.addConstr(quicksum(self.alpha[m][j] for m in self.alpha.keys() if j in self.alpha[m].keys() ) == 1,'do_job_j%s'%j)
        
        self.model.setObjective(self.Cmax, sense = GRB.MINIMIZE )

                        

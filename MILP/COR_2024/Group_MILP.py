from .Improved_MILP import *
import networkx as nx
from .GroupSize_MILP import *

################################################################################
#####                            Class Group_MILP()                     #####
################################################################################
class Group_MILP(Improved_MILP):
    ##### MILP(name)
    def __init__(self, inst, modelName = "Group_MILP"):
        super().__init__(inst, modelName)
        
        # compute cliques
        self.Q_m = {}
        for m in inst.M:
            self.Q_m[m] = []
            G = nx.Graph()
            for j in inst.J:
                for j2 in inst.J:
                    if j2 > j:
                        if len(inst.T_j[j]) <= inst.C_m[m] and  len(inst.T_j[j2]) <= inst.C_m[m] and len(set(inst.T_j[j] + inst.T_j[j2])) > inst.C_m[m]:
                            G.add_edge(j, j2)
            max_clique = nx.approximation.max_clique(G)
            while len(max_clique) > 1:
                self.Q_m[m].append(max_clique)
                for j in max_clique:
                    G.remove_node(j)
                max_clique = nx.approximation.max_clique(G)
        
        # compute max group size per (m,j)
        self.Delta_mj = {}
        for m in inst.M:
            self.Delta_mj[m] = {}
            for j in inst.p_mj[m].keys():
                GMILP = GroupSize_MILP(inst, m, fixedJ = [j])
                self.Delta_mj[m][j] = int(min(GMILP.solve(outputFlag=False), self.K_m[m]))
                GMILP.model.dispose()

from functools import partial
from MILP.MILP import *
from instance import *
from .MachineKP_MILP import *
from .MachineThreshold_MILP import *


def mycallback(model, where, *, milp, logfile):
    # new heuristic solution
    if where == GRB.Callback.MIPSOL:
        # MIP solution callback
        obj = model.cbGet(GRB.Callback.MIPSOL_OBJ)
        new_lazy_constr = 0

        for m, t in milp.T_m.items():
            if obj < t["Cmax"]-0.05:
            
                KP = MachineKP_MILP(milp.inst, m)
                K = int(KP.solve(outputFlag = False))
                KP.model.dispose()
            
                milp.nb_pos_removed += t["K_m"] - K
                
                for k in range(K+1, t["K_m"]+1):
                    for var in milp.var_mk[m][k]:
                        model.cbLazy(var <= 0)
                
                Th = MachineThreshold_MILP(milp.inst, m, K)
                milp.T_m[m] = {"Cmax":int(Th.solve(outputFlag = False)), "K_m":K}
                Th.model.dispose()
                
    elif where == GRB.Callback.MESSAGE:
        # Message callback
        msg = model.cbGet(GRB.Callback.MSG_STRING)
        logfile.write(msg)



################################################################################
#####                            Class Improved_MILP()                     #####
################################################################################
class Improved_MILP(MILP):
    ##### MILP(name)
    def __init__(self, inst, modelName = "Improved_MILP"):
        super().__init__(modelName)
                
        self.Cmax = self.model.addVar(ub=inst.CmaxUB, obj = 1, vtype = GRB.INTEGER, name = 'Cmax')         # makespan
        self.model.setObjective( self.Cmax, sense = GRB.MINIMIZE)

        self.K_m = {m:len(inst.J) for m in inst.M}
        
        self.var_mk = {}
        
        self.T_m = {}
        
        self.inst = inst
        
        self.nb_pos_removed = 0
        
        for m in inst.M:
            KP = MachineKP_MILP(inst, m)
            self.K_m[m] = int(KP.solve(outputFlag = False))
            KP.model.dispose()
            
            Th = MachineThreshold_MILP(inst, m, self.K_m[m])
            self.T_m[m] = {"Cmax":int(Th.solve(outputFlag = False)), "K_m":self.K_m[m]}
            Th.model.dispose()
            
            self.var_mk[m]= {k : [] for k in range(self.K_m[m])}

    ##### solve(time limit, output information, relaxation
    def solve(self, timeLimit = 3600, outputFlag = True, relax = False, iUB = -1):
        #Define callback first
        if relax:
            super().solve(timeLimit, outputFlag, relax, iUB)
        else:
            self.model.Params.OutputFlag = outputFlag
            self.model.params.timeLimit = timeLimit

            self.model.Params.MIPGap = 0.0
            self.model.Params.MIPGapAbs = 0.99999
            self.model.Params.Threads = 1
            if(iUB != -1):
                self.model.Params.Cutoff = iUB
            self.model.Params.MIPFocus = 1 #1 = feasible solutions
            self.model.Params.LazyConstraints = 1            

            # Solve model and print solution information
            # Set up callback function with required arguments
            callback_func = partial(mycallback, milp=self, logfile=open(self.name + "_log_callback.log", "w"))
            self.model.optimize(callback_func)
            
            #print("Status: ", self.model.status)
            self.nbColumns = self.model.NumVars
            self.nbRows = self.model.NumConstrs
            
            if self.model.status in [2,9]: #optimal or feasible solution found
                self.nbNodes = self.model.NodeCount
                self.modelUB = self.model.ObjVal
                self.modelLB = self.model.ObjBound
                self.modelGap = self.model.MIPGap
                self.modelExec_time = self.model.Runtime
                self.objValue = self.model.ObjVal
                #self.model.write("Solution.sol")
            else:
                if self.model.status == 3:
                    print("Model is infeasible!!!")
                    self.model.computeIIS()
                    self.model.write("Infeasible_model.ilp")
                self.modelUB = -1

            return self.modelUB

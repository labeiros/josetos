from instance import *
from MILP.MILP import *

################################################################################
#####                             GroupSize_MILP()                              #####
################################################################################

class GroupSize_MILP(MILP):
    def __init__(self, instance, m, fixedJ = [], modelName = "GroupSize_MILP"):
        super().__init__(modelName + "_" + str(m))
      
        self.alpha = {}                 # alpha_{j} = 1 if job j is selected
        self.beta = {}                  # beta_{t} = 1 if tool t is loaded on machine m
        for j in instance.p_mj[m].keys():
            if j in fixedJ:
                lb = 1
            else:
                lb = 0
            self.alpha[j] = self.model.addVar(lb=lb, obj = 1, vtype = GRB.BINARY, name = 'alpha[%s]'%j)
        
        for t in instance.T:
            self.beta[t] = self.model.addVar(obj = 0, vtype = GRB.BINARY, name = 'beta[%s]'%t)
                    
        #Tool using constraints
        for j in self.alpha.keys():
            for t in instance.T_j[j]:
                self.model.addLConstr(self.beta[t] >= self.alpha[j],'C1_j%s_t%s'%(j,t))
        
        self.model.addLConstr( quicksum(self.beta[t] for t in self.beta.keys() ) <= instance.C_m[m] ,'C2')

        self.model.setObjective(quicksum(self.alpha[j] for j in self.alpha.keys()), sense = GRB.MAXIMIZE )

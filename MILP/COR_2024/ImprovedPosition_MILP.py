from .Improved_MILP import *
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle


################################################################################
#####                            Class ImprovedPosition_MILP()                       #####
################################################################################
class ImprovedPosition_MILP(Improved_MILP):
    #-----------------------
    #----------------------- Constructor
    def __init__(self, inst, modelName = "ImprovedPositionMILP"):
        super().__init__(inst, modelName)
        ##### Attributes

        #----- dict of nb. positions k per m
        
        #-----
        self.x = {}     # x_{m,k,j}
        self.v = {}     # v_{m,k,t}
        self.u = {}     # u_{m,k,t}

        #-----
        #----- generate_variables for model
        for m in inst.M:
            self.x[m] = {}
            self.v[m] = {}
            self.u[m] = {}
            for k in range(self.K_m[m]):
                self.x[m][k] = {}
                self.v[m][k] = {}
                self.u[m][k] = {}
                for j in inst.p_mj[m].keys():
                    self.x[m][k][j] = self.model.addVar(obj = 0, vtype = GRB.BINARY, name='x[%s][%s][%s]'%(m, k, j) )
                    self.var_mk[m][k].append(self.x[m][k][j])
                    for t in inst.T_j[j]:
                        if t not in self.v[m][k].keys():
                            self.v[m][k][t] = self.model.addVar(obj = 0, vtype = GRB.BINARY, name = 'v[%s][%s][%s]'%(m, k, t) )
                            self.var_mk[m][k].append(self.v[m][k][t])
                            self.u[m][k][t] = self.model.addVar(obj = 0, vtype = GRB.BINARY, name = 'u[%s][%s][%s]'%(m, k, t) )
                            self.var_mk[m][k].append(self.u[m][k][t])
        #-----
        #----- generate_constraints for model - Calmels_2022 (page 68)
        #----- constraint (1)
        for j in inst.J:
            self.model.addLConstr( quicksum(self.x[m][k][j] for m in inst.M if j in inst.p_mj[m].keys() for k in range(self.K_m[m]) ) == 1,'C1_m%s'%j)

        #----- constraint (2) - (4) - (5)
        for m in inst.M:
            for k in range(self.K_m[m]):
                #----- constraint (2)
                self.model.addLConstr( quicksum(self.x[m][k][j] for j in self.x[m][k].keys()) <= 1,'C2_m%s_k%s'%(m, k) )
                #----- constraint (5)
                self.model.addLConstr( quicksum(self.v[m][k][t] for t in self.v[m][k].keys()) <= inst.C_m[m],'C5_m%s_k%s'%(m, k) )
                #----- constraint (4)
                for t in self.v[m][k].keys():
                    self.model.addLConstr( quicksum(self.x[m][k][j] for j in inst.J_t[t] if j in self.x[m][k].keys()) <= self.v[m][k][t],'C4_m%s_k%s_t%s'%(m, k, t) )

        #----- constraint (3) - (6)
        for m in inst.M:
            for k in range(1,self.K_m[m]):
                #----- constraint (3)
                self.model.addLConstr( quicksum(self.x[m][k][j] for j in self.x[m][k].keys()) <= quicksum(self.x[m][k-1][j] for j in self.x[m][k-1].keys()),'C3_m%s_k%s'%(m, k) )
                #----- constraint (6)
                for t in self.v[m][k].keys():
                    self.model.addLConstr( self.v[m][k][t] - self.v[m][k-1][t] <= self.u[m][k][t],'C6_m%s_k%s_t%s'%(m, k, t) )

        #----- constraint (7) - define Cmax
        for m in inst.M:
            self.model.addLConstr(self.Cmax >= quicksum(inst.sw_m[m] * self.u[m][k][t] for k in self.u[m].keys() for t in self.u[m][k].keys()) + quicksum( inst.p_mj[m][j]*self.x[m][k][j] for k in self.x[m].keys() for j in self.x[m][k].keys()) ,'Cmax_m%s'%m )
        

        #----- breaking symetry constraint () -
        for m in inst.M:
            self.model.addLConstr( quicksum(j * self.x[m][k][j] for k in self.x[m].keys() if k <= max(self.x[m].keys())/2 for j in self.x[m][k].keys()) >= quicksum(j * self.x[m][k][j] for k in self.x[m].keys() if k > max(self.x[m].keys())/2 for j in self.x[m][k].keys()) ,'symetry_m%s'%m )
        #-----
        #----- objective_function
        self.model.setObjective( self.Cmax, sense = GRB.MINIMIZE)
        #self.model.write(modelName +'.lp' )
        self.model.update()
        
    #-----------------------
    #-----------------------

    def retrieveBlocks(self, inst):
        for m in self.x.keys():
            for k in self.x[m].keys():
                for j in self.x[m][k].keys():
                    if self.x[m][k][j].x > 0:
                        block = []
                        for t in self.v[m][k].keys():
                            if self.v[m][k][t].x > 0:
                                block.append(t)
                        inst.addBlock(m, j, block)
                        
    
    #-----------------------
    #-----------------------
    def warmStart(self, blocks_mj, seq_m):
        for m in self.var_mk.keys():
            firstHalf = [seq_m[m][k] for k in range(math.floor(min(len(seq_m[m]),self.K_m[m]/2)))]
            if sum(firstHalf) < sum([j for j in seq_m[m] if j not in firstHalf]):
                seq_m[m].reverse()
            for k in self.var_mk[m].keys():
                for var in self.var_mk[m][k]:
                    var.start = 0.0
                    
        for m, seq in seq_m.items():
            for k in range(len(seq)):
                j = seq[k]
                self.x[m][k][j].start = 1.0
                for t in blocks_mj[m][j]:
                    self.v[m][k][t].start = 1.0
                    if k > 0 and t not in blocks_mj[m][seq[k-1]]:
                        self.u[m][k][t].start = 1.0

    #-----------------------
    #-----------------------
    def retrieveBlocks(self, inst):
        for m in self.x.keys():
            for k in self.x[m].keys():
                for j in self.x[m][k].keys():
                    if self.x[m][k][j].x > 0:
                        block = []
                        for t in self.v[m][k].keys():
                            if self.v[m][k][t].x > 0:
                                block.append(t)
                        inst.addBlock(m, j, block)
    
    #-----------------------
    #-----------------------
    def plotSolution(self, inst, name = ""):
        if self.model.status in [2,9]:
            f, ax = plt.subplots(figsize = (10, 10) )
            ax.set_xlim([0,self.Cmax.x])
            ax.set_ylim([0,sum([c+2 for c in inst.C_m.values()])])

            y = 0
            for m in inst.M:
                x = 0
                lastb = 0
                for k in self.x[m].keys():
                    for t in self.u[m][k].keys():
                        if self.u[m][k][t].x > 0:
                            #print("W[m%s][k%s][t%s] = %s"%(m,k,t,self.u[m][k][t].x))
                            length = inst.sw_m[m]
                            ax.add_patch(Rectangle((x,y), length, 1, fc="red", ec="black", alpha=0.3))
                            plt.text(x+length/3,y + 0.33,"t"+str(t))
                            x += length
                    for j in self.x[m][k].keys():
                        if self.x[m][k][j].x > 0:
                            print("x[m%s][k%s][j%s]"%(m,k,j), inst.p_mj[m][j])
                            length = inst.p_mj[m][j]
                            ax.add_patch(Rectangle((x,y), length, 1, fc="green", ec="black", alpha=0.3))
                            plt.text(x+length/3,y + 0.33,"j"+str(j))
                            
                            yt = y+1
                            for t in self.v[m][k].keys():
                                if self.v[m][k][t].x > 0:
                                    ax.add_patch(Rectangle((x,yt), length, 1, fc="yellow", ec="black", alpha=0.3))
                                    plt.text(x+length/3,yt + 0.33,"t"+str(t))
                                    yt += 1
                            x += length
                y += inst.C_m[m] + 2
                                
            f.savefig(name + "sol.png")
                    

from .Group_MILP import *
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle


################################################################################
#####                               Class JGS_MILP()                       #####
################################################################################
class JGS_MILP(Group_MILP):
    #-----------------------
    #----------------------- Constructor
    def __init__(self, inst, modelName = "JGS_MILP", cb=False):
        super().__init__(inst, modelName)
        ##### Attributes
        self.callBack=cb
        
        #-----
        self.x = {}     # x_{m,k,j} j in bin k of machine m
        self.v = {}     # y_{m,k,t} t in bin k of machine m
        self.u = {}     # u_{m,k,t} t loaded to bin k of machine m
        self.w = {}     # w_{m,k,t} t removed from bin k of machine m
        self.z = {}     # z_{m,k} k of machine m not empty
        #-----
        #----- generate_variables for model
        for m in inst.M:
            self.x[m] = {}
            self.v[m] = {}
            self.u[m] = {}
            self.w[m] = {}
            self.z[m] = {}
            for k in range(self.K_m[m]):
                self.x[m][k] = {}
                self.v[m][k] = {}
                self.u[m][k] = {}
                self.w[m][k] = {}
                self.z[m][k] = self.model.addVar(obj = 0, vtype = GRB.BINARY, name='z[%s][%s]'%(m, k) )
                for j in inst.p_mj[m].keys():
                    self.x[m][k][j] = self.model.addVar(obj = 0, vtype = GRB.BINARY, name='x[%s][%s][%s]'%(m, k, j) )
                    self.var_mk[m][k].append(self.x[m][k][j])
                    for t in inst.T_j[j]:
                        if t not in self.u[m][k].keys():
                            self.v[m][k][t] = self.model.addVar(obj = 0, vtype = GRB.BINARY, name = 'v[%s][%s][%s]'%(m, k, t) )
                            self.var_mk[m][k].append(self.v[m][k][t])
                            self.u[m][k][t] = self.model.addVar(obj = 0, vtype = GRB.BINARY, name = 'u[%s][%s][%s]'%(m, k, t) )
                            self.var_mk[m][k].append(self.u[m][k][t])
                            self.w[m][k][t] = self.model.addVar(obj = 0, vtype = GRB.BINARY, name = 'w[%s][%s][%s]'%(m, k, t) )
            
            self.w[m][self.K_m[m]] = {}
            for t in self.w[m][0].keys():
                self.w[m][self.K_m[m]][t] = self.model.addVar(obj = 0, vtype = GRB.BINARY, name = 'w[%s][%s][%s]'%(m, self.K_m[m], t) )
            
            for k in self.w[m].keys():
                if k > 0:
                    for t in self.w[m][k].keys():
                        self.var_mk[m][k-1].append(self.w[m][k][t])
        #-----
        #----- generate_constraints for model - adapted Akhundov_2024 (page 8-10)
        #----- constraint (1)
        for j in inst.J:
            self.model.addLConstr( quicksum(self.x[m][k][j] for m in inst.M if j in inst.p_mj[m].keys() for k in range(self.K_m[m]) ) == 1,'C1_j%s'%j)

        #----- constraint (2) - (4) - (5)
        for m in inst.M:
            for k in range(self.K_m[m]):
                #----- constraint (2)
                self.model.addLConstr( quicksum(self.v[m][k][t] for t in self.v[m][k].keys()) <= inst.C_m[m] ,'C2_m%s_k%s'%(m, k) )
                for j in self.x[m][k].keys():
                    for t in inst.T_j[j]:
                        #----- constraint (3)
                        self.model.addLConstr( self.x[m][k][j] <= self.v[m][k][t]  ,'C3_m%s_k%s_j%s_t%s'%(m, k, j, t) )
                for t in self.v[m][k].keys():
                    if k-1 in self.v[m].keys():
                        #----- constraint (4)
                        self.model.addLConstr( self.v[m][k][t] <= self.v[m][k-1][t] + self.u[m][k][t] - self.w[m][k][t]  ,'C4_m%s_k%s_t%s'%(m,k,t) )
                        
                for j in self.x[m][k].keys():
                    #----- constraint (5)
                    self.model.addLConstr( self.x[m][k][j]  <= quicksum(self.u[m][k][t] for t in self.u[m][k].keys()),'C5_m%s_k%s_j%s'%(m,k,j) )
                if k-1 in self.u[m].keys():
                    #----- constraint (6)
                    self.model.addLConstr( quicksum(inst.C_m[m] * self.u[m][k-1][t] for t in self.u[m][k].keys()) >= quicksum(self.u[m][k][t] for t in self.u[m][k].keys()),'C6_m%s_k%s'%(m, k) )
                #----- constraint (7)
                self.model.addLConstr( quicksum(inst.C_m[m] * self.w[m][k][t] for t in self.w[m][k].keys()) >= quicksum(self.w[m][k+1][t] for t in self.w[m][k+1].keys()),'C7_m%s_k%s'%(m, k) )
                #----- constraint (8)
                for t in self.w[m][k].keys():
                    self.model.addLConstr( quicksum(self.x[m][k][j] for j in inst.J_t[t] if j in self.x[m][k].keys()) >= self.w[m][k][t],'C8_m%s_k%s_t%s'%(m, k, t) )
                    
                for j in self.x[m][k].keys():
                    #----- constraint (9)
                    #self.model.addLConstr(quicksum(self.x[m2][k2][j] for m2 in self.x.keys() if m2 != m and j in inst.p_mj[m2].keys() for k2 in self.x[m2].keys()) + quicksum(self.x[m][k2][j] for k2 in self.x[m].keys() if k2 <= k) >= quicksum(self.v[m][k][t] for t in inst.T_j[j]) - len(inst.T_j[j]) + 1,'C9_m%s_k%s_j%s'%(m,k,j) )
                    #----- constraint (9bis)
                    self.model.addLConstr( len(inst.T_j[j]) - quicksum(self.v[m][k][t] for t in inst.T_j[j]) >=  quicksum(self.x[m][k2][j] for k2 in self.x[m].keys() if k2 > k) ,'C9bis_m%s_k%s_j%s'%(m,k,j) )

                    #----- constraint (10)
                    self.model.addLConstr(self.z[m][k] >= self.x[m][k][j],'C10_m%s_k%s_j%s'%(m,k,j))

                #----- constraint (11)
                self.model.addLConstr(self.z[m][k] <=  quicksum(self.u[m][k][t] for t in self.u[m][k].keys()),'C11_m%s_k%s'%(m,k))

                if k+1 in self.z[m].keys():
                    #----- constraint (12)
                    self.model.addLConstr( self.z[m][k] >= self.z[m][k+1],'C12_m%s_k%s'%(m, k) )
                
                for G in self.Q_m[m]:
                    #----- constraint (13)
                    self.model.addLConstr( self.z[m][k] >= quicksum(self.x[m][k][j] for j in G),'C13_m%s_k%s'%(m, k) )
            
            #----- constraint (15) - define Fmax
            self.model.addLConstr(self.Cmax >= quicksum(inst.sw_m[m] * self.u[m][k][t] for k in self.u[m].keys() if k > 0 for t in self.u[m][k].keys()) + quicksum( inst.p_mj[m][j]*self.x[m][k][j] for k in self.x[m].keys() for j in self.x[m][k].keys()) ,'C14_m%s'%m )
            
            #----- breaking symetry constraint () -
            self.model.addLConstr( quicksum(j * self.x[m][k][j] for k in self.x[m].keys() if k <= max(self.x[m].keys())/2 for j in self.x[m][k].keys()) >= quicksum(j * self.x[m][k][j] for k in self.x[m].keys() if k > max(self.x[m].keys())/2 for j in self.x[m][k].keys()) ,'symetry_m%s'%m )
            
            #----- group size cut constraint () -
            for j in self.x[m][k].keys():
                self.model.addLConstr( quicksum(self.x[m][k][j2] for j2 in self.x[m][k].keys()) <= self.Delta_mj[m][j]*self.x[m][k][j] +  max(self.Delta_mj[m].values())*(1-self.x[m][k][j]) ,'cut_m%s_k%s_j%s'%(m,k,j) )
        #-----
        
        #----- objective_function
        self.model.setObjective( self.Cmax, sense = GRB.MINIMIZE)
        #self.model.write(modelName +'.lp' )
        self.model.update()
        if self.callBack == True:
            #DEFINE CALLBACK OBJECT
            self.cbData = CallbackData(inst, self.K_m, self.treshold_UB, self.model.getVars())
    
    
    def warmStart(self, blocks_mj, seq_m):
        '''
        for m in self.var_mk.keys():
            firstHalf = [seq_m[m][k] for k in range(math.floor(min(len(seq_m[m]),self.K_m[m]/2)))]
            if sum(firstHalf) < sum([j for j in seq_m[m] if j not in firstHalf]):
                seq_m[m].reverse()
            for k in self.var_mk[m].keys():
                for var in self.var_mk[m][k]:
                    var.start = 0.0
                    
        for m, seq in seq_m.items():
            kz = 0
            for kj in range(len(seq)):
                j = seq[kj]
                self.z[m][kz].start = 1.0
                self.x[m][kz][j].start = 1.0
                newGroup = False
                for t in blocks_mj[m][j]:
                    if kj > 0 and t not in blocks_mj[m][seq[kj-1]]:
                        self.u[m][kz][t].start = 1.0
                        newGroup = True
                if kz > 0 and newGroup:
                    for t in blocks_mj[m][seq[kj-1]]:
                        if t not in blocks_mj[m][seq[kj]]:
                            self.w[m][kz][t].start = 1.0
                if newGroup and kj > 0:
                    kz += 1
                self.v[m][kz][t].start = 1.0
        '''
        return True
    
    #-----------------------
    #-----------------------
    def plotSolution(self, inst, name = ""):
        if self.model.status in [2,9]:
            f, ax = plt.subplots(figsize = (10, 10) )
            ax.set_xlim([0,self.Cmax.x])
            ax.set_ylim([0,sum([c+2 for c in inst.C_m.values()])])

            y = 0
            for m in inst.M:
                x = 0
                lastb = 0
                for k in self.x[m].keys():
                    for t in self.w[m][k].keys():
                        if self.w[m][k][t].x > 0:
                            #print("W[m%s][k%s][t%s] = %s"%(m,k,t,self.w[m][k][t].x))
                            length = inst.sw_m[m]
                            ax.add_patch(Rectangle((x,y), length, 1, fc="red", ec="black", alpha=0.3))
                            plt.text(x+length/3,y + 0.33,"t"+str(t))
                            x += length
                    for j in self.x[m][k].keys():
                        if self.x[m][k][j].x > 0:
                            #print("X[m%s][k%s][b%s]"%(m,k,j))
                            length = inst.p_mj[m][j]
                            ax.add_patch(Rectangle((x,y), length, 1, fc="green", ec="black", alpha=0.3))
                            plt.text(x+length/3,y + 0.33,"j"+str(j))
                            
                            yt = y+1
                            for t in self.u[m][k].keys():
                                if self.u[m][k][t].x > 0:
                                    ax.add_patch(Rectangle((x,yt), length, 1, fc="yellow", ec="black", alpha=0.3))
                                    plt.text(x+length/3,yt + 0.33,"t"+str(t))
                                    yt += 1
                            x += length
                y += inst.C_m[m] + 2
                                
            f.savefig(name + "sol.png")
                    

from instance import *
from MILP.MILP import *

################################################################################
#####                      MachineThreshold_MILP()                      #####
################################################################################

class MachineThreshold_MILP(MILP):
    def __init__(self, instance, m, minNbJobs, modelName = "ThresholdMILP"):
        super().__init__(modelName + "_" + str(m))
      
        self.alpha = {}                 # alpha_{j} = 1 if job j is selected
        self.beta = {}                  # beta_{t} = 1 if tool t is loaded on machine m
        self.Gamma = self.model.addVar(obj = 0, lb = 0, vtype = GRB.INTEGER, name = 'Gamma')
        self.Cmax = self.model.addVar(obj = 1, vtype = GRB.INTEGER, name = 'Cmax')         # makespan
        for j in instance.p_mj[m].keys():
            self.alpha[j] = self.model.addVar(obj = 1, vtype = GRB.BINARY, name = 'alpha[%s]'%j)
            for t in instance.T_j[j]:
                if t not in self.beta.keys():
                    self.beta[t] = self.model.addVar(obj = 0, vtype = GRB.BINARY, name = 'beta[%s]'%t)
                    
        #Tool using constraints
        for t in self.beta.keys():
            self.model.addLConstr(len(instance.J_t[t]) * self.beta[t] >= quicksum(self.alpha[j] for j in instance.J_t[t] if j in self.alpha.keys() ),'tool_t%s_requirement'%t)
        #UB constraint - Cmax
        self.model.addLConstr( self.Gamma * instance.sw_m[m] + quicksum(instance.p_mj[m][j] * self.alpha[j] for j in self.alpha.keys()) <= self.Cmax ,'Cmax_def')
        #min nb jobs
        self.model.addLConstr( quicksum(self.alpha[j] for j in self.alpha.keys()) >= minNbJobs ,'minNbJobs')
        #gamma - nb of swithces
        self.model.addLConstr( (quicksum(self.beta[t] for t in self.beta.keys() ) - instance.C_m[m]) <= self.Gamma ,'gamma_switches')

        self.model.setObjective(self.Cmax, sense = GRB.MINIMIZE )

from COR_2024.ImprovedMILP import *

################################################################################
#####                               Class daSilva_MILP()                       #####
################################################################################
class daSilva_MILP(ImprovedMILP):
    #-----------------------
    #----------------------- Constructor
    def __init__(self, inst, modelName = "daSilva_MILP"):
        super().__init__(modelName)
        ##### Attributes

        #----- dict of nb. positions k per m
        self.K_m = {}
        for m in inst.M:
            KP = MachineKP_MILP(inst, m)
            self.K_m[m] = int(KP.solve(outputFlag = False))
            print("K_%s = %s"%(m, self.K_m[m]))

        #check the tools needed par machine
        self.T_m = {}
        for m in inst.M:
            self.T_m[m] = [False for t in inst.T]    # list of tools
            for j in inst.p_mj[m].keys():
                for t in inst.T_j[j]:
                    self.T_m[m][t] = True
            #print("Machine ", m, ": ", self.T_m[m])

        #-----
        self.X = {}     # X_{m,k,j} j in process at position k of machine m
        self.Y = {}     # Y_{m,k,k',t} if t loaded from k to k' of machine m
        #-----
        #----- generate_variables for model
        # X variables
        for m in inst.M: #each machine
            self.X[m] = {}
            for k in range(1,self.K_m[m]+1): #each position
                self.X[m][k] = {}
                for j in inst.p_mj[m].keys():
                    self.X[m][k][j] = self.model.addVar(vtype = GRB.BINARY, name='X[%s][%s][%s]'%(m, k, j) )
                self.X[m][k][len(inst.J)] = self.model.addVar(vtype = GRB.BINARY, name='X[%s][%s][%s]'%(m, k, len(inst.J)) ) #dummy job
        # Y variables
        for m in inst.M: #each machine
            #print("Machine: ", m)
            #create structures
            self.Y[m] = {}
            for k in range(self.K_m[m]+3): #each position
                self.Y[m][k] = {}
                for kk in range(self.K_m[m]+3): #each position
                    self.Y[m][k][kk] = {}
            # Set PA_m
            for k in range(self.K_m[m]+1): #each position
                for t in inst.T:
                    if self.T_m[m][t] == True:
                        self.Y[m][k][k+1][t] = self.model.addVar(vtype = GRB.BINARY, name = 'Y[%s][%s][%s][%s]'%(m, k, k+1, t) )
                        #print('Y[%s][%s][%s][%s]'%(m, k, k+1, t))
            # Set PB_m
            for k in range(1,self.K_m[m]): #each position
                for t in inst.T:
                    if self.T_m[m][t] == True:
                        self.Y[m][k][self.K_m[m]+1][t] = self.model.addVar(vtype = GRB.BINARY, name = 'Y[%s][%s][%s][%s]'%(m, k, self.K_m[m]+1, t) )
                        #print('Y[%s][%s][%s][%s]'%(m, k, self.K_m[m]+1, t))
            # Set PC_m
            for k in range(max(self.K_m[m]-1,0+1)): #each position - from 0 to N+2 must exists, always!!
                for t in inst.T:
                    if self.T_m[m][t] == True:
                        self.Y[m][k][self.K_m[m]+2][t] = self.model.addVar(vtype = GRB.BINARY, name = 'Y[%s][%s][%s][%s]'%(m, k, self.K_m[m]+2, t) )
                        #print('Y[%s][%s][%s][%s]'%(m, k, self.K_m[m]+2, t))
            # Set PD_m
            for k in range(1,self.K_m[m]+1): #each position
                for t in inst.T:
                    if self.T_m[m][t] == True:
                        self.Y[m][self.K_m[m]+2][k][t] = self.model.addVar(vtype = GRB.BINARY, name = 'Y[%s][%s][%s][%s]'%(m, self.K_m[m]+2, k, t) )
                        #print('Y[%s][%s][%s][%s]'%(m, self.K_m[m]+2, k, t))
            '''
            # Set PA_m
            for k in range(self.K_m[m]+1): #each position
               for j in inst.p_mj[m].keys():
                   for t in inst.T_j[j]:
                       if t not in self.Y[m][k][k+1].keys():
                           self.Y[m][k][k+1][t] = self.model.addVar(vtype = GRB.BINARY, name = 'Y[%s][%s][%s][%s]'%(m, k, k+1, t) )
                           #print('Y[%s][%s][%s][%s]'%(m, k, k+1, t))
            # Set PB_m
            for k in range(1,self.K_m[m]): #each position
               for j in inst.p_mj[m].keys():
                   for t in inst.T_j[j]:
                       if t not in self.Y[m][k][self.K_m[m]+1].keys():
                           self.Y[m][k][self.K_m[m]+1][t] = self.model.addVar(vtype = GRB.BINARY, name = 'Y[%s][%s][%s][%s]'%(m, k, self.K_m[m]+1, t) )
                           #print('Y[%s][%s][%s][%s]'%(m, k, self.K_m[m]+1, t))
            # Set PC_m
            for k in range(self.K_m[m]-1): #each position
                for j in inst.p_mj[m].keys():
                    for t in inst.T_j[j]:
                        if t not in self.Y[m][k][self.K_m[m]+2].keys():
                            self.Y[m][k][self.K_m[m]+2][t] = self.model.addVar(vtype = GRB.BINARY, name = 'Y[%s][%s][%s][%s]'%(m, k, self.K_m[m]+2, t) )
                            #print('Y[%s][%s][%s][%s]'%(m, k, self.K_m[m]+2, t))
            # Set PD_m
            for k in range(1,self.K_m[m]+1): #each position
                for j in inst.p_mj[m].keys():
                    for t in inst.T_j[j]:
                        if t not in self.Y[m][self.K_m[m]+2][k].keys():
                            self.Y[m][self.K_m[m]+2][k][t] = self.model.addVar(vtype = GRB.BINARY, name = 'Y[%s][%s][%s][%s]'%(m, self.K_m[m]+2, k, t) )
                            #print('Y[%s][%s][%s][%s]'%(m, self.K_m[m]+2, k, t))
            '''

        #Makespan variable
        self.Cmax = self.model.addVar(lb=inst.FmaxLB, ub=inst.FmaxUB, vtype = GRB.INTEGER, name = 'Cmax')         # makespan

        #----- generate_constraints for model - adapted from da Silva et al (20xx)
        #job processing constraints
        for j in inst.J:
            self.model.addLConstr( quicksum(self.X[m][k][j] for m in inst.M if j in inst.p_mj[m].keys() for k in range(1, self.K_m[m]+1) ) == 1,'job_demand_j%s'%j)
        #position occupation constraints
        for m in inst.M:
            for k in range(1,self.K_m[m]+1):
                self.model.addLConstr( quicksum(self.X[m][k][j] for j in inst.J if j in inst.p_mj[m].keys() ) + self.X[m][k][len(inst.J)] == 1,'position_ocupation_m%s_k%s'%(m, k))
        #flow constraints
        for m in inst.M:
            for t in inst.T:
                if self.T_m[m][t] == True:
                    #print("m: ", m, " t: ", t)
                    #position 0
                    self.model.addLConstr( self.Y[m][0][1][t] + self.Y[m][0][self.K_m[m]+2][t] == 1,'flow_p0_m%s_t%s'%(m, t))
                    #positions 1 to nb_pos-2
                    for k in range(1,self.K_m[m]-1): 
                        self.model.addLConstr( self.Y[m][k-1][k][t] + self.Y[m][self.K_m[m]+2][k][t] - self.Y[m][k][self.K_m[m]+1][t] - self.Y[m][k][self.K_m[m]+2][t] - self.Y[m][k][k+1][t] == 0,'flow_p%s_m%s_t%s'%(k, m, t))
                    #position nb_pos-1
                    if(self.K_m[m] > 1):
                        self.model.addLConstr( self.Y[m][self.K_m[m]-2][self.K_m[m]-1][t] + self.Y[m][self.K_m[m]+2][self.K_m[m]-1][t] - self.Y[m][self.K_m[m]-1][self.K_m[m]][t] - self.Y[m][self.K_m[m]-1][self.K_m[m]+1][t] == 0,'flow_p%s_m%s_t%s'%(self.K_m[m]-1,m, t))
                    #position nb_pos
                    self.model.addLConstr( self.Y[m][self.K_m[m]-1][self.K_m[m]][t] - self.Y[m][self.K_m[m]][self.K_m[m]+1][t] + self.Y[m][self.K_m[m]+2][self.K_m[m]][t] == 0,'flow_p%s_m%s_t%s'%(self.K_m[m], m, t))
                    #position nb_pos+1
                    self.model.addLConstr( quicksum(self.Y[m][k][self.K_m[m]+1][t] for k in range(1,self.K_m[m]+1) ) == 1,'flow_p%s_m%s_t%s'%(self.K_m[m]+1, m, t))
                    #position nb_pos+2
                    self.model.addLConstr( quicksum(self.Y[m][k][self.K_m[m]+2][t] for k in range(0,max(self.K_m[m]-1, 0+1)) ) - quicksum(self.Y[m][self.K_m[m]+2][k][t] for k in range(1,self.K_m[m]+1) ) == 0,'flow_p%s_m%s_t%s'%(self.K_m[m]+2, m, t))
        #tool requirements
        for m in inst.M:
            for j in inst.p_mj[m].keys():
                for t in inst.T_j[j]:
                    for k in range(1,self.K_m[m]+1):
                        self.model.addLConstr( self.X[m][k][j] - self.Y[m][k-1][k][t] <= 0,'job_tool_requirement_m%s_j%s_t%s_k%s'%(m,j,t,k) )
        #machine magazine constraints
        for m in inst.M:
            for k in range(1,self.K_m[m]+1):
                self.model.addLConstr( quicksum(self.Y[m][k-1][k][t] for t in inst.T if self.T_m[m][t] == True) <= inst.C_m[m] ,'machine_capacities_m%s_k%s'%(m,k) )
        #makespan definition
        for m in inst.M:
            self.model.addLConstr( quicksum(inst.p_mj[m][j]*self.X[m][k][j] for j in inst.J if j in inst.p_mj[m].keys() for k in range(1,self.K_m[m]+1) ) + quicksum(inst.sw_m[m]*self.Y[m][k][self.K_m[m]+1][t] for t in inst.T if self.T_m[m][t] == True for k in range(1,self.K_m[m]) ) + quicksum(inst.sw_m[m]*self.Y[m][k][self.K_m[m]+2][t] for t in inst.T if self.T_m[m][t] == True for k in range(1,self.K_m[m]-1) ) <= self.Cmax ,'makespan_m%s'%(m) )
            #self.model.addLConstr( quicksum(inst.p_mj[m][j]*self.X[m][k][j] for j in inst.J for k in range(1,self.K_m[m]+1) ) + quicksum(inst.sw_m[m]*self.Y[m][k][self.K_m[m]+1][t] for t in inst.T if self.T_m[m][t] == True for k in range(1,self.K_m[m]) ) + quicksum(inst.sw_m[m]*self.Y[m][k][self.K_m[m]+2][t] for t in inst.T if self.T_m[m][t] == True for k in range(1,self.K_m[m]-1) ) <= self.Cmax ,'makespan_m%s'%(m) )
                

        #---- valid inequalities
        #if a tool is load then it should be used
        for m in inst.M:
            for t in inst.T:
                if self.T_m[m][t] == True:
                    for k in range(1,self.K_m[m]):
                        self.model.addLConstr( self.Y[m][self.K_m[m]+2][k][t] + self.Y[m][k][self.K_m[m]+1][t] <= 1,'VI1_m%s_t%s_k%s'%(m, t, k) )
        #defining the number of tools left out (not loaded) at the begining
        for m in inst.M:
            #self.model.addLConstr( quicksum(self.Y[m][0][self.K_m[m]+2][t] for t in inst.T if self.T_m[m][t] == True) == len(inst.T) - inst.C_m[m],'VI2_m%s'%(m) )
            self.model.addLConstr( quicksum(self.Y[m][0][self.K_m[m]+2][t] for t in inst.T if self.T_m[m][t] == True) == quicksum(1 for t in inst.T if self.T_m[m][t] == True) - inst.C_m[m],'VI2_m%s'%(m) )
        #defining the number of tools loaded at the begining
        for m in inst.M:
            self.model.addLConstr( quicksum(self.Y[m][0][1][t] for t in inst.T if self.T_m[m][t] == True) == inst.C_m[m],'VI3_m%s'%(m) )
        #-----
        #----- objective_function
        self.model.setObjective( self.Cmax, sense = GRB.MINIMIZE)
        #self.model.write(modelName +'.lp' )
   
    
    def printSolution(self, inst):
        print("Cmax: %s"%self.Cmax.x)
        # X variables
        for m in self.X.keys(): #each machine
            self.X[m] = {}
            for k in self.X[m].keys(): #each position
                self.X[m][k] = {}
                for j in self.X[m][k].keys():
                    if self.X[m][k][j].x > 0.5:
                        print('X[%s][%s][%s]'%(m, k, j))
                if self.X[m][k][len(inst.J)].x > 0.5:
                        print('X[%s][%s][%s]'%(m, k, len(inst.J))) #dummy job
                        
        for m in self.Y.keys(): #each machine
            for k in self.Y[m].keys(): #each position
                for kk in self.Y[m][k].keys(): #each position
                    for t in self.Y[m][k][kk].keys():
                        if self.Y[m][k][kk][t].x > 0.5:
                            print('Y[%s][%s][%s][%s]'%(m, k, kk, t))
    
    '''
    #-----------------------
    #-----------------------
    def plotSolution(self, inst, name = ""):
        if self.model.status in [2,9]:
            f, ax = plt.subplots(figsize = (10, 10) )
            ax.set_xlim([0,self.Fmax.x])
            ax.set_ylim([0,sum([c+2 for c in inst.C_m.values()])])

            y = 0
            for m in inst.M:
                x = 0
                lastb = 0
                for k in self.X[m].keys():
                    for t in self.W[m][k].keys():
                        if self.W[m][k][t].x > 0:
                            #print("W[m%s][k%s][t%s] = %s"%(m,k,t,self.W[m][k][t].x))
                            length = inst.sw_m[m]
                            ax.add_patch(Rectangle((x,y), length, 1, fc="red", ec="black", alpha=0.3))
                            plt.text(x+length/3,y + 0.33,"t"+str(t))
                            x += length
                    for j in self.X[m][k].keys():
                        if self.X[m][k][j].x > 0:
                            #print("X[m%s][k%s][b%s]"%(m,k,j))
                            length = inst.p_mj[m][j]
                            ax.add_patch(Rectangle((x,y), length, 1, fc="green", ec="black", alpha=0.3))
                            plt.text(x+length/3,y + 0.33,"j"+str(j))
                            
                            yt = y+1
                            for t in self.V[m][k].keys():
                                if self.V[m][k][t].x > 0:
                                    ax.add_patch(Rectangle((x,yt), length, 1, fc="yellow", ec="black", alpha=0.3))
                                    plt.text(x+length/3,yt + 0.33,"t"+str(t))
                                    yt += 1
                            x += length
                y += inst.C_m[m] + 2
                                
            f.savefig(name + "sol.png")
    '''

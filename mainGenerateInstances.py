###################### divers import
import os
import time
import pandas as pd
from printColors import *

###################### Instance
from instance import *

###################### Instance directories

######### Generated
InstSet3 = "COR_2024"

###################### Generate new instances
dir = os.path.join("instances",InstSet3)
if not os.path.exists(dir):
    os.makedirs(dir)
instNum = 1
for nbM in range(2,4):
    for nbJ in [10, 15, 20]:
        for nbT in [10, 15, 20]:
            subDir = os.path.join(dir,"0%s-%s-%s"%(nbM, nbJ, nbT))
            if not os.path.exists(subDir):
                os.makedirs(subDir)
            for rd in [0.2, 0.3, 0.4, 0.5]:
                for instVar in range(10):
                    inst = Instance()
                    inst.generates(nbT, nbJ, nbM, rd, mode = "matrix")
                    inst.write(os.path.join(subDir, "ins%s_m=%s_j=%s_t=%s_dens=%s_var=%s.csv"%(instNum, nbM, nbJ, nbT, int(10*rd), instVar)))

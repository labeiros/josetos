from gurobipy import *
from instance import *


################################################################################
#####                               Class Node()                           #####
################################################################################
class Node:

    def __init__(self, nbM = 0, J = []):
        self.assignment_m = {}
        for m in range(nbM):
            self.assignment_m[m] = []
        self.unassignedJ = [j for j in J]
        self.LB = 0


    def copy(self, other):
        for m, A in other.assignment_m.items():
            self.assignment_m[m] = [j for j in A]
        self.unassignedJ = [j for j in other.unassignedJ]
            
    def offsprings(self, instance):
        children = []
        j = min(self.unassignedJ)
        for m in self.assignment_m.keys():
            if j in instance.pmj[m].keys():
                child = Node()
                child.copy(self)
                child.unassignedJ = [j2 for j2 in self.unassignedJ if j2 > j]
                child.assignment_m[m].append(j)
                children.append(child)
        return children
        
    def computeLB(self, inst):
        LBm = {}
        Tm = {}
        for m, A in self.assignment_m.items():
            LBm[m] = 0
            T = set()
            for j in A:
                for t in inst.Tj[j]:
                    T.add(t)
                LBm[m] += inst.pmj[m][j]
            print("LBm[%s] = %s"%(m, LBm[m]))
            Tm[m] = T
            
        model = Model("NodeLb")      # model
        alpha = {}                 # alpha_{mj} = 1 if job j is selected
        beta = {}                  # beta_{mt} = 1 if tool t is loaded on machine m
        Fmax = model.addVar(obj=0, vtype=GRB.CONTINUOUS, name='Fmax')
        
        for m in inst.M:
            alpha[m] = {}
            beta[m] = {}
            for j in inst.pmj[m].keys():
                if j in self.unassignedJ:
                    alpha[m][j] = model.addVar(obj=0, vtype=GRB.BINARY, name='alpha[%s]'%j)
                    for t in inst.Tj[j]:
                        if t not in beta.keys():
                            beta[m][t] = model.addVar(obj=0, vtype=GRB.BINARY, name='beta[%s]'%t)
            for t in Tm[m]:
                if t not in beta.keys():
                    beta[m][t] = model.addVar(lb = 1, obj = 0, vtype = GRB.BINARY, name='beta[%s]'%t)
                #model.addConstr( beta[m][t] >= 1,'tool_t%s_is_on_machine_m%s'%(t,m) )

            for t in beta[m].keys():
                model.addConstr( len(inst.Jt[t])*beta[m][t] >= quicksum(alpha[m][j] for j in inst.Jt[t] if j in alpha[m].keys()),'tool_t%s_requirement_on_machine_m%s'%(t,m) )
                
            model.addConstr( (quicksum(beta[m][t] for t in beta[m].keys()) - inst.Cm[m])*inst.swm[m] + quicksum(inst.pmj[m][j]*alpha[m][j] for j in alpha[m].keys()) + LBm[m] <= Fmax ,'makespan')
        
        for j in self.unassignedJ:
            model.addConstr( quicksum(alpha[m][j] for m in alpha.keys() if j in alpha[m].keys()) == 1,'do_job_j%s'%j )
        
        model.setObjective( Fmax, sense=GRB.MINIMIZE )

        model.Params.OutputFlag = False
        model.params.timeLimit = 60
        model.optimize()
        
        self.LB = model.objVal
        
        return self.LB
        
    def printNode(self):
        print("Node LB:", self.LB)
        for m, A in self.assignment_m.items():
            print("Machine %s:"%m, A)

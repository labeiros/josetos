/* Copyright 2016, Gurobi Optimization, Inc. */

/*
   This example reads a model from a file, sets up a callback that
   monitors optimization progress and implements a custom
   termination strategy, and outputs progress information to the
   screen and to a log file.

   The termination strategy implemented in this callback stops the
   optimization of a MIP model once at least one of the following two
   conditions have been satisfied:
     1) The optimality gap is less than 10%
     2) At least 10000 nodes have been explored, and an integer feasible
        solution has been found.
   Note that termination is normally handled through Gurobi parameters
   (MIPGap, NodeLimit, etc.).  You should only use a callback for
   termination if the available parameters don't capture your desired
   termination criterion.
*/

#include "gurobi_c++.h"
#include <fstream>

using namespace std;

class myGRBcallback: public GRBCallback
{
	public:
		Data *data;
		double lastiter;
		double lastnode;
		int numvars;
		GRBVar* vars;
		ofstream* logfile;
		myGRBcallback(Data *data, int xnumvars, GRBVar* xvars, ofstream* xlogfile) {
			this->data = data;
			lastiter = lastnode = -GRB_INFINITY;
			numvars = xnumvars;
			vars = xvars;
			logfile = xlogfile;
		}
	protected:
		void callback () {
			try{
				if (where == GRB_CB_POLLING) {
				  // Ignore polling callback
				} else if (where == GRB_CB_PRESOLVE) {

				} else if (where == GRB_CB_SIMPLEX) {
				  // Simplex callback

				} else if (where == GRB_CB_MIP) {
				  // General MIP callback

					//double nodecnt = getDoubleInfo(GRB_CB_MIP_NODCNT);
					//double objbst = getDoubleInfo(GRB_CB_MIP_OBJBST);
					//double objbnd = getDoubleInfo(GRB_CB_MIP_OBJBND);
					//double cutsaplied = getIntInfo(GRB_CB_MIP_CUTCNT);
					//cout << "Node: " << nodecnt << " iteration: " << data->iteration << " bound: " << objbnd << endl;getchar();
					//if(nodecnt == 0){
					//	data->rootObj = objbnd;
					//	cout << "Node: " << nodecnt << " Root bound: " << objbnd << endl; getchar();
					//}

				} else if (where == GRB_CB_MIPSOL) {
				  //// MIP solution callback
		    //      int nodecnt = (int) getDoubleInfo(GRB_CB_MIPSOL_NODCNT);
		    //      double obj = getDoubleInfo(GRB_CB_MIPSOL_OBJ);
		    //      int solcnt = getIntInfo(GRB_CB_MIPSOL_SOLCNT);
		    //      double* x = getSolution(vars, numvars);
		    //      cout << "**** New solution at node " << nodecnt
		    //           << ", obj " << obj << ", sol " << solcnt
		    //           << ", x[0] = " << x[0] << " ****" << endl;
		    //      delete[] x;
				} else if (where == GRB_CB_MIPNODE) {

					double nodecnt = getDoubleInfo(GRB_CB_MIPNODE_NODCNT);
					double obj = getDoubleInfo(GRB_CB_MIPNODE_OBJBND);			//Current best objective bound - relaxed
					//double objbst = getDoubleInfo(GRB_CB_MIPNODE_OBJBST);		//Current best objective - feasible
					//int solcnt = getIntInfo(GRB_CB_MIPNODE_SOLCNT);				//Current count of feasible solutions found
					//cout << "___Node: " << nodecnt << " iteration: " << data->iteration << endl; getchar();
					if(nodecnt == 0 && data->iteration == 0){
						data->rootRelObj = obj;
					}
					data->iteration++; 	


				} else if (where == GRB_CB_BARRIER) {

				} else if (where == GRB_CB_MESSAGE) {
				  // Message callback
				  string msg = getStringInfo(GRB_CB_MSG_STRING);
				  *logfile << msg;
				}
			} catch (GRBException e) {
			cout << "Error number: " << e.getErrorCode() << endl;
			cout << e.getMessage() << endl;
			} catch (...) {
			cout << "Error during callback" << endl;
			}
		}
};

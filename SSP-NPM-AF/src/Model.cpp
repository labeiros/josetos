//---------------------------------------------------------------------------

#include "Model.h"
//#include "MyGRBcallback.h"
#include <string>

Model::Model(Data *data, Parameters *parameters, Solution *solution) {	//construtor
	this->data = data;
	this->parameters = parameters;
	this->solution = solution;
	nbRows = -1;
	nbColumns = -1;
	grb_solStatus = -1;
	varsAdded = -1;
	iter = -1;
	nbNodes = -1.0;
	modelLB = -1.0;
	modelLB_lp = -1.0;
	modelUB = -1.0;
	modelGap = -1.0;
	modelExec_time = -1.0;
	nbUnexpNodes = -1.0;
		
}

Model::~Model() { // Destrutor

}

void Model::createArcFlowModel_grb_2() {	//AF formulation - Gurobi

	try {

		cout << "Creating AF model with gurobi..." << endl;

		// Model
		GRBEnv* env = new GRBEnv();
		GRBModel model = GRBModel(*env);
		model.set(GRB_StringAttr_ModelName, "AF_SSP-NPM_grb");
		int variables = 0;
		int constraints = 0;


		GRBVar Z;
		Z = model.addVar(0, data->H, 1, GRB_CONTINUOUS, "Z"); //(lb, ub, obj, type, name)

		// x variables -- arc variables
		//JOB PROCESSING ARCS
		GRBVar**** x;	//j p q k
		x = new GRBVar *** [data->N];
		for (int i = 0; i < data->N; i++) {
			x[i] = new GRBVar ** [data->H];
			for (int p = 0; p < data->H; p++) {
				x[i][p] = new GRBVar * [data->H];
				for (int q = 0; q < data->H; q++) {
					x[i][p][q] = new GRBVar[data->M];
					for (int k = 0; k < data->M; k++) {
						if (data->jobNP[i][p][q][k]) {
							string vname;
							vname = "x(" + to_string(i) + "_" + to_string(p) + "_" + to_string(q) + "_" + to_string(k) + ")";
							double cost_coef = 0;
							x[i][p][q][k] = model.addVar(0, 1, cost_coef, GRB_BINARY, vname); //(lb, ub, obj, type, name)
							//set var initial value to 0 if there is an initial UB
							variables++;
						}
					}
				}
			}
		}
		//SETUP/SWITCHING PROCESSING ARCS
		GRBVar*** s;	//p q k
		s = new GRBVar ** [data->H];
		for (int p = 0; p < data->H; p++) {
			s[p] = new GRBVar * [data->H];
			for (int q = 0; q < data->H; q++) {
				s[p][q] = new GRBVar[data->M];
				for (int k = 0; k < data->M; k++) {
					if (data->setupNP[p][q][k]) {
						string vname;
						vname = "s(" + to_string(p) + "_" + to_string(q) + "_" + to_string(k) + ")";
						double cost_coef = 0;
						s[p][q][k] = model.addVar(0, 1, cost_coef, GRB_BINARY, vname); //(lb, ub, obj, type, name)
						//set var initial value to 0 if there is an initial UB
						variables++;
					}
				}
			}
		}
		//LOSS PROCESSING ARCS
		GRBVar** l;
		l = new GRBVar *[data->H];
		for (int p = 0; p < data->H; p++) {
			l[p] = new GRBVar[data->M];
			for (int k = 0; k < data->M; k++) {
				if (data->lossNP[p][k]) {
					string vname;
					vname = "l(" + to_string(p) + "_" + to_string(k) + ")";
					double cost_coef = 0;
					l[p][k] = model.addVar(0, 1, cost_coef, GRB_BINARY, vname); //(lb, ub, obj, type, name)
					//set var initial value to 0 if there is an initial UB
					variables++;
				}
			}
		}

		// y variables -- arc variables
		//TOOLS PROCESSING ARCS
		GRBVar**** y;	//t p q k
		y = new GRBVar ***[data->T];
		for (int t = 0; t < data->T; t++) {
			y[t] = new GRBVar **[data->H];
			for (int p = 0; p < data->H; p++) {
				y[t][p] = new GRBVar * [data->H];
				for (int q = 0; q < data->H; q++) {
					y[t][p][q] = new GRBVar[data->M];
					for (int k = 0; k < data->M; k++) {
						if (data->toolNP[t][p][q][k]) {
							string vname;
							vname = "y(" + to_string(t) + "_" + to_string(p) + "_" + to_string(q) + "_" + to_string(k) + ")";
							double cost_coef = 0;
							y[t][p][q][k] = model.addVar(0, 1, cost_coef, GRB_BINARY, vname); //(lb, ub, obj, type, name)
							//set var initial value to 0 if there is an initial UB
							variables++;
						}
					}
				}
			}
		}

		cout << variables << " variables created!!" << endl;

		////============================================================================================================= 
		//// Creating objective function
		////=============================================================================================================
		cout << "Creating objective function..." << endl;

		// Adding the objective function to the model
		model.set(GRB_IntAttr_ModelSense, 1);

		// Update model to integrate new variables
		model.update();

		cout << "Objective function created!" << endl;

		//////============================================================================================================= 
		////	// Creating constraints
		//////=============================================================================================================
		int aux_t;

		//ALL JOBS MUST BE SCHEDULED
		cout << "Creating constraint 1..." << endl;
		for (int i = 0; i < data->N; i++) {	//number of jobs
			GRBLinExpr C1 = 0;
			for (int k = 0; k < data->M; k++) {
				for (int p = 0; p < data->H; p++) {
					for (int q = 0; q < data->H; q++) {
						if (data->jobNP[i][p][q][k])
						{
							C1 += x[i][p][q][k];
						}
					}
				}
			}
			string cname;
			cname = "C1_" + to_string(i);
			model.addConstr(C1 == 1, cname);
			constraints++;
		}
		
		//FLOW CONSERVATION ON JOBS PROCESSING
		cout << "Creating constraint 2..." << endl;
		for (int k = 0; k < data->M; k++) {
			for (int q = 0; q < data->H; q++) {
				GRBLinExpr C2 = 0;
				//ARCS LEAVING q
				//job arcs
				for (int i = 0; i < data->N; i++)
				{
					for (int r = q + 1; r < data->H; r++) {
						if (data->jobNP[i][q][r][k]) {
							C2 += x[i][q][r][k];
						}
					}
				}
				//setup arcs
				for (int r = q + 1; r < data->H; r++) {
					if (data->setupNP[q][r][k]) {
						C2 += s[q][r][k];
					}
				}
				//loss arc
				if (data->lossNP[q][k]) {
					C2 += l[q][k];
				}
				//ARCS ARRIVING IN q
				//job arcs
				for (int i = 0; i < data->N; i++)
				{
					for (int p = 0; p < q; p++) {
						if (data->jobNP[i][p][q][k]) {
							C2 -= x[i][p][q][k];
						}
					}
				}
				for (int p = 0; p < q; p++) {
					if (data->setupNP[p][q][k]) {
						C2 -= s[p][q][k];
					}
				}
				//CREATE CONSTRAINTS
				//RHS + loss arcs rhs
				if (q == 0) {
					C2 += -(1);
				}
				else {
					if (q == data->H - 1) {
						C2 += 1;
						for (int p = 0; p < data->H; p++) {
							if (data->lossNP[p][k]) {
								C2 += -(l[p][k]);
							}
						}
					}
				}
				string cname;
				cname = "C2_" + to_string(k) + "_" + to_string(q);
				model.addConstr(C2 == 0, cname);
				constraints++;
			}
		}
		
		//FLOW CONSERVATION CONSTRAINT ON TOOLS "PROCESSING"
		cout << "Creating constraint 3..." << endl;
		for (int k = 0; k < data->M; k++) {
			for (int q = 0; q < data->H; q++) {
				GRBLinExpr C3 = 0;
				//ARCS LEAVING q
				//tool arcs
				for (int t = 0; t < data->T; t++)
				{
					for (int r = q + 1; r < data->H; r++) {
						if (data->toolNP[t][q][r][k]) {
							C3 += y[t][q][r][k];
						}
					}
				}
				//setup arcs
				for (int r = q + 1; r < data->H; r++) {
					if (data->setupNP[q][r][k]) {
						C3 += data->setupCoefs[q][r][k] * s[q][r][k];
					}
				}
				////loss arc ==================================================================================== REMOVE OR NOT!!!
				//if (data->lossNP[q][k]) {
				//	C3 += data->C_m[k] * l[q][k];
				//}
				//ARCS ARRIVING IN q
				//tools arcs
				for (int t = 0; t < data->T; t++)
				{
					for (int p = 0; p < q; p++) {
						if (data->toolNP[t][p][q][k]) {
							C3 -= y[t][p][q][k];
						}
					}
				}
				for (int p = 0; p < q; p++) {
					if (data->setupNP[p][q][k]) {
						C3 -= data->setupCoefs[p][q][k] * s[p][q][k];
					}
				}
				//CREATE CONSTRAINTS
				//RHS + loss arcs rhs
				if (q == 0) {
					C3 += -(data->C_m[k]);
				}
				else {
					if (q == data->H - 1) {
						C3 += (data->C_m[k]);
						////loss arc ==================================================================================== REMOVE OR NOT!!!
						//for (int p = 0; p < data->H; p++) {
						//	if (data->lossNP[p][k]) {
						//		C3 += -(data->C_m[k]*l[p][k]);
						//	}
						//}
					}
				}
				string cname;
				cname = "C3_" + to_string(k) + "_" + to_string(q);
				model.addConstr(C3 == 0, cname);
				constraints++;
			}
		}
		
		//NO MORE THEN ONE COPY OF EACH TOOLS PER MACHINE AT SAME TIME
		cout << "Creating constraint 4..." << endl;
		for (int k = 0; k < data->M; k++) {
			for (int s = 0; s < data->H; s++) {
				for (int t = 0; t < data->T; t++) {
					GRBLinExpr C4 = 0;
					bool create = false;
					for (int p = 0; p <= s; p++) {
						for (int q = s + 1; q < data->H; q++) {
							if (data->toolNP[t][p][q][k]) {
								C4 += y[t][p][q][k];
								create = true;
							}
						}
					}
					if (create) {
						string cname;
						cname = "C4_" + to_string(k) + "_" + to_string(t) + "_" + to_string(s);
						model.addConstr(C4 <= 1, cname);
						constraints++;
					}
				}
			}
		}


		/**/
		////TOOL REQUIREMENTS PER JOB - IF A JOB IS BEING PROCESSED THEN ALL NEED TOOLS SHOULD BE CHARGED
		// V1.1 OLD - usar com a anterior
		cout << "Creating constraint 5..." << endl;
		for (int k = 0; k < data->M; k++) {
			for (int i = 0; i < data->N; i++) {	//number of jobs
				for (int s = 0; s < data->H - data->p_mj[k][i]; s++) {
					GRBLinExpr C5 = 0;// data->tr_j[i].size()* x[i][s][s + data->p_mj[k][i]][k];
					int init = s - data->p_mj[k][i]+1;
					if (init < 0) { init = 0; }
					for (int p = init; p <= s; p++) {
						if (data->jobNP[i][p][p + data->p_mj[k][i]][k]) {
							C5 += data->tr_j[i].size() * x[i][p][p + data->p_mj[k][i]][k];
						}
					}
					for (int t = 0; t < data->tr_j[i].size(); t++) {
						int tool = data->tr_j[i][t];
						for (int p = 0; p <= s; p++) {
							for (int q = s+1; q < data->H ; q++) {
								if (data->toolNP[tool][p][q][k]) {
									C5 += -(y[tool][p][q][k]);
								}
							}
						}
					}
					string cname;
					cname = "C5_" + to_string(k) + "_" + to_string(i) + "_" + to_string(s);
					model.addConstr(C5 <= 0, cname);
					constraints++;
				}
			}
		}
		
		// V1.2 NEW - usar com a anterior
		//cout << "Creating constraint 5..." << endl;
		//for (int k = 0; k < data->M; k++) {
		//	for (int i = 0; i < data->N; i++) {	//number of jobs
		//		for (int s = 0; s < data->H - data->p_mj[k][i]; s++) {
		//			GRBLinExpr C5 = 0;// data->tr_j[i].size()* x[i][s][s + data->p_mj[k][i]][k];
		//			if (data->jobNP[i][s][s + data->p_mj[k][i]][k]) {
		//				C5 += data->tr_j[i].size() * x[i][s][s + data->p_mj[k][i]][k];
		//			}
		//			for (int t = 0; t < data->tr_j[i].size(); t++) {
		//				int tool = data->tr_j[i][t];
		//				for (int p = 0; p <= s; p++) {
		//					for (int q = s + 1 + data->p_mj[k][i]; q < data->H ; q++) {
		//						if (data->toolNP[tool][p][q][k]) {
		//							C5 += -(y[tool][p][q][k]);
		//						}
		//					}
		//				}
		//			}
		//			string cname;
		//			cname = "C5_v01_" + to_string(k) + "_" + to_string(i) + "_" + to_string(s);
		//			model.addConstr(C5 <= 0, cname);
		//			constraints++;
		//		}
		//	}
		//}
		 
		//TOOL REQUIREMENTS PER JOB V2.1 - OLD
		cout << "Creating constraint 5..." << endl;
		for (int k = 0; k < data->M; k++) {
			for (int i = 0; i < data->N; i++) {	//number of jobs
				for (int t = 0; t < data->tr_j[i].size(); t++) {
					int tool = data->tr_j[i][t];
					for (int s = 0; s < data->H - data->p_mj[k][i]; s++) {
						GRBLinExpr C5b = 0;// data->tr_j[i].size()* x[i][s][s + data->p_mj[k][i]][k];
						int init = s - data->p_mj[k][i] + 1;
						if (init < 0) { init = 0; }
						for (int p = init; p <= s; p++) {
							if (data->jobNP[i][p][p + data->p_mj[k][i]][k]) {
								C5b += x[i][p][p + data->p_mj[k][i]][k];
							}
						}
						for (int p = 0; p <= s; p++) {
							for (int q = s + 1; q < data->H; q++) {
								if (data->toolNP[tool][p][q][k]) {
									C5b += -(y[tool][p][q][k]);
								}
							}
						}
						string cname;
						cname = "C5b_" + to_string(k) + "_" + to_string(i) + "_" + to_string(tool) + "_" + to_string(s);
						model.addConstr(C5b <= 0, cname);
						constraints++;
					}
				}
			}
		}


		//TOOL REQUIREMENTS PER JOB V2.2 - NEW
		//cout << "Creating constraint 5..." << endl;
		//for (int k = 0; k < data->M; k++) {
		//	for (int i = 0; i < data->N; i++) {	//number of jobs
		//		for (int t = 0; t < data->tr_j[i].size(); t++) {
		//			int tool = data->tr_j[i][t];
		//			for (int s = 0; s < data->H - data->p_mj[k][i]; s++) {
		//				GRBLinExpr C5b = 0;// data->tr_j[i].size()* x[i][s][s + data->p_mj[k][i]][k];
		//				if (data->jobNP[i][s][s + data->p_mj[k][i]][k]) {
		//					C5b += x[i][s][s + data->p_mj[k][i]][k];
		//				}
		//				for (int p = 0; p <= s; p++) {
		//					for (int q = s + 1 + data->p_mj[k][i]; q < data->H; q++) {
		//						if (data->toolNP[tool][p][q][k]) {
		//							C5b += -(y[tool][p][q][k]);
		//						}
		//					}
		//				}
		//				string cname;
		//				cname = "C5b_" + to_string(k) + "_" + to_string(i) + "_" + to_string(tool) + "_" + to_string(s);
		//				model.addConstr(C5b <= 0, cname);
		//				constraints++;
		//			}
		//		}
		//	}
		//}




		////TOOL REQUIREMENTS PER JOB V3 - IF A JOB IS BEING PROCESSED THEN ALL NEED TOOLS SHOULD BE CHARGED
		//cout << "Creating constraint 5c..." << endl;
		//for (int k = 0; k < data->M; k++) {
		//	for (int i = 0; i < data->N; i++) {	//number of jobs
		//		for (int s = 0; s < data->H - data->p_mj[k][i]; s++) {
		//			GRBLinExpr C5c = 0;// data->tr_j[i].size()* x[i][s][s + data->p_mj[k][i]][k];
		//			bool create_const = false;
		//			if (data->jobNP[i][s][s + data->p_mj[k][i]][k]) {
		//				C5c += data->tr_j[i].size() * x[i][s][s + data->p_mj[k][i]][k];
		//				create_const = true;
		//			}
		//			//}
		//			for (int t = 0; t < data->tr_j[i].size(); t++) {
		//				int tool = data->tr_j[i][t];
		//				for (int p = 0; p <= s + data->p_mj[k][i]; p++) {
		//					for (int q = s + 1; q < data->H; q++) {
		//						if (data->toolNP[tool][p][q][k]) {
		//							C5c += -(y[tool][p][q][k]);
		//						}
		//					}
		//				}
		//			}
		//			if (create_const)
		//			{
		//				string cname;
		//				cname = "C5c_" + to_string(k) + "_" + to_string(i) + "_" + to_string(s);
		//				model.addConstr(C5c <= 0, cname);
		//				constraints++;
		//			}
		//		}
		//	}
		//}







		//ALTERNATING PATHS IDEA - If a tool changement is performed, than a setup arc must be considered after a tool arc
		cout << "Creating constraint 6..." << endl;
		for (int k = 0; k < data->M; k++) {
			for (int q = 1; q < data->H-1; q++) {
				GRBLinExpr C6 = 0;
				//ARCS LEAVING q
				//tool arcs
				for (int t = 0; t < data->T; t++)
				{
					for (int r = q + 1; r < data->H; r++) {
						if (data->toolNP[t][q][r][k]) {
							C6 += y[t][q][r][k];
						}
					}
				}
				//ARCS ARRIVING IN q
				//setup arcs
				for (int p = 0; p < q; p++) {
					if (data->setupNP[p][q][k]) {
						C6 -= data->setupCoefs[p][q][k] * s[p][q][k];
					}
				}
				string cname;
				cname = "C6_" + to_string(k) + "_" + to_string(q);
				model.addConstr(C6 == 0, cname);
				constraints++;
			}
		}

		/*
		//These constraints are not needed, but can be considered as valid inequalities
		cout << "Creating constraint 7..." << endl;
		for (int k = 0; k < data->M; k++) {
			for (int q = 1; q < data->H - 1; q++) {
				GRBLinExpr C7 = 0;
				//ARCS LEAVING q
				//setup arcs
				for (int r = q + 1; r < data->H; r++) {
					if (data->setupNP[q][r][k]) {
						C7 += data->setupCoefs[q][r][k] * s[q][r][k];
					}
				}
				//ARCS ARRIVING IN q
				//tools arcs
				for (int t = 0; t < data->T; t++)
				{
					for (int p = 0; p < q; p++) {
						if (data->toolNP[t][p][q][k]) {
							C7 -= y[t][p][q][k];
						}
					}
				}
				string cname;
				cname = "C7_" + to_string(k) + "_" + to_string(q);
				model.addConstr(C7 == 0, cname);
				constraints++;
			}
		}
		*/
		//Makespan constraint
		for (int i = 0; i < data->N; i++) {	//number of jobs
			GRBLinExpr C8 = Z;
			for (int k = 0; k < data->M; k++) {
				for (int p = 0; p < data->H; p++) {
					for (int q = 0; q < data->H; q++) {
						if (data->jobNP[i][p][q][k])
						{
							C8 += -(q * x[i][p][q][k]);
						}
					}
				}
			}
			string cname;
			cname = "C8_" + to_string(i);
			model.addConstr(C8 >= 0, cname);
			constraints++;
		}
		
		//VALID INEQUALITIES
		/*
		//NO TWO CONSECUTIVE SETUP ARCS ON SAME MACHINE
		cout << "Creating VI 1..." << endl;
		for (int k = 0; k < data->M; k++) {
			for (int q = 1; q < data->H - 1; q++) {
				GRBLinExpr VI1 = 0;
				//ARCS LEAVING q
				//tool arcs
				for (int t = 1; t < data->C_m[k] + 1; t++) {
					int aux_end_time = q + t * data->st_m[k];
					if (aux_end_time < data->H - 1 && data->setupNP[q][aux_end_time][k]) {
						VI1 += s[q][aux_end_time][k];
					}
				}

				//ARCS ARRIVING IN q
				//setup arcs
				for (int t = 1; t < data->C_m[k] + 1; t++) {
					int aux_start_time = q - t * data->st_m[k];
					if (aux_start_time >= 0 && data->setupNP[aux_start_time][q][k]) {
						VI1 += s[aux_start_time][q][k];
					}
				}
				string cname;
				cname = "VI1_" + to_string(k) + "_" + to_string(q);
				model.addConstr(VI1 <= 1, cname);
				constraints++;
			}
		}
		
		//FOR A GIVEN JOB AND A GIVEN MACHINE, IF THE NUMBER OF TOOLS REQUIRED BY THE JOB IS MOR THEN THE NUMBER OF SLOTS OF THE MACHINE, THEN AL VARIABLES ASSOCIATED TO THAT JOB ON THAT MACHINE SHOULD BE REMOVED / FIXED TO ZERO
		cout << "Creating VI 2..." << endl;
		int auxFixed = 0;
		for (int k = 0; k < data->M; k++) {
			for (int j = 0; j < data->N; j++) {
				if (data->tr_j[j].size() > data->C_m[k]) { //then fix all variables to zero
					for (int p = 0; p < data->H; p++) {
						for (int q = 0; q < data->H; q++) {
							if (data->jobNP[j][p][q][k]){
								GRBLinExpr VI2 = x[j][p][q][k];
								string cname = "VI2_" + to_string(j) + "_" + to_string(p) + "_" + to_string(q) + "_" + to_string(k);
								model.addConstr(VI2 == 0, cname);
								auxFixed++;
							}
						}
					}
				}
			}
		}
		cout << "Var fixed: " << auxFixed << endl;

		////NUMBER OF SETUPS PER MACHINE IS ALWAYS SMALLER THEN THE NUMBER OF JOBS PER MACHINE
		cout << "Creating VI 3..." << endl;
		for (int k = 0; k < data->M; k++) {
			GRBLinExpr VI3 = 0;
			//SUM ALL JOB ARCS ON MACHINE K
			for (int i = 0; i < data->N; i++) {	//number of jobs
				for (int p = 0; p < data->H; p++) {
					for (int q = 0; q < data->H; q++) {
						if (data->jobNP[i][p][q][k])
						{
							VI3 += x[i][p][q][k];
						}
					}
				}
			}
			//SUM ALL SETUP ARCS ON MACHINE K
			for (int p = 1; p < data->H - 1; p++) {
				for (int t = 1; t < data->C_m[k] + 1; t++) {
					int aux_end_time = p + t * data->st_m[k];
					if (aux_end_time < data->H - 1 && data->setupNP[p][aux_end_time][k]) {
						VI3 -= s[p][aux_end_time][k];
					}
				}
			}

			string cname = "VI3_" + to_string(k);
			model.addConstr(VI3 >= 1, cname);
		}

		////MAKESPAN BASED ON MACHINES AND LOSS ARCS INSTEAD OF JOB ARCS
		cout << "Creating constraint 9 and 10..." << endl; //together are equivalent to constraint 8
		for (int k = 0; k < data->M; k++) {
			GRBLinExpr C9 = Z;
			GRBLinExpr C10 = 0;
			for (int p = 0; p < data->H; p++) {
				if (data->lossNP[p][k]) {
					C9 += -(p * l[p][k]);
					C10 += l[p][k];
				}
			}
			string cname;
			cname = "C9_" + to_string(k);
			model.addConstr(C9 >= 0, cname);
			cname = "C10_" + to_string(k);
			model.addConstr(C10 == 1, cname);
			constraints++;
		}


		cout << constraints << " constraints created!!" << endl;
		model.update();
		//getchar();
		*/

		//Update elapsed time

		//Setting gurobi parameters
		model.getEnv().set(GRB_DoubleParam_TimeLimit, parameters->model_tLim);
		model.getEnv().set(GRB_IntParam_Seed, 1);
		model.getEnv().set(GRB_IntParam_DisplayInterval, 10);
		model.getEnv().set(GRB_IntParam_Method, -1); //-1=automatic, 0=primal simplex, 1=dual simplex, 2=barrier, 3=concurrent, 4=deterministic concurrent
		model.getEnv().set(GRB_DoubleParam_MIPGap, 0.0);
		model.getEnv().set(GRB_DoubleParam_MIPGapAbs, 0.99999);
		model.getEnv().set(GRB_IntParam_Threads, 1);
		//model.getEnv().set(GRB_DoubleParam_Cutoff, data->T);

		//model.getEnv().set(GRB_IntParam_Cuts, 0);
		//model.getEnv().set(GRB_IntParam_Presolve, 0);
		//model.getEnv().set(GRB_IntParam_Aggregate, 0);
		model.getEnv().set(GRB_IntParam_MIPFocus, 1);			// 1 = feasible solutions
		//model.getEnv().set(GRB_DoubleParam_Heuristics, 0.95);	//default = 0.05
		////model.getEnv().set(GRB_IntParam_DisplayInterval, 10);
		//model.getEnv().set(GRB_IntParam_CutPasses, 0);

		string str = string("Models/") + data->instanceName + string(".lp");
		//string str = string("Models/\\") + data->instanceName + string(".lp");
		//model.write(str);	//uncomment to write lp in file
		str = string("Gurobi_logs/") + data->instanceName + string(".log");
		ofstream logfile(str);

		//// Create a callback object and associate it with the model
		nbColumns = model.get(GRB_IntAttr_NumVars);
		GRBVar *vars = vars = model.getVars();
		myGRBcallback cb = myGRBcallback(data, nbColumns, vars, &logfile);
		model.setCallback(&cb);

		//Solving model
		cout << "Solving model..." << endl << endl;
		model.optimize();
		grb_solStatus = model.get(GRB_IntAttr_Status);

		if (grb_solStatus == GRB_INFEASIBLE) {
			model.computeIIS();
			str = string("IIS_Models/\\") + data->instanceName + string(".ilp");
			model.write(str);
		}
		else {
			solution->hasSolution = true;
			solution->cmax = model.get(GRB_DoubleAttr_ObjVal);
			double varVal = 0;
			for (int k = 0; k < data->M; k++) {
				for (int p = 0; p < data->H; p++) {
					for (int q = 0; q < data->H; q++) {
						for (int i = 0; i < data->N; i++) {
							if (data->jobNP[i][p][q][k]) {
								varVal = x[i][p][q][k].get(GRB_DoubleAttr_X);
								if (varVal > 0.5) {
									cout << "x(" + to_string(i) + "_" + to_string(p) + "_" + to_string(q) + "_" + to_string(k) + ")\t=\t" << varVal << endl;
									solution->jobNP[i][p][q][k] = 1;
								}
							}
						}
					}
				}

				//SETUP/SWITCHING PROCESSING ARCS
				for (int p = 0; p < data->H; p++) {
					for (int q = 0; q < data->H; q++) {
						if (data->setupNP[p][q][k]) {
							varVal = s[p][q][k].get(GRB_DoubleAttr_X);
							if (varVal > 0.5) {
								cout << "s(" + to_string(p) + "_" + to_string(q) + "_" + to_string(k) + ")\t=\t" << varVal << endl;
								solution->setupNP[p][q][k] = 1;
							}
						}
					}
				}
				//LOSS PROCESSING ARCS
				for (int p = 0; p < data->H; p++) {
					if (data->lossNP[p][k]) {
						varVal = l[p][k].get(GRB_DoubleAttr_X);
						if (varVal > 0.5) {
							cout << "l(" + to_string(p) + "_" + to_string(k) + ")\t=\t" << varVal << endl;
							solution->lossNP[p][k] = 1;
						}
					}
				}

				// y variables -- arc variables
				//TOOLS PROCESSING ARCS
				for (int p = 0; p < data->H; p++) {
					for (int q = 0; q < data->H; q++) {
						for (int t = 0; t < data->T; t++) {
							if (data->toolNP[t][p][q][k]) {
								varVal = y[t][p][q][k].get(GRB_DoubleAttr_X);
								if (varVal > 0.5) {
									cout << "y(" + to_string(t) + "_" + to_string(p) + "_" + to_string(q) + "_" + to_string(k) + ")\t=\t" << varVal << endl;
									solution->toolNP[t][p][q][k] = 1;
								}
							}
						}
					}
				}
			}

		}
		cout << "End getting solution" << endl;
		//Getting final information
		nbColumns = model.get(GRB_IntAttr_NumVars);
		nbRows = model.get(GRB_IntAttr_NumConstrs);
		nbNodes = model.get(GRB_DoubleAttr_NodeCount);
		modelUB = model.get(GRB_DoubleAttr_ObjVal);
		modelLB = model.get(GRB_DoubleAttr_ObjBound);
		modelGap = model.get(GRB_DoubleAttr_MIPGap);
		modelExec_time = model.get(GRB_DoubleAttr_Runtime);

		//model.write(string("solutionAF.sol"));
		str = string("Solutions/") + data->instanceName + string(".sol");
		model.write(str);	//uncomment to write solution in file

	}
	catch (GRBException e) {
		cout << "Error code = " << e.getErrorCode() << endl;
		cout << e.getMessage() << endl;
	}
	catch (...) {
		cout << "Error during optimization" << endl;
	}

}
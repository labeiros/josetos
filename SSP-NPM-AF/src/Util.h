#ifndef Util_H
#define Util_H

#include <vector>

using namespace std;

bool sortVectorOfPairs_inc(const pair< int, double >& i, const pair< int, double >& j);
bool sortVectorOfPairs_dec(const pair< int, double >& i, const pair< int, double >& j);
double maxdbl ( double i, double j);
double mindbl ( double i, double j);
int maxint ( int i, int j);
int minint ( int i, int j);
//double cpuTime();

#endif

//---------------------------------------------------------------------------

#ifndef MODEL_H
#define MODEL_H

#include "Data.h"
#include "Parameters.h"
#include "Solution.h"
#include "MyGRBcallback.h"
using namespace std;

class Model {

public:

	//Methods
	Model(Data *data, Parameters *parameters, Solution *solution);	//constructor
	virtual~Model();							//destructor
	void createArcFlowModel_grb_2();			//Arc-flow model gurobi
	
	//Output vars
	int nbRows, nbColumns, grb_solStatus, varsAdded, iter;
	double nbNodes, modelLB, modelLB_lp, modelUB, modelGap, modelExec_time, nbUnexpNodes;

private:
	Data *data;
	Parameters *parameters;
	Solution *solution;

};

#endif
//---------------------------------------------------------------------------



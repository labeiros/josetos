//---------------------------------------------------------------------------

#include "Solution.h"
//#include "MyGRBcallback.h"
#include <string>

Solution::Solution(Data* data, Parameters* parameters) {	//construtor
	this->data = data;
	this->parameters = parameters;
	hasSolution = false;
	cmax = 0;
	initialize();
}

Solution::~Solution() { // Destrutor
}

string Solution::generate_hex_color() {
	string generated_string_color = "#";
	int auxRand;
	for (int j = 0; j < 6; j++) { //six values
		auxRand = rand() % 16;     // v2 in the range 1 to 100
		switch (auxRand) {
		case 10:
			generated_string_color = generated_string_color + "a";
			break;
		case 11:
			generated_string_color = generated_string_color + "b";
			break;
		case 12:
			generated_string_color = generated_string_color + "c";
			break;
		case 13:
			generated_string_color = generated_string_color + "d";
			break;
		case 14:
			generated_string_color = generated_string_color + "e";
			break;
		case 15:
			generated_string_color = generated_string_color + "f";
			break;
		default:
			generated_string_color = generated_string_color + to_string(auxRand);
		}
	}
	return generated_string_color;
}

void Solution::initialize() {
	cout << "Creating solution structure..." << endl;
	colors = new string[data->N];
	tools_colors = new string[data->T];
	for (int i = 0; i < data->N; i++) {
		colors[i] = generate_hex_color();
	}
	for (int t = 0; t < data->T; t++) {
		tools_colors[t] = generate_hex_color();
	}


	jobNP = new bool*** [data->N];
	for (int i = 0; i < data->N; i++) {
		jobNP[i] = new bool** [data->H];
		for (int p = 0; p < data->H; p++) {
			jobNP[i][p] = new bool* [data->H];
			for (int q = 0; q < data->H; q++) {
				jobNP[i][p][q] = new bool[data->M];
				for (int k = 0; k < data->M; k++) {
					jobNP[i][p][q][k] = 0;
				}
			}
		}
	}

	//JOB SETUP ARCS AND TOOLS SWITCHING ARCS
	//cout << "\tcreating Setups NP...\t";
	setupNP = new bool** [data->H];
	setupCoefs = new int** [data->H];
	for (int p = 0; p < data->H; p++) {
		setupNP[p] = new bool* [data->H];
		setupCoefs[p] = new int* [data->H];
		for (int q = 0; q < data->H; q++) {
			setupNP[p][q] = new bool[data->M];
			setupCoefs[p][q] = new int[data->M];
			for (int k = 0; k < data->M; k++) {
				setupNP[p][q][k] = 0;
				setupCoefs[p][q][k] = 0;
			}
		}
	}

	//JOB LOSS ARCS
	//cout << "\tcreating Loss NP...\t";
	lossNP = new bool* [data->H];
	for (int p = 0; p < data->H; p++) {
		lossNP[p] = new bool[data->M];
		for (int k = 0; k < data->M; k++) {
			lossNP[p][k] = 0;
		}
	}

	//TOOL "PROCESSING" ARCS
	//cout << "\tcreating Tools NP...\t";
	toolNP = new bool*** [data->T];
	for (int t = 0; t < data->T; t++) {
		toolNP[t] = new bool** [data->H];
		for (int p = 0; p < data->H; p++) {
			toolNP[t][p] = new bool* [data->H];
			for (int q = 0; q < data->H; q++) {
				toolNP[t][p][q] = new bool[data->M];
				for (int k = 0; k < data->M; k++) {
					toolNP[t][p][q][k] = 0;
				}
			}
		}
	}
}

void Solution::printSolution_html()
{
	cout << "Printing solution html..." << endl;
	//Print results in file
	int st_year = 0;
	int st_month = 0;
	int st_day = 0;
	int st_hour = 0;
	int st_min = 0;

	string str = string("Solutions_html/Sol_") + data->instanceName + string(".html");
	ofstream out_html(str.c_str(), ios::out);

	out_html << "<html>\n\t<body>" << endl;
	out_html << "\t<h1>Instance: \"" << data->instanceName << " \"</h1>" << endl;
	//out_html << "\t<p>Lunghezza della sequenza: " << input_data->Nseq;// << "</p>" << endl;
	//out_html << "  Sequenza: ";
	//for (int i = 0; i < job_sequence.size(); i++)
	//{
	//	out_html << input_data->input_seq[job_sequence[i]];
	//	if (i != input_data->Nseq - 1)
	//	{
	//		out_html << " - ";
	//	}
	//}
	//out_html << "</p>" << endl;
	//out_html << "\t<p>Divisible: " << parameters->divisible << "  Iter: " << parameters->nb_iter << "</p>" << endl;
	//out_html << "\t<p>Tempo di fine: " << Cmax << "  Produttivitą media nel intervalo [ " << ini_t / 60 << ", " << end_t / 60 << " ]: " << setprecision(2) << fixed << prod_avg * 100 << "%" << "</p>" << endl;

	//out_html << "<style>" << endl;
	//out_html << "\ttable, th, td{" << endl;
	//out_html << "\tborder: 1px solid black;" << endl;
	//out_html << "\tborder - collapse: collapse;" << endl;
	//out_html << "\tpadding: 7px;\n}" << endl;
	//out_html << "</style>" << endl;
	//out_html << "<table BORDER=\"2\">" << endl;

	////First row with labels of the columns
	//out_html << "<tr>";
	//out_html << "\t<td>Prod</td> "; //first column
	//for (int j = 0; j < input_data->Nseq; j++)
	//{
	//	out_html << "<td>" << j << "</td >";
	//}
	//out_html << "</tr>" << endl;

	////Second row with data
	//out_html << "<tr>";
	//out_html << "\t<td>T.fine</td> "; //first column
	//for (int i = 0; i < job_sequence.size(); i++)
	//{
	//	out_html << "<td>" << job_ct[i] << "</td>";
	//}
	//out_html << "</tr>" << endl;
	//out_html << "</table>" << endl;
	out_html << "\t</body>" << endl;
	out_html << "<head>" << endl;

	out_html << "<script type =\"text/javascript\"src = \"https://www.gstatic.com/charts/loader.js\"></script>" << endl;
	//out_html << "<script type =\"text/javascript\"src = \"D:/Research/Projetos/E80_Assembly line/loader.js\"></script>" << endl;
	out_html << "<script type =\"text/javascript\">" << endl;
	out_html << "google.charts.load(\"current\", {packages:[\"timeline\"]});" << endl;
	out_html << "google.charts.setOnLoadCallback(drawChart);" << endl;
	out_html << "function drawChart() {" << endl;
	{
		out_html << "var container = document.getElementById('example5.1');" << endl;
		out_html << "var chart = new google.visualization.Timeline(container);" << endl;
		out_html << "var dataTable = new google.visualization.DataTable();" << endl;
		out_html << "dataTable.addColumn({ type: 'string', id : 'Machine' });" << endl;
		out_html << "dataTable.addColumn({ type: 'string', id : 'Job' });" << endl;
		//if (parameters->color_by_job_type || parameters->two_colors)
		//{
			out_html << "dataTable.addColumn({ type: 'string', id : 'style', role: 'style' });" << endl;
		//}
		out_html << "dataTable.addColumn({ type: 'number', id : 'Start' });" << endl;
		out_html << "dataTable.addColumn({ type: 'number', id : 'End' });" << endl;
		out_html << "dataTable.addRows([" << endl;
		{
			for (int k = 0; k < data->M; k++) {
				//JOBS BARS
				for (int i = 0; i < data->N; i++) {
					for (int p = 0; p < data->H; p++) {
						for (int q = 0; q < data->H; q++) {
							if (jobNP[i][p][q][k]) {
								//out_html << "['Machine " << k << "', '" << i << "', '" << colors[i] << "', new Date( 0, 0, 0, 0, " << p << "), new Date( 0, 0, 0, 0, " << q << ") ]," << endl;
								//out_html << "['Machine " << k << "', '" << i << "', '" << colors[i] << "', new Date(" << p << "), new Date(" << q << ") ]," << endl;
								out_html << "['Machine " << k << "', '" << i << "', '" << colors[i] << "', " << p*1000 << ", " << q*1000 << " ]," << endl;
							}
						}
					}
				}
				//TOOLS BARS - Setups
				vector<int> cmax_slots;
				cmax_slots.resize(data->C_m[k], 0);
				int best = 0;
				int aux_min = data->H + 1;
				for (int p = 0; p < data->H; p++) {
					for (int q = 0; q < data->H; q++) {
						//tools
						for (int t = 0; t < data->T; t++) {
							if (toolNP[t][p][q][k]) {
								for (int sl = 0; sl < data->C_m[k]; sl++) {
									if (cmax_slots[sl] == p) {
										out_html << "['M" << k << "_Slot_" << sl << "', 'T" << t << "', '" << tools_colors[t] << "', " << p * 1000 << ", " << q * 1000 << " ]," << endl;
										cmax_slots[sl] = q;
										break;
									}
								}
							}
						}
						//SETUP/SWITCHING PROCESSING ARCS
						if (setupNP[p][q][k]) {
							out_html << "['Machine " << k << "', 'S" << data->setupCoefs[p][q][k] << "', '" << "stroke-color: #ff0000; stroke-width: 2; fill-color: #191919" << "', " << p * 1000 << ", " << q * 1000 << " ]," << endl;
							for (int ii = 0; ii < data->setupCoefs[p][q][k]; ii++) {//depending on the setup size, it must be taken multiple times
								for (int sl = 0; sl < data->C_m[k]; sl++) {
									if (cmax_slots[sl] == p) {
										//setup arcs;
										out_html << "['M" << k << "_Slot_" << sl << "', 'S" << data->setupCoefs[p][q][k] << "', '" << "stroke-color: #ff0000; stroke-width: 2; fill-color: #191919" << "', " << p * 1000 << ", " << q * 1000 << " ]," << endl;
										cmax_slots[sl] = q;
										break;
									}
								}
							}
						}
					}
					//LOSS ARCS
					if (lossNP[p][k]) {
						//copy in job AF
						out_html << "['Machine " << k << "', 'L" << p << "', '" << "stroke-color: #000000; stroke-width: 2; fill-color: #ffffff" << "', " << p * 1000 << ", " << data->H * 1000 << " ]," << endl;
						for (int sl = 0; sl < data->C_m[k]; sl++) {
							if (cmax_slots[sl] == p) {
								//copy in tool AF
								out_html << "['M" << k << "_Slot_" << sl << "', 'L" << p << "', '" << "stroke-color: #000000; stroke-width: 2; fill-color: #ffffff" << "', " << p * 1000 << ", " << data->H * 1000 << " ]," << endl;
								cmax_slots[sl] = data->H;
								break;
							}
						}
					}
				}

			}
		}
		out_html << "]);" << endl;
		out_html << "var options = {" << endl;
		out_html << "timeline: { colorByRowLabel: true, groupByRowLabel: true, }" << endl;
		out_html << "};" << endl;
		out_html << "chart.draw(dataTable, options);" << endl;

	}
	out_html << "}" << endl;
	out_html << "</script>" << endl;
	out_html << "</head>" << endl;
	out_html << "\t<body>" << endl;
	out_html << "\t<div id=\"example5.1\" style=\"height: 1080px; width: " << cmax*40 << "px; \"></div>" << endl;
	out_html << "\t</body>" << endl;
	out_html << "\t</html>" << endl;

	out_html.close();

}
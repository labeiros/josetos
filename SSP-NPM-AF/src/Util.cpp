#include "Util.h"
#include <sys/timeb.h>
//#include <sys/resource.h>


bool sortVectorOfPairs_inc(const pair< int, double >& i, const pair< int, double >& j) //creating a pairbool sortVectorOfPairs( pair< int, int > i, pair< int, int > j )
{
	return (i.second < j.second);
}

bool sortVectorOfPairs_dec(const pair< int, double >& i, const pair< int, double >& j) //creating a pairbool sortVectorOfPairs( pair< int, int > i, pair< int, int > j )
{
	return (i.second > j.second);
}

double maxdbl ( double i, double j)
{
	if ( i >= j )
		return i;
	else
		return j;
}

double mindbl ( double i, double j)
{
	if ( i < j )
		return i;
	else
		return j;	
}

int maxint ( int i, int j)
{
	if ( i >= j )
		return i;
	else
		return j;
}

int minint ( int i, int j)
{
	if ( i < j )
		return i;
	else
		return j;	
}

//double cpuTime() {
//	static struct rusage usage;
//	getrusage(RUSAGE_SELF, &usage);
//	return ((double)usage.ru_utime.tv_sec)+(((double)usage.ru_utime.tv_usec)/((double)1000000));
//}

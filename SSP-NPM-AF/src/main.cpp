#include "Data.h"
#include "Parameters.h"
#include "Solution.h"
#include "Model.h"
//#include "MyGRBcallback.h"
#include <ilcplex/ilocplex.h>
#include <time.h>

using namespace std;
//using namespace std::chrono;

int main(int argc, char** argv)
{

	//long long seed = time(0);
	long long seed = 1519835016;
	//while ( seed == time(0) ) {}
	srand(seed);
	cout << "\nSeed: " << seed << endl;
	//Start time
	double t_begin = clock();
	double diff_time;

	//Create new parameters object
	Parameters *parameters = new Parameters();
	if(argc == 5)	//Then I am passing the time limit as an argument
	{
		string str(argv[4]);
		int tlim = atoi(str.c_str());
		if(tlim > 0)
		{
			parameters->model_tLim = tlim;
		}else
		{
			cout << "Warning!! Invalid time limit inputed. Using the standard one" << endl;
		}
		cout << "Time limit: " << parameters->tLim << endl;
	}

	//Create new data object
	Data *data = new Data(argc, argv, parameters);
	//Initialize data - read instance and create arc-flow arcs
	cout << "Reading data..." << endl;
	data->readData();
	cout << "...reading finished!" << endl;
	data->estimateT();
	cout << "T estimated! = " << data->H << endl;
	//data->modifData();
	data->printData();
	data->create_NP();
	cout << "-----------------------" << endl;
	//--------------------------------------------------------
	//Create solution object
	Solution* solution = new Solution(data, parameters);

	//Create model object
	Model *model = new Model(data, parameters, solution);
	model->createArcFlowModel_grb_2();	//use gurobi
	if (solution->hasSolution) {
		solution->printSolution_html();
	}

	cout << "Final Status: best LB: " << model->modelLB << "\tbest UB: " << model->modelUB << "\troot LB: " << data->rootRelObj << endl;

	//Print total execution time
	diff_time = (clock() - t_begin) / CLOCKS_PER_SEC;
	parameters->elapsed_time += diff_time;
	cout << "Elapsed time: " << parameters->elapsed_time << endl;

	//Print results in file
	ofstream Results(argv[3], ios::app);
	Results << data->instanceName.c_str()
		<< "\t" << data->N
		<< "\t" << data->M
		<< "\t" << data->T
		<< "\t" << data->H
		<< "\t" << model->nbColumns
		<< "\t" << model->nbRows
		<< "\t" << model->nbNodes
		<< "\t" << model->modelUB
		<< "\t" << model->modelLB
		<< "\t" << data->rootRelObj
		<< "\t" << model->modelGap
		<< "\t" << model->modelExec_time
		<< "\t" << std::setprecision(3) << std::fixed << diff_time
		<< endl;

	Results.close();
	//Free memory
	delete model;
	delete solution;
	delete data;
	delete parameters;
	cout << "Execution finished!" << endl;
	return 0;
}
#pragma once
//---------------------------------------------------------------------------

#ifndef SOLUTION_H
#define SOLUTION_H

#include "Data.h"
#include "Parameters.h"
using namespace std;

class Solution {

public:

	//Variables
	bool hasSolution;
	double cmax;
	bool**** jobNP;	//j p q k
	bool*** setupNP;	//p q k
	int*** setupCoefs;	//p q k
	bool** lossNP;	//p k
	bool**** toolNP;	//j p q k
	string* colors;
	string* tools_colors;

	//Methods
	Solution(Data* data, Parameters* parameters);	//constructor
	void initialize();
	void printSolution_html();
	string generate_hex_color();
	virtual~Solution();							//destructor
	
private:
	Data* data;
	Parameters* parameters;
	
};

#endif
//---------------------------------------------------------------------------



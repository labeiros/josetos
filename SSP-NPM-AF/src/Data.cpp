//---------------------------------------------------------------------------

#include "Data.h"
#include <string>

Data::Data(int argc, char** argv, Parameters *parameters) {	//construtor

	argV = argv;
	argC = argc;
	instanceName = getInstanceName();
	rootRelObj = -1;
	this->parameters = parameters;

}

Data::~Data() { // Destrutor

}

void Data::readData()
{
	//Check if number of parameters is ok
	if (argC < 4) {
		cout << "\nMissing Parameters\n";
		cout << " ./exe [Instance][outputFile][**tlim]" << endl;
		exit(1);
	}

	if (argC > 5) {
		cout << "\nToo many parameters\n";
		cout << " ./exe [Instance][outputFile][**tlim]" << endl;
		exit(1);
	}

	//Open instance file
	char* instance;
	instance = argV[1];
	ifstream in(instance, ios::in);
	if (!in) {
		cout << "File not found\n";
		exit(1);
	}

	//Aux variable
	string auxN, auxM, auxT, auxInt;


	//First Row: m; j; t;
	string auxS = "a";
	in >> auxS; // auxS contains First Row: m; j; t;
	string::size_type loc = auxS.find(";", 0);	//find the 1st semicolon
	string::size_type loc_b = auxS.find(";", loc + 1);	//find the 2nd semicolon
	string::size_type loc_c = auxS.find(";", loc_b + 1);	//find the 3rd semicolon

	//cout << auxS << ":\t" << loc << " " << loc_b << " " << loc_c << endl;
	auxM.append(auxS, 0, loc);	//string_to.append(string_from, 1st_position, how_many_chars)
	auxN.append(auxS, loc + 1, loc_b - loc - 1);	//string_to.append(string_from, 1st_position, how_many_chars)
	auxT.append(auxS, loc_b + 1, loc_c - loc_b - 1);	//string_to.append(string_from, 1st_position, how_many_chars)
	
	M = stoi(auxM);
	N = stoi(auxN);
	T = stoi(auxT);
	//cout << N << " " << M << " " << T << endl;


	//Initialize data structure
	C_m = new int[M];
	st_m = new int[M];
	p_mj = new int* [M];
	tau_tj = new bool* [T];
	sum_required_tools = 0;

	////Second Row: tool capacity of each machine: C_m
	in >> auxS; // auxS contains Second Row
	loc = 0;
	for (int i = 0; i < M; i++) {
		auxInt.resize(0);
		if (i > 0) {
			loc_b = auxS.find(";", loc+1);	//find the i_th semicolon
			auxInt.append(auxS, loc + 1, loc_b - loc - 1);
		}
		else {
			loc_b = auxS.find(";", 0);	//find the i_th semicolon
			auxInt.append(auxS, 0, loc_b);
		}
		loc = loc_b;
		C_m[i] = stoi(auxInt);// -3;
	}
	//Third Row: Switching time of each machine
	in >> auxS; // auxS contains Third Row
	loc = 0;
	for (int i = 0; i < M; i++) {
		auxInt.resize(0);
		if (i > 0) {
			loc_b = auxS.find(";", loc + 1);	//find the i_th semicolon
			auxInt.append(auxS, loc + 1, loc_b - loc - 1);
		}
		else {
			loc_b = auxS.find(";", 0);	//find the i_th semicolon
			auxInt.append(auxS, 0, loc_b);
		}
		loc = loc_b;
		st_m[i] = stoi(auxInt);
	}
	//Rows 4 to 4+M: processing times of jobs on the machines
	for (int j = 0; j < M; j++) {
		p_mj[j] = new int[N];
		in >> auxS; // auxS contains procssing times of all jobs on jth machine
		loc = 0;
		for (int i = 0; i < N; i++) {
			auxInt.resize(0);
			if (i > 0) {
				loc_b = auxS.find(";", loc + 1);	//find the i_th semicolon
				auxInt.append(auxS, loc + 1, loc_b - loc - 1);
			}
			else {
				loc_b = auxS.find(";", 0);	//find the i_th semicolon
				auxInt.append(auxS, 0, loc_b);
			}
			loc = loc_b;
			p_mj[j][i] = stoi(auxInt);
		}
	}

	//Remaining rows: Job (column) -Tool (row) -Matrix // "1" indicates that job x in column x requires a tool
	tr_j.resize(N); //one line per job
	for (int j = 0; j < T; j++) { //tools
		tau_tj[j] = new bool[N];
		in >> auxS; // auxS contains procssing times of all jobs on jth machine
		loc = 0;
		for (int i = 0; i < N; i++) { //jobs
			auxInt.resize(0);
			if (i > 0) {
				loc_b = auxS.find(";", loc + 1);	//find the i_th semicolon
				auxInt.append(auxS, loc + 1, loc_b - loc - 1);
			}
			else {
				loc_b = auxS.find(";", 0);	//find the i_th semicolon
				auxInt.append(auxS, 0, loc_b);
			}
			loc = loc_b;
			tau_tj[j][i] = stoi(auxInt);
			if (tau_tj[j][i] == true) {
				tr_j[i].push_back(j);
				sum_required_tools++;
			}
		}
	}

	//Close file
	in.close();

}

void Data::printData()
{
	//Print data
	cout << "=====================================================" << endl;
	cout << "Instance: " << instanceName << endl;
	cout << "Data:\nN = " << N << "\tM = " << M << "\tT = " << T << "\tH = " << H << "\th = " << minH << "\tReqTools = " << sum_required_tools << endl;
	cout << "C_m = {\t";
	for (int i = 0; i < M; i++) {
		cout << C_m[i] << " ";
	}
	cout << "}\nSt_m = { ";
	for (int i = 0; i < M; i++) {
		cout << st_m[i] << " ";
	}
	cout << "}\np_mj:" << endl;
	for (int j = 0; j < M; j++) {
		cout << "\tMachine " << j << ":\t{ ";
		for (int i = 0; i < N; i++) {
			cout << p_mj[j][i] << " ";
		}cout << " }" << endl;
	}
	cout << "}\ntau_tj:" << endl;
	for (int j = 0; j < T; j++) {
		cout << "\tTool " << j << ":\t{ ";
		for (int i = 0; i < N; i++) {
			cout << tau_tj[j][i] << " ";
		}cout << "}" << endl;
	}cout << endl;
	cout << "Tr_j:" << endl;
	for (int i = 0; i < N; i++) {
		cout << "\tJob " << i << ":\t{";
		for (int j = 0; j < tr_j[i].size(); j++) {
			cout << " " << tr_j[i][j];
		}cout << " }" << endl;
	}cout << endl;

	cout << "=====================================================" << endl;

}

string Data::getInstanceName()
{

	string arquivo(argV[1]);
	string::size_type loc = arquivo.find_last_of(".", arquivo.size());
	string::size_type loc2 = arquivo.find_last_of("/\\", arquivo.size());
	string nomeDaInstancia;
	if (loc != string::npos) {
		nomeDaInstancia.append(arquivo, loc2 + 1, loc - loc2 - 1);
		//cout << "\n1-" << nomeDaInstancia << endl;
	}
	else {
		nomeDaInstancia.append(arquivo, loc2 + 1, arquivo.size());
		//cout << "\n2-" << nomeDaInstancia << endl;
	}

	return nomeDaInstancia;

}

void Data::estimateT()
{

	string str(argV[2]);
	H = atoi(str.c_str());
	H_less_one + H - 1;


	//int maxSt = 0;
	//int maxPmj = 0;
	//H_less_one = 0;
	//
	//int minSt = 9999999999;
	//int minPmj = 9999999999;
	//minH = 0;

	//for (int i = 0; i < N; i++) {
	//	maxPmj = 0;
	//	minPmj = 9999999999;
	//	for (int k = 0; k < M; k++) {
	//		//Get max
	//		if (p_mj[k][i] > maxPmj) {
	//			maxPmj = p_mj[k][i];
	//		}
	//		//Get min
	//		if (p_mj[k][i] < minPmj) {
	//			minPmj = p_mj[k][i];
	//		}
	//	}
	//	H_less_one += maxPmj;
	//	minH += minPmj;
	//}
	//for (int k = 0; k < M; k++) {
	//	//Get max
	//	if (st_m[k] > maxSt) {
	//		maxSt = st_m[k];
	//	}
	//	//Get min
	//	if (st_m[k] < minSt) {
	//		minSt = st_m[k];
	//	}
	//}
	//H_less_one += maxSt * sum_required_tools*N; // sum_required_tools is supposed to be an estimation on the maximum number os switches
	//H = H_less_one + 1;


	//int total_cap = 0;
	//for (int k = 0; k < M; k++) {
	//	total_cap += C_m[k];
	//}
	//int min_changes = T - total_cap;
	//if (min_changes < 0) {
	//	min_changes = 0;
	//}
	//minH += minSt * min_changes; // min_changes is supposed to be an estimation on the minimum number os switches
	//minH = ceil(minH / M);
}

void Data::create_NP()
{
	int nb_arcs = 0;
	int nb_job_arcs = 0;
	int nb_setup_arcs = 0;
	int nb_switching_arcs = 0;
	int nb_tools_arcs = 0;
	int nb_loss_arcs = 0;
	int aux_end_time = 0;

	cout << "Creating NP..." << endl;

	//JOB PROCESSING ARCS
	cout << "\tcreating Job NP...\t";

	jobNP = new bool*** [N];
	for (int i = 0; i < N; i++) {
		jobNP[i] = new bool** [H];
		for (int p = 0; p < H; p++) {
			jobNP[i][p] = new bool* [H];
			for (int q = 0; q < H; q++) {
				jobNP[i][p][q] = new bool[M];
				for (int k = 0; k < M; k++) {
					jobNP[i][p][q][k] = 0;
				}
			}
		}
	}
	//FILL MATRIX
	for (int k = 0; k < M; k++) {
		for (int i = 0; i < N; i++) {
			for (int p = 0; p < H - p_mj[k][i]; p++) {
				jobNP[i][p][p+ p_mj[k][i]][k] = 1;
				nb_job_arcs++;
			}
		}
	}
	cout << nb_job_arcs << " job arcs." << endl;
	nb_arcs += nb_job_arcs;

	//JOB SETUP ARCS AND TOOLS SWITCHING ARCS
	cout << "\tcreating Setups NP...\t";
	setupNP = new bool** [H];
	setupCoefs = new int** [H];
	for (int p = 0; p < H; p++) {
		setupNP[p] = new bool* [H];
		setupCoefs[p] = new int* [H];
		for (int q = 0; q < H; q++) {
			setupNP[p][q] = new bool [M];
			setupCoefs[p][q] = new int[M];
			for (int k = 0; k < M; k++) {
				setupNP[p][q][k] = 0;
				setupCoefs[p][q][k] = 0;
			}
		}
	}
	//FILL MATRIX
	for (int k = 0; k < M; k++) {
		for (int p = 1; p < H; p++) {
			for (int t = 1; t < C_m[k]+1; t++) {
				aux_end_time = p + t * st_m[k];
				if (aux_end_time < H-1) {
					setupNP[p][aux_end_time][k] = 1;
					setupCoefs[p][aux_end_time][k] = t;
					nb_setup_arcs++;
				}
			}
		}
	}
	cout << nb_setup_arcs << " setup/switching arcs." << endl;
	nb_arcs += nb_setup_arcs;

	//JOB LOSS ARCS
	cout << "\tcreating Loss NP...\t";
	lossNP = new bool* [H];
	for (int p = 0; p < H; p++) {
		lossNP[p] = new bool [M];
		for (int k = 0; k < M; k++) {
			lossNP[p][k] = 0;
		}
	}
	//FILL MATRIX
	for (int k = 0; k < M; k++) {
		//for (int p = minH; p < H-1; p++) {
		for (int p = 1; p < H-1; p++) {
			lossNP[p][k] = 1;
			nb_loss_arcs++;
		}
	}
	cout << nb_loss_arcs << " loss arcs." << endl;
	nb_arcs += nb_loss_arcs;

	//TOOL "PROCESSING" ARCS
	cout << "\tcreating Tools NP...\t";
	toolNP = new bool*** [T];
	for (int t = 0; t < T; t++) {
		toolNP[t] = new bool** [H];
		for (int p = 0; p < H; p++) {
			toolNP[t][p] = new bool* [H];
			for (int q = 0; q < H; q++) {
				toolNP[t][p][q] = new bool[M];
				for (int k = 0; k < M; k++) {
					toolNP[t][p][q][k] = 0;
				}
			}
		}
	}
	//FILL MATRIX
	for (int k = 0; k < M; k++) {
		for (int t = 0; t < T; t++) {
			//Calculate minimum processing time o job that requires tool t on machine k
			int min_p_mj = H;
			for (int j = 0; j < N; j++) {
				if (tau_tj[t][j] == 1 && p_mj[k][j] < min_p_mj) { // if job j requires tool t and proc time is smaller
					min_p_mj = p_mj[k][j];
				}
			}
			for (int p = 0; p < H; p++) {
				for (int q = p + min_p_mj; q < H; q++) {
					toolNP[t][p][q][k] = 1;
					nb_tools_arcs++;
				}
			}
			toolNP[t][0][(int)H-1][k] = 1;
			nb_tools_arcs++;
		}
	}
	cout << nb_tools_arcs << " tools arcs." << endl;
	nb_arcs += nb_tools_arcs;

	cout << "\tTotal arcs: " << nb_arcs << endl;

}

void Data::modifData()
{
	H = 40;
	H_less_one = H - 1;
}

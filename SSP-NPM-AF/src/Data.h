//---------------------------------------------------------------------------

#ifndef DATA_H
#define DATA_H

#include <iostream>
#include <iomanip>
#include <fstream>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <list>
#include <vector>
#include <set>
#include <algorithm>
#include <iterator>
#include <limits>
#include <ctime>
#include <time.h>
#include <gurobi_c++.h>
#include <ilcplex/ilocplex.h>
#include "Parameters.h"
#include "Util.h"


using namespace std;

class Data {

public:

	//Data
	int N;						// Number of Jobs
	int M;						// Number of machines
	int T;						// Number of tools
	double H;						// Time horizon estimation
	double H_less_one;						// Time horizon estimation
	double minH;					// LB on Cmax value
	
	int *C_m;					// Capacity of each machine
	int* st_m;					// Switching time per each machine
	int** p_mj;					// Processing times
	bool** tau_tj;				// Tool requirements
	vector<vector<int>> tr_j;	// Tool required by each job

	//===========================================================//
	double sum_required_tools;
	bool**** jobNP;	//j p q k
	bool*** setupNP;	//p q k
	int*** setupCoefs;	//p q k
	bool** lossNP;	//p k
	bool**** toolNP;	//j p q k
	
	string instanceName;		// Instance name

	//Extra data
	int iLB;					//iLB
	int iteration;
	double rootRelObj;

	Parameters *parameters;

	//Methods
	Data(int argc, char** argv, Parameters *parameters);	//constructor
	string getInstanceName();		//read instance name
	void readData();				//read data function
	void estimateT();				//estimate the time horizon
	void printData();				//print data
	void create_NP();				//Create normal patterns
	void modifData();

	virtual~Data();					//destructor

private:
	char** argV;
	int argC;

};

#endif
//---------------------------------------------------------------------------



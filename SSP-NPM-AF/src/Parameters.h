//---------------------------------------------------------------------------

#ifndef PARAMETERS_H
#define PARAMETERS_H
//using namespace std;

class Parameters {

public:

	//Parameters
	double tLim;			// Total time limit
	double elapsed_time;	// Elapsed time
	double model_tLim;		// Model time limit
	bool print_all_data;

	//Methods
	Parameters();	//constructor
	virtual~Parameters();	//destructor

private:
	//Data *data;

};

#endif
//---------------------------------------------------------------------------



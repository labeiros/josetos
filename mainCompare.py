###################### divers import
import os
import time
import pandas as pd
from printColors import *

###################### Instance
from instance import *

###################### Calmels IJOR 2021
from MILP.Calmels_IJOR_2021.position_MILP import *
from MILP.Calmels_IJOR_2021.precedence_MILP import *

###################### COR 2024

######### Greedy
from heuristic.greedy import *

######### LB
from MILP.COR_2024.LBmakespan_MILP import *

######### Max Group size
from MILP.COR_2024.GroupSize_MILP import *

######### position based
from MILP.COR_2024.ImprovedPosition_MILP import *
from MILP.COR_2024.AFpos_MILP import *

######### group based
from MILP.COR_2024.JGS_MILP import *
from MILP.COR_2024.AFJGS_MILP import *

###################### Instance directories

######### Calmels 2019
InstSet1 = "Calmels1"
######### Calmels ILS
InstSet2 = "Calmels2"
######### Generated
InstSet3 = "COR_2024"

###################### instances characteristics dictionnary
dictInst = {"instanceSet":[], "instance":[], "|M|":[], "|J|":[], "|T|":[], "requirement density":[], "cluster density":[], "LB":[], "Greedy":[], "LB CPU":[], "Greedy CPU": [], "cluster density CPU": [], "density":[]}


###################### MILP results dictionnary
dictRes = {"instanceSet":[], "instance":[], "model": [], "nbCols":[], "nbRows":[], "model LB":[], "model UB":[], "Gurobi gap":[], "nbNodes":[], "CPU":[]}


###################### Generate new instances
dir = os.path.join("instances",InstSet3)
if not os.path.exists(dir):
    os.makedirs(dir)
instNum = 1
for nbM in range(2,4):
    for nbJ in [10, 15, 20]:
        for nbT in [10, 15, 20]:
            subDir = os.path.join(dir,"0%s-%s-%s"%(nbM, nbJ, nbT))
            if not os.path.exists(subDir):
                os.makedirs(subDir)
            for rd in [0.2, 0.3, 0.4, 0.5]:
                for instVar in range(10):
                    inst = Instance()
                    inst.generates(nbT, nbJ, nbM, rd, mode = "matrix")
                    inst.write(os.path.join(subDir, "ins%s_m=%s_j=%s_t=%s_dens=%s_var=%s.csv"%(instNum, nbM, nbJ, nbT, int(10*rd), instVar)))


###################### Compute caracteristics for all instances
for instSet in [InstSet1, InstSet2, InstSet3]:
    dir = os.path.join("instances",instSet)
    for subDir in os.listdir(dir):
        if os.path.isdir(os.path.join(dir, subDir)):
            for file in os.listdir(os.path.join(dir, subDir)):
                instPath = os.path.join(dir, subDir, file)
                
                ######### read instance
                # initialize an instance object
                instance = Instance()
                # read the instance
                instance.read(instPath)
                
                ######### update machine capacities
                instance.changeMachinesCapacities_v02() #new function
                
                ######### print the instance in the terminal
                print(CYAN)
                instance.printInstance()
                print(RESET)
                dictInst["instanceSet"].append(instSet)
                dictInst["instance"].append(file)
                dictInst["|M|"].append(len(instance.M))
                dictInst["|J|"].append(len(instance.J))
                dictInst["|T|"].append(len(instance.T))
                dictInst["requirement density"].append(instance.requirementDensity)
                dictInst["density"].append(re.sub(".*dens=", "", file)[0])

                ######### compute cluster density
                instance.clusterDensity = 0
                cdCPU = time.time()
                for m in instance.M:
                    for j in instance.J:
                        if j in instance.p_mj[m].keys():
                            maxG = GroupSize_MILP(instance, m, fixedJ=[j])
                            Delta_mj = int(maxG.solve(outputFlag = False))
                            maxG.model.dispose()
                        else:
                            Delta_mj = 0
                        instance.clusterDensity += Delta_mj
                        
                instance.clusterDensity /= (len(instance.M) * len(instance.J)**2)
                cdCPU = time.time() - cdCPU
                dictInst["cluster density"].append(instance.clusterDensity)
                dictInst["cluster density CPU"].append(cdCPU)
            
                ######### compute Lower Bound
                lbCPU = time.time()
                LBMILP = LBmakespan_MILP(instance)
                instance.CmaxLB = int(LBMILP.solve(outputFlag = False))
                lbCPU = time.time() - lbCPU
                dictInst["LB"].append(instance.CmaxLB)
                dictInst["LB CPU"].append(lbCPU)

                ######### compute Upper Bound
                greedyCPU = time.time()
                instance.CmaxUB, blocks, seq_m = randomGreedy(instance)
                greedyCPU = time.time() - greedyCPU
                dictInst["Greedy"].append(instance.CmaxUB)
                dictInst["Greedy CPU"].append(greedyCPU)
                
                dfInst = pd.DataFrame(dictInst)
                dfInst.to_csv(os.path.join("results","instances.csv"))
            

###################### Compute models for all instances
for instSet in [InstSet1, InstSet2, InstSet3]:
    dir = os.path.join("instances",instSet)
    for subDir in os.listdir(dir):
        if os.path.isdir(os.path.join(dir, subDir)):
            for file in os.listdir(os.path.join(dir, subDir)):
                instPath = os.path.join(dir, subDir, file)
                
                ######### read instance
                # initialize an instance object
                instance = Instance()
                # read the instance
                instance.read(instPath)
                
                ######### update machine capacities
                instance.changeMachinesCapacities_v02() #new function
                
                ######### print the instance in the terminal
                print(CYAN)
                instance.printInstance()
                print(RESET)
                dictRes["instanceSet"].append(instSet)
                dictRes["instance"].append(file)

                ######### compute Upper Bound
                greedyCPU = time.time()
                instance.CmaxUB, blocks_mj, seq_m = randomGreedy(instance)
                greedyCPU = time.time() - greedyCPU
                
                for modelMILP in [Position_MILP, ImprovedPosition_MILP, AFpos_MILP, JGS_MILP, AFJGS_MILP]:
                    start = time.time()
                    milp = modelMILP(instance)
                    milp.warmStart(blocks_mj, seq_m)
                    milp.solve()
                    
                    ######### compute Upper Bound
                    dictRes["model"].append(milp.name)
                    dictRes["model UB"].append(milp.modelUB)
                    dictRes["model LB"].append(milp.modelLB)
                    dictRes["Gurobi gap"].append(milp.modelGap)
                    dictRes["nbCols"].append(milp.nbColumns)
                    dictRes["nbRows"].append(milp.nbRows)
                    dictRes["nbNodes"].append(milp.nbNodes)
                    dictRes["CPU"].append(time.time() - start)
                 
                    milp.model.dispose()
                    
                    #Save results to file
                    dfres = pd.DataFrame(dictRes)
                    dfres.to_csv("resuls_" + milp.name + "_" + instSet + ".csv")

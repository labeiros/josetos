from docplex.cp.model import *
import json
import math
from sklearn.cluster import AgglomerativeClustering
import pandas as pd


#--------------------
#-------------------- INSTANCE TEST - ROADEF 2023
#----- JOBS
#Tj = {1:[1,2,3,4], 2:[1,3,5], 3:[3,6,7], 4:[2,7,9], 5:[2,10]}
Tj = {1:{2,4,5,6,9,10},2:{1,2,5,6,9},3:{1,4,5,6,10},4:{1,2,5,6,8},5:{1,3,4,6,7}}#,8,9}}

#----- TOOLS
T = []
for j, ts in Tj.items():
    for t in ts:
        if t not in T:
            T.append(t)
print(T)
#----- MACHINES
C = {1:6,2:5}
Jt = {}
for t in T:
    Jt[t] = []
    for j in Tj.keys():
        if t in Tj[j]:
            Jt[t].append(j)

print(Jt)
#----- SET K
Kt = {}
for t in T:
    Kt[t] = [k for k in range(1, len(Jt[t])+1)]

print(Kt)
#--------------------
#-------------------- MODEL - CP1 - min TS
#----------
#---------- create mymodel
mymodel = CpoModel()

#----------
#---------- define variables
intervalJ = {}
intervalT = {}
integerTS = {}
#----------
#---------- create variables
minSize = 1
maxSize = len(Tj.keys())
#----- intervalJ_jm
for j in Tj.keys():
    intervalJ[j] = {}
    for m in C.keys():
        intervalJ[j][m] = interval_var(optional=True, start=(0,maxSize), end=(1,maxSize), length=1)#, start=(minStart,maxStart), end=(minEnd,maxEnd)
#----- intervalT_tmk
for t in T:
    intervalT[t] = {}
    for m in C.keys():
        intervalT[t][m] = {}
        for k in Kt[t]:
            intervalT[t][m][k] = interval_var(optional=True, start=(0,maxSize), end=(1,maxSize), length=(1,maxSize))#, start=(minStart,maxStart), end=(minEnd,maxEnd)
#----- integerTS
for m in C.keys():
    integerTS[m] = integer_var(min=0,max=sum([len(x) for x in Tj.values()]))
#----------
#---------- define Obj.F: min TS
mymodel.minimize(mymodel.sum([integerTS[m] for m in C.keys()]))
    
#[mymodel.presence_of(intervalT[t][m][k]) for t in T for m in C.keys() for k in Kt[t] ]))

#----------
#---------- add constraints
#----- constraint C1
for j in Tj.keys():
    mymodel.add( mymodel.sum([mymodel.presence_of(intervalJ[j][m]) for m in C.keys()]) >= 1 )

#----- constraint C2 & C3
for m in C.keys():

    mymodel.add(integerTS[m] + C[m] >= mymodel.sum([mymodel.presence_of(intervalT[t][m][k]) for t in T for k in Kt[t]]))
    #mymodel.add( mymodel.no_overlap([ intervalJ[j][m] for j in Tj.keys()]))

    for t in T:
        #----- C2

        for k in Kt[t][1:]:
            mymodel.add(mymodel.end_of(intervalT[t][m][k-1],maxSize)<=mymodel.start_of(intervalT[t][m][k],maxSize))

    #----- C3
    mymodel.add( mymodel.sum([ mymodel.pulse(intervalT[t][m][k], 1, _x=None) for t in T for k in Kt[t] ]) <= C[m] )

#----- constraint C4
for j in Tj.keys():
    for m in C.keys():
        pulseList = [mymodel.pulse(intervalT[t][m][k], 1, _x=None) for t in Tj[j] for k in Kt[t]]
        mymodel.add( mymodel.always_in( mymodel.sum(pulseList), intervalJ[j][m], len(Tj[j]), len(Tj[j]), _x=None ) )


#----------
#---------- solve mymodel
sol_model = mymodel.solve(TimeLimit=10, execfile='/usr/local/CPLEX_Studio1210/cpoptimizer/bin/x86-64_linux/cpoptimizer')
print(sol_model.get_search_status(), sol_model.get_stop_cause(), sol_model.get_solve_time())
#print(sol_model.get_objective_bound())
#print("Tool Switchs:",sol_model.get_objective_value())
print("jobs","machine","start","end")
for j in Tj.keys():
    for m in C.keys():
        soljm = sol_model.get_var_solution(intervalJ[j][m])
        if soljm.is_present():
            print(j,m, soljm.get_start(), soljm.get_end())

print("tools", "machine", "k", "start", "end")
for t in T:
    for m in C.keys():
        for k in Kt[t]:
            soltmk = sol_model.get_var_solution(intervalT[t][m][k])
            if soltmk.is_present():
                print(t,m,k, soltmk.get_start(), soltmk.get_end())

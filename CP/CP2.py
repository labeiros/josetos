from docplex.cp.model import *
import json
import math
from sklearn.cluster import AgglomerativeClustering
import pandas as pd
import random
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle

#--------------------
#-------------------- INSTANCE TEST - ROADEF 2023
#----- JOBS
#Tj = {1:[1,2,3,4], 2:[1,3,5], 3:[3,6,7], 4:[2,7,9], 5:[2,10]}
Tj = {1:{2,4,5,6,9,10},2:{1,2,5,6,9},3:{1,4,5,6,10},4:{1,2,5,6,8},5:{1,3,4,6,7}}#,8,9}}
#Tj = {1:{2,4,5},2:{1,2,3,5},3:{1,5}}

#----- TOOLS
T = []
for j, ts in Tj.items():
    for t in ts:
        if t not in T:
            T.append(t)
print(T)
#----- MACHINES
C = {1:6,2:5}
#C = {1:4,2:3}
sw = {1:1, 2:2}
Jt = {}
for t in T:
    Jt[t] = []
    for j in Tj.keys():
        if t in Tj[j]:
            Jt[t].append(j)

print(Jt)

#---- PROCESSING TIMES
p = {}
for m in C.keys():
    p[m] = {}
    for j in Tj.keys():
        if len(Tj[j]) <= C[m]:
            p[m][j] = random.randint(1,4)
    
print(p)

#----- SET K
Kt = {}
for t in T:
    Kt[t] = [k for k in range(1, len(Jt[t])+1)]

print(Kt)
#--------------------
#-------------------- MODEL - CP1 - min TS
#----------
#---------- create mymodel
mymodel = CpoModel()

#----------
#---------- define variables
intervalJ = {}
intervalT = {}
intervalL = {}
integerFmax = {}
#----------
#---------- create variables
LB = math.ceil(sum([p[m][j] for m in p.keys() for j in p[m].keys()])/len(C))
UB = sum([p[m][j] + sw[m]*len(Tj[j]) for m in p.keys() for j in p[m].keys()])
#----- intervalJ_jm
for j in Tj.keys():
    intervalJ[j] = {}
    for m in C.keys():
        if len(Tj[j]) <= C[m]:
            intervalJ[j][m] = interval_var(optional=True, start=(0,UB), end=(p[m][j],UB), length=p[m][j])#, start=(minStart,maxStart), end=(minEnd,maxEnd)
#----- intervalT_tmk
for t in T:
    intervalT[t] = {}
    intervalL[t] = {}
    for m in C.keys():
        if len([j for j in Jt[t] if j in p[m].keys()]) > 0:
            intervalT[t][m] = {}
            intervalL[t][m] = {}
            minLength = min([p[m][j] for j in p[m].keys() if j in Jt[t]])
            for k in Kt[t]:
                if k > 1:
                    intervalL[t][m][k] = interval_var(optional=True, start=((k-1)*(minLength+sw[m]),UB), end=((k-1)*(minLength+sw[m])+sw[m],UB), length=sw[m])
                    intervalT[t][m][k] = interval_var(optional=True, start=((k-1)*(minLength+sw[m]),UB), end=(k*(minLength+sw[m]) - sw[m],UB), length=(minLength,UB))
                else:
                    intervalL[t][m][1] = interval_var(optional=True, start=(-C[m]*sw[m],UB), end=(sw[m]-C[m]*sw[m],UB), length=sw[m])
                    #intervalT[t][m][1] = interval_var(optional=True, start=(-C[m]*sw[m]+sw[m],UB), end=(minLength,UB), length=(minLength,UB))
                    intervalT[t][m][1] = interval_var(optional=True, start=(0,UB), end=(minLength,UB), length=(minLength,UB))

#----- integerFmax
integerFmax = integer_var(min=LB,max=UB)
#----------
#---------- define Obj.F: min Fmax
mymodel.minimize(integerFmax)
    
#----------
#---------- add constraints
#----- constraint C1
for j in Tj.keys():
    mymodel.add( mymodel.sum([mymodel.presence_of(intervalJ[j][m]) for m in intervalJ[j].keys()])==1)

#----- constraint C2 & C3
for m in C.keys():

    overlapList = [ intervalJ[j][m] for j in intervalJ.keys() if m in intervalJ[j].keys()] + [ intervalL[t][m][k] for t in intervalL.keys() if m in intervalL[t].keys() for k in intervalL[t][m].keys()]
    mymodel.add( mymodel.no_overlap(overlapList))

    for t in intervalL.keys():
        if m in intervalL[t].keys():
            mymodel.add(integerFmax >= mymodel.end_of(intervalT[t][m][1]))
            #----- C2
            mymodel.add(mymodel.presence_of(intervalT[t][m][1]) <= mymodel.presence_of(intervalL[t][m][1]) )
            mymodel.add(mymodel.start_at_end(intervalT[t][m][1], intervalL[t][m][1], 0))
            mymodel.add(mymodel.end_of(intervalL[t][m][1],UB)<=mymodel.start_of(intervalT[t][m][1],UB))
            for k in Kt[t][1:]:
                mymodel.add(integerFmax >= mymodel.end_of(intervalT[t][m][k]))
                mymodel.add(mymodel.end_of(intervalT[t][m][k-1],UB)<mymodel.start_of(intervalT[t][m][k],UB+1))
                mymodel.add(mymodel.end_of(intervalL[t][m][k-1],UB)<mymodel.start_of(intervalL[t][m][k],UB+1))
                mymodel.add(mymodel.presence_of(intervalT[t][m][k]) <= mymodel.presence_of(intervalL[t][m][k]))
                #mymodel.add(mymodel.start_at_end(intervalT[t][m][k], intervalL[t][m][k],0))
                mymodel.add(mymodel.end_of(intervalL[t][m][k],UB)<=mymodel.start_of(intervalT[t][m][k],UB))


    #----- C3
    mymodel.add( mymodel.sum([ mymodel.pulse(intervalT[t][m][k], 1, _x=None) for t in intervalT.keys() if m in intervalL[t].keys() for k in Kt[t] ]) <= C[m] )

#----- constraint C4
for j in Tj.keys():
    for m in intervalJ[j].keys():
        #mymodel.add(integerFmax >= mymodel.end_of(intervalJ[j][m]))
        pulseList = [mymodel.pulse(intervalT[t][m][k], 1, _x=None) for t in Tj[j] for k in Kt[t]]
        mymodel.add( mymodel.always_in( mymodel.sum(pulseList), intervalJ[j][m], len(Tj[j]), len(Tj[j]), _x=None ) )

#----------
#---------- solve mymodel
sol_model = mymodel.solve(TimeLimit=60, execfile='/usr/local/CPLEX_Studio1210/cpoptimizer/bin/x86-64_linux/cpoptimizer')
print(sol_model.get_search_status(), sol_model.get_stop_cause(), sol_model.get_solve_time())
#print(sol_model.get_objective_bound())
#print("Tool Switchs:",sol_model.get_objective_value())

f, ax = plt.subplots(figsize=(10,10))
ax.set_xlim([min([-C[m]*sw[m] for m in C.keys()]),UB])
ax.set_ylim([0,sum([C[m]+2 for m in C.keys()])])

print("jobs","machine","start","end")
for j in Tj.keys():
    for m in intervalJ[j].keys():
        soljm = sol_model.get_var_solution(intervalJ[j][m])
        if soljm.is_present():
            x = soljm.get_start()
            y = sum([C[m2]+2 for m2 in C.keys() if m2 < m])
            length = soljm.get_end() - x
            print(j,m, x, x+length)
            ax.add_patch(Rectangle((x,y), length, 1, fc="green", ec="black", alpha=0.3))
            plt.text(x+length/3,y + 0.33,"J"+str(j))

print("variable","tools", "machine", "k", "start", "end")
Ymc = {}
for m in C.keys():
    Ymc[m] = {}
    for c in range(C[m]):
        Ymc[m][c] = []

for t in T:
    for m in intervalL[t].keys():
        for k in Kt[t]:
            soltmk = sol_model.get_var_solution(intervalL[t][m][k])
            if soltmk.is_present():
                x = soltmk.get_start()
                y = sum([C[m2]+2 for m2 in C.keys() if m2 < m])
                length = soltmk.get_end() - x 
                print("L:",t,m,k, x, x+length)
                ax.add_patch(Rectangle((x,y), length, 1, fc="red", ec="black", alpha=0.3))
                plt.text(x+length/3,y + 0.33,"L"+str(t))

            soltmk = sol_model.get_var_solution(intervalT[t][m][k])
            if soltmk.is_present():
                x = soltmk.get_start()
                y = sum([C[m2]+2 for m2 in C.keys() if m2 < m])
                length = soltmk.get_end() - x
                print("T:",t,m,k, x, x+length)
                drawn = False
                c = 0
                while not drawn:
                    p = x
                    while (p < x+length) and (p not in Ymc[m][c]):
                        p += 1
                    if p == x+length:
                        for p2 in range(x,p):
                            Ymc[m][c].append(p2)
                        ax.add_patch(Rectangle((x,y+1+c), length, 1, fc="orange", ec="black", alpha=0.3))
                        plt.text(x+length/3,y + 1.33+c,"T"+str(t))
                        drawn=True
                    else:
                        c += 1


f.savefig("sol.png")

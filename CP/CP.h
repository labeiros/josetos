#ifndef CP_H
#define CP_H

#include "Data.h"
#include "Parameters.h"
#include "Solution.h"
#include "MyGRBcallback.h"

using namespace std;

Class CP{
public:
  //Methods
  CP();	//constructor
  virtual~CP(); //destructor
  void createCPModel1();	//CP1 model CPOptimizer --- min TS
  void createCPModel2();	//CP2 model CPOptimizer --- min FMAX

  //Output vars

private:
	Data *data;
	Parameters *parameters;
	Solution *solution;

};
#endif
//---------------------------------------------------------------------------

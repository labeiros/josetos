#include CP.h
#include <string>

//constructor
CP::CP(Data *dta, Parameters *parameters, Solution *solution){
  this->data = data;
  this->parameters = parameters;
  this->solution = solution

}
//destructor
CP::~CP(){

}

//CP1 model CPOptimizer --- min TS
void CP::createCPModel1(){
  try{
    cout << "Creating CP1 using CP Optmizer --- to minimize TS" << endl;
    //define VARIABLES

    // define CONSTRAINTs

    // create MODEL


  }
  catch(...){
    cout << "Error during Optimization" << endl;
  }

}


//CP2 model CPOptimizer --- min FMAX
void CP::createCPModel2(){
  try{
    cout << "Creating CP2 using CP Optmizer --- to minimize FMAX" << endl;
    //define VARIABLES

    // define CONSTRAINTs

    // create MODEL


  }
  catch(...){
    cout << "Error during Optimization" << endl;
  }

}
